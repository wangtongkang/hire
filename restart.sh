#!/bin/bash
echo "拉去最新代码"
cd /data/hire
git fetch --all
git reset --hard origin/master
git pull

echo "打包最新代码"
mvn clean package -Dmaven.test.skip=true 
# mv ./target/*.jar /data

echo "重启服务"
# cd /data/hire/

port_9999="lsof -i:9999 |awk -F '  *' '{if (NR>1){print \$2}}'"

port_9999=`lsof -i:9999 |awk -F '  *' '{if (NR>1){print $2}}'`
port_9998=`lsof -i:9998 |awk -F '  *' '{if (NR>1){print $2}}'`


#!/bin/sh
LOG_DIR="./"

JAVA_OPT_LOG=" -verbose:gc" #打印 GC 日志
JAVA_OPT_LOG="${JAVA_OPT_LOG} -XX:+PrintHeapAtGC" 
JAVA_OPT_LOG="${JAVA_OPT_LOG} -XX:+PrintGCDetails" #打印详细 GC 日志
JAVA_OPT_LOG="${JAVA_OPT_LOG} -XX:+PrintGCDateStamps" #系统时间，更加可读
JAVA_OPT_LOG="${JAVA_OPT_LOG} -XX:+PrintGCApplicationStoppedTime" #打印 STW 时间
JAVA_OPT_LOG="${JAVA_OPT_LOG} -XX:+PrintTenuringDistribution" #打印对象年龄分布，对调优 MaxTenuringThreshold 参数帮助很大
JAVA_OPT_LOG="${JAVA_OPT_LOG} -Xloggc:${LOG_DIR}gc_%p.log" # GC日志输出的文件路径

JAVA_OPT_OOM=" -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=${LOG_DIR} -XX:ErrorFile=${LOG_DIR}hs_error_pid%p.log "

JAVA_OPT=" -XX:+UseG1GC"
JAVA_OPT="${JAVA_OPT} ${JAVA_OPT_LOG} ${JAVA_OPT_OOM}"
# JAVA_OPT="${JAVA_OPT} -XX:-OmitStackTraceInFastThrow"




# 两个服务都启动的时候将两个服务都杀死
if test -n "$port_9999" && test -n "$port_9998"
then
    echo "kill all server"
    kill -9 $port_9999 $port_9998
fi


if test -n "$port_9999"
then
    echo "9999 server is running"
    echo "starting 9998 server"
    (java $JAVA_OPT -Dserver.port=9998 -jar /data/hire/target/hire.jar >> hire.log &)

    started=`lsof -i:9998 |awk -F '  *' '{if (NR>1){print $2}}'`
    while test -z $started
    do
        sleep 1;
        started=`lsof -i:9998 |awk -F '  *' '{if (NR>1){print $2}}'`
        echo "starting 9998 server in $started"
    done

    echo "kill 9999 server in $port_9999"
    kill -9 $port_9999

elif test -n "$port_9998"
then
    echo "server 9998 is running"
    echo "starting 9999 server"
    (java $JAVA_OPT -Dserver.port=9999 -jar /data/hire/target/hire.jar >> hire.log &)

    started=`lsof -i:9999 |awk -F '  *' '{if (NR>1){print $2}}'`
    while test -z $started
    do
        sleep 1;
        started=`lsof -i:9999 |awk -F '  *' '{if (NR>1){print $2}}'`
        echo "starting 9999 server in $started"
    done
    echo "kill 9998 server in $port_9998"
    kill -9 $port_9998
else
    (java $JAVA_OPT -Dserver.port=9998 -jar /data/hire/target/hire.jar >> hire.log &)
fi

# ps -ef|grep java|awk -F "  *" '{print $2}' |xargs kill -9
# nohup java -XX:+UseG1GC -jar /data/hire/target/hire.jar &
# echo "重启完成"