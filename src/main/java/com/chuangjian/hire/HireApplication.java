package com.chuangjian.hire;

import com.baomidou.kisso.SSOCache;
import com.baomidou.kisso.SSOHelper;
import com.chuangjian.hire.common.exption.UnionExceptionHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class HireApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(HireApplication.class, args);
        SSOCache bean = run.getBean(SSOCache.class);
        SSOHelper.getSsoConfig().setCache(bean);
    }

    @Bean
    UnionExceptionHandler getUnionExceptionHandler() {
        return new UnionExceptionHandler();
    }
}
