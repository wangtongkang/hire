//package com.chuangjian.hire.active;
//
//
//import com.chuangjian.hire.order.service.OrderMainService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.jms.annotation.JmsListener;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//
//@Slf4j
//@Component
//public class OrderConsumer {
//
//
//    @Resource
//    OrderMainService orderMainService;
//
//    @JmsListener(destination = "default.queue")
//    public void receiveQueue(Long orderId) {
//        log.info("收到延迟订单 : {}", orderId);
//        if (orderId == null) {
//            return;
//        }
//        orderMainService.payFailOrCancel(orderId, true);
//    }
//}