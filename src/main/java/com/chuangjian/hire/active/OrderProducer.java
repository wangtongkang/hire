//package com.chuangjian.hire.active;
//
//import com.alibaba.fastjson.JSON;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.activemq.ScheduledMessage;
//import org.apache.activemq.command.ActiveMQQueue;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.jms.JmsProperties;
//import org.springframework.jms.core.JmsMessagingTemplate;
//import org.springframework.stereotype.Service;
//
//import javax.jms.*;
//import java.io.Serializable;
//
//@Slf4j
//@Service
//public class OrderProducer {
//
//    public static final Destination DEFAULT_QUEUE = new ActiveMQQueue("default.queue");
//
//    @Autowired
//    private JmsMessagingTemplate template;
//
//    /**
//     * 发送消息
//     *
//     * @param destination destination是发送到的队列
//     * @param message     message是待发送的消息
//     */
//    public <T extends Serializable> void send(Destination destination, T message) {
//        template.convertAndSend(destination, message);
//    }
//
//    /**
//     * 延时发送
//     *
//     * @param destination 发送的队列
//     * @param data        发送的消息
//     * @param minute      延迟时间 分钟
//     */
//    public <T extends Serializable> void delaySend(Destination destination, T data, Integer minute) {
//        log.info("发送延迟消息 : {}", JSON.toJSONString(data));
//        Connection connection = null;
//        Session session = null;
//        MessageProducer producer = null;
//        // 获取连接工厂
//        ConnectionFactory connectionFactory = template.getConnectionFactory();
//        try {
//            // 获取连接
//            connection = connectionFactory.createConnection();
//            connection.start();
//            // 获取session，true开启事务，false关闭事务
//            session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
//            // 创建一个消息队列
//            producer = session.createProducer(destination);
//            producer.setDeliveryMode(JmsProperties.DeliveryMode.PERSISTENT.getValue());
//            ObjectMessage message = session.createObjectMessage(data);
//            //设置延迟时间
//            message.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY, minute * 60 * 1000L);
//            // 发送消息
//            producer.send(message);
//            session.commit();
//        } catch (Exception e) {
//            log.error("发送消息失败", e);
//        } finally {
//            try {
//                if (producer != null) {
//                    producer.close();
//                }
//                if (session != null) {
//                    session.close();
//                }
//                if (connection != null) {
//                    connection.close();
//                }
//            } catch (Exception e) {
//                log.error("发送消息失败");
//            }
//        }
//    }
//}