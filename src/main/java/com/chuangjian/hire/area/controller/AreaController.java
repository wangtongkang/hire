package com.chuangjian.hire.area.controller;


import com.chuangjian.hire.area.service.AreaService;
import com.chuangjian.hire.common.controller.BaseController;
import com.chuangjian.hire.common.dto.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@Api(tags = "作品区域 工作区域")
@RestController
@RequestMapping("/area")
public class AreaController extends BaseController {
    @Resource
    private AreaService areaService;

    @GetMapping("getList")
    @ApiOperation("获取区域")
    public R getList() {
        return R.success(areaService.getList());
    }
}

