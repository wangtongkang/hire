package com.chuangjian.hire.area.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.area.domain.AreaDO;
import com.github.flyingglass.mybatis.cache.MybatisRedisCache;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Property;


@CacheNamespace(
        implementation = MybatisRedisCache.class,
        properties = {@Property(
                name = "flushInterval",
                value = "3600000"
        )}
)
public interface AreaMapper extends BaseMapper<AreaDO> {

}
