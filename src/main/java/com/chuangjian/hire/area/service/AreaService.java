package com.chuangjian.hire.area.service;


import com.chuangjian.hire.common.dto.LabelValueDTO;

import java.util.List;


/**
 * FileName: AreaBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/15 21:54
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface AreaService {
    List<LabelValueDTO> getList();
}
