package com.chuangjian.hire.area.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chuangjian.hire.catalog.domain.CatalogDO;
import com.chuangjian.hire.common.dto.LabelValueDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chuangjian.hire.area.domain.AreaDO;
import com.chuangjian.hire.area.dao.AreaMapper;
import com.chuangjian.hire.area.service.AreaService;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * FileName: AreaBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/15 21:54
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Transactional
@Service("areaService")
public class AreaServiceImpl extends ServiceImpl<AreaMapper, AreaDO> implements AreaService {

    @Override
    public List<LabelValueDTO> getList() {
        return list(Wrappers.<AreaDO>lambdaQuery())
                .stream().map(it -> {
                    LabelValueDTO<String, Long> label = new LabelValueDTO<>();
                    label.setLabel(it.getName());
                    label.setValue(it.getId());
                    return label;
                }).collect(Collectors.toList());
    }
}
