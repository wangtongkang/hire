package com.chuangjian.hire.captcha.service;

import com.chuangjian.hire.captcha.bean.CaptchaBean;

public interface ICaptchaService {

    CaptchaBean generateCaptcha() throws Exception;

    ImageTypeEnum getImageType();

}
