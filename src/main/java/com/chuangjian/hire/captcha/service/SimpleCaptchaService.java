package com.chuangjian.hire.captcha.service;

import com.chuangjian.hire.captcha.strategy.ICaptchaStrategy;

import java.awt.*;
import java.awt.image.BufferedImage;

public class SimpleCaptchaService extends AbstractCaptchaService {


    private ImageTypeEnum imageType;

    public SimpleCaptchaService(int width, int height, int fontSize, int lineNum, float yawp,
                                Color color, ICaptchaStrategy captchaStrategy, boolean transform, ImageTypeEnum imageType) {
        super(width, height, fontSize, lineNum, yawp, color, captchaStrategy, transform);
        this.imageType = imageType;
    }

    @Override
    public void drawOther(BufferedImage image) {
        //do something
    }

    @Override
    public ImageTypeEnum getImageType() {
        return imageType;
    }
}
