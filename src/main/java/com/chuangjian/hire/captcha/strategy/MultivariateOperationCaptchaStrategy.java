package com.chuangjian.hire.captcha.strategy;

import com.chuangjian.hire.captcha.bean.CaptchaBean;
import com.chuangjian.hire.captcha.strategy.calculate.CalculateTypeEnum;
import com.chuangjian.hire.captcha.utils.EnumUtils;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import java.math.BigInteger;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 多元运算
 *
 * @date: 2019-01-23 9:42
 */
public class MultivariateOperationCaptchaStrategy implements ICaptchaStrategy {
    private static final Random RANDOM = new Random();
    private static final ExpressionParser parser = new SpelExpressionParser();
    private int number = 2;

    @Override
    public CaptchaBean generateCode() throws Exception {
        CaptchaBean captchaBean = new CaptchaBean();
        //生成指定个数的数字字符串并随机拼接运算符
        List<String> strs = Stream.iterate(BigInteger.ZERO, i -> i.add(BigInteger.ONE))
                .limit(number)
                .map(i -> String.valueOf(RANDOM.nextInt(10)) + EnumUtils.random(CalculateTypeEnum.class).getType())
                .collect(Collectors.toList());

        //将数组进行拼接
        String result = String.join("", strs);
        result = result.substring(0, result.length() - 1);
        Expression expression = parser.parseExpression(result);
        captchaBean.setCodeArray((result + "=").split(""));
        captchaBean.setResult(String.valueOf(expression.getValue(Integer.class)));
        return captchaBean;
    }

    @Override
    public void setNumber(int number) {
        if (number < 2) {
            throw new IllegalArgumentException("MultivariateOperationCaptchaStrategy number must > 1 ");
        }
        this.number = number;
    }
}
