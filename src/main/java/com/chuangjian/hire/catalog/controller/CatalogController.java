//package com.chuangjian.hire.catalog.controller;
//
//
//import com.baomidou.kisso.annotation.Login;
//import com.chuangjian.hire.catalog.dto.UsersCatalogDTO;
//import com.chuangjian.hire.catalog.service.CatalogService;
//import com.chuangjian.hire.catalog.service.UserCatalogService;
//import com.chuangjian.hire.common.controller.BaseController;
//import com.chuangjian.hire.common.dto.R;
//import com.chuangjian.hire.users.dto.UserDTO;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.web.bind.annotation.*;
//
//import javax.annotation.Resource;
//
//
//@Api(tags = "用户擅长类目")
//@RestController
//@RequestMapping("/catalog")
//public class CatalogController extends BaseController {
//    @Resource
//    private CatalogService catalogService;
//
//    @GetMapping("getList")
//    @ApiOperation("获取类目")
//    public R getList() {
//        return R.success(catalogService.getList());
//    }
//
//    @Resource
//    private UserCatalogService userCatalogService;
//
//    @Login
//    @ApiOperation("获取用户擅长类目")
//    @GetMapping("getUserCatalogList")
//    public R getUserCatalogList() {
//        UserDTO userInfo = getUserInfo();
//        return R.success(userCatalogService.getUserCatalogList(userInfo.getId()));
//    }
//
//    @Login
//    @PostMapping("addOrUpdate")
//    @ApiOperation("添加擅长类目")
//    public R addOrUpdate(@RequestBody UsersCatalogDTO catalog) {
//        UserDTO userInfo = getUserInfo();
//        return R.success(userCatalogService.addOrUpdate(catalog, userInfo.getId()));
//    }
//
//    @Login
//    @PostMapping("deleteById")
//    @ApiOperation("删除用户擅长类目")
//    public R deleteById(@RequestBody UsersCatalogDTO catalog) {
//        return R.success(userCatalogService.deleteById(catalog, getUserInfo().getId()));
//    }
//}
//
