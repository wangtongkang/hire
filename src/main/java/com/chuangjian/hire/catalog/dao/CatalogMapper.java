package com.chuangjian.hire.catalog.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.catalog.domain.CatalogDO;
import com.github.flyingglass.mybatis.cache.MybatisRedisCache;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Property;


@CacheNamespace(
        implementation = MybatisRedisCache.class,
        properties = {@Property(
                name = "flushInterval",
                value = "3600000"
        )}
)
public interface CatalogMapper extends BaseMapper<CatalogDO> {

}
