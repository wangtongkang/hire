package com.chuangjian.hire.catalog.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * FileName: UserCatalogBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/13 09:21
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
@ApiModel("类目")
public class UsersCatalogDTO {
    @ApiModelProperty("类目名称")
    private String catalog;
    @ApiModelProperty("catalogId")
    private String catalogId;
}
