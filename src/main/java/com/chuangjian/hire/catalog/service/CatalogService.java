package com.chuangjian.hire.catalog.service;


import com.chuangjian.hire.catalog.dto.CatalogDTO;
import com.chuangjian.hire.common.dto.LabelValueDTO;

import java.util.List;
import java.util.Map;


/**
 * FileName: CatalogBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/07 22:13
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface CatalogService {
    List<LabelValueDTO> getList();

    Map<String, CatalogDTO> getLabelMap();
}
