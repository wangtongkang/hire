package com.chuangjian.hire.catalog.service;


import com.chuangjian.hire.catalog.dto.UsersCatalogDTO;

import java.util.List;

/**
 * FileName: UserCatalogBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/13 09:21
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface UserCatalogService {


    Boolean addOrUpdate(UsersCatalogDTO catalog, Long userId);

    Boolean deleteById(UsersCatalogDTO catalog, Long userId);

    List<UsersCatalogDTO> getUserCatalogList(Long userId);

    List<UsersCatalogDTO> getUserCatalogList(String catalogIdList);
}
