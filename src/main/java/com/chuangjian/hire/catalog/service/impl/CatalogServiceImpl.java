package com.chuangjian.hire.catalog.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chuangjian.hire.catalog.dao.CatalogMapper;
import com.chuangjian.hire.catalog.domain.CatalogDO;
import com.chuangjian.hire.catalog.dto.CatalogDTO;
import com.chuangjian.hire.catalog.service.CatalogService;
import com.chuangjian.hire.common.dto.LabelValueDTO;
import com.chuangjian.hire.label.domain.LabelDO;
import com.chuangjian.hire.util.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * FileName: CatalogBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/07 22:13
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Transactional
@Service("catalogService")
public class CatalogServiceImpl extends ServiceImpl<CatalogMapper, CatalogDO> implements CatalogService {

    @Override
    public List<LabelValueDTO> getList() {
        return list(Wrappers.<CatalogDO>lambdaQuery())
                .stream().map(it -> {
                    LabelValueDTO<String, Long> label = new LabelValueDTO<>();
                    label.setLabel(it.getName());
                    label.setValue(it.getId());
                    return label;
                }).collect(Collectors.toList());
    }

    @Override
    public Map<String, CatalogDTO> getLabelMap() {
        return BeanUtils.transformList(CatalogDTO.class, list(Wrappers.<CatalogDO>lambdaQuery()))
                .stream().collect(Collectors.toMap(it -> it.getId(), it -> it));
    }
}
