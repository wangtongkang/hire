//package com.chuangjian.hire.catalog.service.impl;
//
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.chuangjian.hire.catalog.dto.CatalogDTO;
//import com.chuangjian.hire.catalog.dto.UsersCatalogDTO;
//import com.chuangjian.hire.catalog.service.CatalogService;
//import com.chuangjian.hire.catalog.service.UserCatalogService;
//import com.chuangjian.hire.common.dto.LabelValueDTO;
//import com.chuangjian.hire.users.dao.UsersMapper;
//import com.chuangjian.hire.users.domain.UsersDO;
//import com.chuangjian.hire.util.BeanUtils;
//import com.chuangjian.hire.util.StringUtil;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.annotation.Resource;
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;
//
///**
// * FileName: UserCatalogBusiness
// * Description:
// *
// * @author: CodeGenerator
// * @date: 2020/07/13 09:21
// * Copyright (C) 杭州同基汽车科技有限公司
// */
//@Transactional
//@Service("userCatalogService")
//public class UserCatalogServiceImpl extends ServiceImpl<UsersMapper, UsersDO> implements UserCatalogService {
//
//    @Resource
//    CatalogService catalogService;
//
//    @Override
//    public Boolean addOrUpdate(UsersCatalogDTO catalog, Long userId) {
//        UsersDO users = getById(userId);
//        List<String> catalogIdList = StringUtil.getList(users.getCatalogIdList());
//        catalogIdList.add(catalog.getCatalogId());
//        return update(Wrappers.<UsersDO>lambdaUpdate()
//                .set(UsersDO::getCatalogIdList, StringUtil.getDBString(catalogIdList))
//                .eq(UsersDO::getId, userId));
//    }
//
//    @Override
//    public Boolean deleteById(UsersCatalogDTO catalog, Long userId) {
//        UsersDO users = getById(userId);
//        List<String> catalogIdList = StringUtil.getList(users.getCatalogIdList());
//        catalogIdList.remove(catalog.getCatalogId());
//        return update(Wrappers.<UsersDO>lambdaUpdate()
//                .set(UsersDO::getCatalogIdList, StringUtil.getDBString(catalogIdList))
//                .eq(UsersDO::getId, userId));
//    }
//
//    @Override
//    public List<UsersCatalogDTO> getUserCatalogList(Long userId) {
//        UsersDO users = getById(userId);
//        return getUserCatalogList(users.getCatalogIdList());
//    }
//
//    @Override
//    public List<UsersCatalogDTO> getUserCatalogList(String catalogIdList) {
//        List<String> catalogIdLists = StringUtil.getList(catalogIdList);
//        Map<String, CatalogDTO> catalogMap = catalogService.getLabelMap();
//        return catalogIdLists.stream().map(it -> {
//            UsersCatalogDTO catalog = new UsersCatalogDTO();
//            catalog.setCatalog(String.valueOf(catalogMap.get(it).getName()));
//            catalog.setCatalogId(it);
//            return catalog;
//        }).collect(Collectors.toList());
//    }
//}
