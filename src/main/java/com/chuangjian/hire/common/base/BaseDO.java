package com.chuangjian.hire.common.base;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import lombok.Data;

@Data
public class BaseDO<T extends Model<?>> extends Model<T> {

    /**
     * 创建人
     */
    @Column
    @TableField(value = "creator", fill = FieldFill.INSERT)
    private java.lang.String creator;
    /**
     * 创建人id
     */
    @Column
    @TableField(value = "creator_id", fill = FieldFill.INSERT)
    private java.lang.Long creatorId;
    /**
     * 创建时间
     */
    @Column(type = MySqlTypeConstant.DATETIME)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private java.util.Date createDate;
    /**
     * 修改人id
     */
    @Column
    @TableField(value = "modifier_id", fill = FieldFill.UPDATE)
    private java.lang.Long modifierId;
    /**
     * 修改人
     */
    @Column
    @TableField(value = "modifier", fill = FieldFill.UPDATE)
    private java.lang.String modifier;
    /**
     * 修改时间
     */
    @Column(type = MySqlTypeConstant.DATETIME)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "modify_date", fill = FieldFill.UPDATE)
    private java.util.Date modifyDate;
}
