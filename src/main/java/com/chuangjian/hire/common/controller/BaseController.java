package com.chuangjian.hire.common.controller;


import com.alibaba.fastjson.JSON;
import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.common.SSOConstants;
import com.baomidou.kisso.security.token.SSOToken;
import com.chuangjian.hire.common.exceptionEnum.ApiExceptionEnum;
import com.chuangjian.hire.common.exption.ApiException;
import com.chuangjian.hire.users.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * ClassName: BaseController
 * Description: Controller公共组件
 *
 * @author hh
 * @date 2018/09/10 11:18
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Slf4j
public abstract class BaseController {


    /**
     * Description : 提供给 MyMetaObjectHandler 使用业务方法请勿请勿请勿请勿请勿调用
     * Group :
     *
     * @return orgEmployee
     * @author wangtk
     */
    public static UserDTO getUserInfo() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) return null;
        HttpServletRequest request = requestAttributes.getRequest();
        SSOToken ssoToken = SSOHelper.getSSOToken(request);
        if (ssoToken == null) {
            return null;
        }
        return JSON.parseObject(JSON.toJSONString(ssoToken.getData()), UserDTO.class);
    }
}
