package com.chuangjian.hire.common.controller;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.chuangjian.hire.users.dto.UserDTO;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

@Component
public class MybatisPlusObjectHandler extends BaseController implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        UserDTO users = getUserInfo();
        if (users != null) {
            setFieldValByName("creatorId", users.getId(), metaObject);
            setFieldValByName("creator", users.getName(), metaObject);
        }
        setFieldValByName("createDate", DateUtil.date(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        UserDTO users = getUserInfo();
        if (users != null) {
            setFieldValByName("modifierId", users.getId(), metaObject);
            setFieldValByName("modifier", users.getName(), metaObject);
        }
        setFieldValByName("modifyDate", DateUtil.date(), metaObject);
    }
}
