package com.chuangjian.hire.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 打勾和选择框 键值对
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class IdsDTO {


    /**
     * 前端展示值
     */
    @NotEmpty
    private List<Long> idList;


    @NotEmpty
    private String type;

}
