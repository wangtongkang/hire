package com.chuangjian.hire.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 打勾和选择框 键值对
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LabelValueDTO<L, V> {


    /**
     * 前端展示值
     */
    private L label;

    /**
     * 选中后实际的值
     */
    private V value;

}
