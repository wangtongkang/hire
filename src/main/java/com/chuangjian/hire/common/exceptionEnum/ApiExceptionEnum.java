package com.chuangjian.hire.common.exceptionEnum;


import com.chuangjian.hire.common.exption.ExceptionEnum;

public enum ApiExceptionEnum implements ExceptionEnum {
    /**
     * 01***--09***，系统异常
     * 10***--19***，HTTP异常
     * 20***--59***，SOA异常
     * 60***--99***，系统内部业务异常
     */
    ERROR_CODE(-1, "sign错误"),
    NO_PARAM_ERROR(01000, "没有参数"),
    ERROR_PARAM(01001, "参数不正确"),
    PARAM_NOT_INCOMPLETE(01002, "参数不全"),
    DEFAULT_ERROR(10500, "服务器繁忙，请稍候重试!"),
    DATA_HUGE_ERROR(10501, "符合搜索条件的银行太多!"),
    DATE_NO_COMPLETE_ERROR(10502, "开始时间或者结束时间未选择"),
    DATE_HUGE_ERROR(10503, "时间间隔最多45天"),
    SOA_ERROR(20000, "SOA异常"),
    LACK_PARAM(01003, "参数缺失，%s为空"),
    OPERATION_FAILED(10504, "操作%s失败"),
    VERIFY_CODE_ERROR(10505, "验证码错误"),
    SMS_PHONE_ERROR(10507, "手机号不存在"),
    USER_NOT_LOGIN(60001, "用户未登录"),
    USER_NOT_EXIST(60002, "用户不存在或者密码错误"),
    REQUEST_PARAM_IS_ERROR(60003, "%s"),
    USER_TYPE_ERROR(60004, "用户类型错误"),
    EXIST_CODE_ERROR(60005, "数据已存在"),
    MENU_EXIST_CODE_ERROR(60005, "菜单已经关联角色 请先删除关联"),
    EXIST_NULL_CODE_ERROR(60006, "数据不存在"),

    /**
     * 系统配置参数模块
     */
    PARAM_ISEXISTS_SYS(94001, "系统配置参数已存在"),

    ERROR_AUTO_RECOMMEND_SYS(94002, "系统配置自动推荐供应商参数错误"),

    ERROR_VERSION_NUM_SYS(94003, "版本号错误"),

    NO_SYSTEM_PARAM_CONFIG_ERROR(94004, "系统配置缺失，请联系管理员"),

    MESSAGE_TO_MANY(94005, "短信发送太快"),

    MSG_TYPE_ERROR(94006, "短信不存在"),

    MOBILE_ERROR(94007, "手机号已经存在"),

    MOBILE_CODE_ERROR(94008, "验证码错误"),
    PASSWORD_ERROR(94009, "原密码错误"),
    COUPON_ERROR(94010, "优惠券错误"),
    HIRE_PAY_ERROR(94011, "支付工资金额不能大于订单金额"),
    HIRE_ONLY_EMPLOYER_ERROR(94012, "只有雇主能支付工资"),
    HIRE_PAY_PASSWORD_ERROR(94013, "支付密码错误"),
    Cmt_ERROR(94014, "已经评价"),
    APPLY_ERROR(94015, "提现金额不能大于余额"),
    BALANCE_ERROR(94015, "账户余额异常"),
    PAY_PWD_ERROR(94016, "支付密码错误"),
    WORK_DATE_ERROR(94017, "在这个时间段设计师已经有工作了"),
    EMAIL_CODE_ERROR(94018, "邮件验证码错误"),
    PAY_ERROR(94019, "支付异常"),
    PAY_BALANCE_ERROR(94020, "余额不足"),
    PAY_CONFIRM_ERROR(94021, "只有设计师能够确认支付"),
    NOT_BIND_ALI_ERROR(94022, "请先绑定支付宝"),
    ORDER_MAIN_STATUS_ERROR(94023, "订单状态异常"),
    ORDER_MULTI_STATUS_ERROR(94024, "有未支付订单"),
    ORDER_AMOUNT_ERROR(94025, "订单金额出错"),
    ORDER_HIRE_ERROR(94026, "只有雇主能够雇佣"),
    ORDER_SUSPEND_1_ERROR(94027, "已经有暂停工作"),
    ORDER_SUSPEND_2_ERROR(94028, "已经请假记录"),
    ORDER_SUSPEND_TIME_ERROR(94029, "时间选择有误"),
    RECOMMEND_REFRESH_ERROR(94030, "推荐设计师和最新设计师不能同时大于0"),
    ORDER_MAIN_EMP_STATUS_ERROR(94031, "雇佣状态异常"),
    ;

    private Integer code;
    private String message;

    ApiExceptionEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
