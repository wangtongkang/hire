package com.chuangjian.hire.common.exceptionEnum;

import com.chuangjian.hire.common.exption.ExceptionEnum;

public enum InnerExceptionEnum implements ExceptionEnum {
    BASE_ERROR(100000, "服务器繁忙，请稍后再试！"),
    USER_NOT_LOGIN(100002, "用户未登陆"),
    WRAPPER_ERROR(100003, "包装结果错误"),
    BLOCK_ERROR(100004, "服务请求频繁，请稍后再试！");

    public Integer code;
    private String message;

    InnerExceptionEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
