package com.chuangjian.hire.common.exption;


import com.baomidou.mybatisplus.core.toolkit.StringUtils;

/**
 * @description API接口请求的统一异常类【SOA所提供的服务采用此异常来统一处理错误信息的返回】
 * 通过该异常类统一将出错信息返回给消费端
 */
public class ApiException extends RuntimeException {
    private static final long serialVersionUID = 2685287081293576410L;
    private Integer errCode;
    private String message;
    private Object[] args;
    private boolean runtime;

    private ApiException() {
        super();
    }

    private ApiException(String message) {
        super(message);
        this.message = message;
    }

    private ApiException(Integer code, String message) {
        super(message);
        this.errCode = code;
        this.message = message;
    }

    private ApiException(String message, Throwable cause) {
        super(message, cause);
        this.message = message;
    }

    private ApiException(Integer errCode, String message, Throwable cause) {
        super(message, cause);
        this.errCode = errCode;
    }

    /**
     * 当带有参数时，文本将通过 String.format 进行相应格式化
     */
    public ApiException(Integer code, String message, Object... args) {
        this.errCode = code;
        this.message = message;
        this.args = args;
        this.resolveMessage();
    }

    /**
     * 当带有参数时，文本将通过 String.format 进行相应格式化
     */
    public ApiException(ExceptionEnum baseExceptionCode, Object... args) {
        this.errCode = baseExceptionCode.getCode();
        this.message = baseExceptionCode.getMessage();
        this.args = args;
        this.resolveMessage();
    }

    private void resolveMessage() {
        if (StringUtils.isBlank(this.message) || this.args == null || this.args.length < 1) {
            return;
        }
        this.message = String.format(this.message, this.args);
    }


    public static ApiException.ApiExceptionBuilder builder() {
        return new ApiException.ApiExceptionBuilder();
    }


    public static class ApiExceptionBuilder {
        ExceptionEnum baseExceptionCode;
        Object[] args;

        ApiExceptionBuilder() {
        }

        public ApiException.ApiExceptionBuilder baseExceptionCode(final ExceptionEnum errCode, Object... args) {
            this.baseExceptionCode = errCode;
            this.args = args;
            return this;
        }

        public ApiException build() {
            return new ApiException(baseExceptionCode, args);
        }
    }


    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public boolean isRuntime() {
        return this.runtime;
    }

    public void setRuntime(boolean runtime) {
        this.runtime = runtime;
    }
}
