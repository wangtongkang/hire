package com.chuangjian.hire.common.exption;

import lombok.Data;

@Data
public class ArgumentInvalidResult {
    private String field;
    private Object rejectedValue;
    private String defaultMessage;
}