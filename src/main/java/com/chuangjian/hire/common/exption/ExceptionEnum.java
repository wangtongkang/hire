package com.chuangjian.hire.common.exption;

public interface ExceptionEnum {

    Integer getCode();

    String getMessage();
}
