package com.chuangjian.hire.commonArea.controller;

import com.chuangjian.hire.commonArea.dto.ProvinceResult;
import com.chuangjian.hire.commonArea.service.CommonProvinceService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Api(tags = "获取省市区")
@RestController
public class CommonProvinceController {

    @Resource
    CommonProvinceService commonProvinceService;

    @GetMapping("/common/getAddress")
    public List<ProvinceResult> getAddress() {
        return commonProvinceService.getAllList();
    }
}
