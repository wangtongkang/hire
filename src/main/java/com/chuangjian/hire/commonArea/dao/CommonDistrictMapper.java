package com.chuangjian.hire.commonArea.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.commonArea.domain.CommonDistrictDO;

/**
 * FileName: CommonDistrictBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2019/07/31 14:12
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface CommonDistrictMapper extends BaseMapper<CommonDistrictDO> {

}

