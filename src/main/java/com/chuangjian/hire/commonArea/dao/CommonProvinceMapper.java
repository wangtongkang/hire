package com.chuangjian.hire.commonArea.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.commonArea.domain.CommonProvinceDO;

import java.util.List;

/**
 * FileName: CommonProvinceBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2019/07/31 14:12
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface CommonProvinceMapper extends BaseMapper<CommonProvinceDO> {
    List<CommonProvinceDO> findListAll();
}
