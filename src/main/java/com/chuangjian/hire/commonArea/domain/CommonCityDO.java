package com.chuangjian.hire.commonArea.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * FileName: CommonCityBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2019/07/31 14:12
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
@TableName("common_city")
public class CommonCityDO extends Model<CommonCityDO> {

    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 城市名称
     */
    private String name;
    /**
     * 城市code
     */
    private Long code;
    /**
     * 上级code
     */
    private Long parentCode;

    public CommonCityDO() {
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
