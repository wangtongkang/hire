package com.chuangjian.hire.commonArea.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * FileName: CommonDistrictBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2019/07/31 14:12
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
@TableName("common_district")
public class CommonDistrictDO extends Model<CommonDistrictDO> {

    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 区域名称
     */
    private String name;
    /**
     * 区域code
     */
    private Long code;
    /**
     * 上级code
     */
    private Long parentCode;

    public CommonDistrictDO() {
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
