package com.chuangjian.hire.commonArea.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * FileName: CommonProvinceBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2019/07/31 14:12
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
@TableName("common_province")
public class CommonProvinceDO extends Model<CommonProvinceDO> {

    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 省份名称
     */
    private String name;
    /**
     * 省份code
     */
    private Long code;

    public CommonProvinceDO() {
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
