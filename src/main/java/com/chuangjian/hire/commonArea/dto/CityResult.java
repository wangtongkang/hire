package com.chuangjian.hire.commonArea.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CityResult implements Serializable {

    private static final long serialVersionUID = 1635047935792335809L;

    public CityResult() {
    }

    public CityResult(String name, Long code) {
        this.name = name;
        this.code = code;
        this.districtList = new ArrayList<>();
    }

    /**
     * 城市名称
     */
    private String name;
    /**
     * 城市code
     */
    private Long code;

    private List<DistrictResult> districtList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public List<DistrictResult> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(List<DistrictResult> districtList) {
        this.districtList = districtList;
    }
}
