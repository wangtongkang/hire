package com.chuangjian.hire.commonArea.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * FileName: CommonCityBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2019/07/31 14:12
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
@TableName("common_city")
public class CommonCityDTO {

    /**
     * 主键ID
     */
    private Long id;
    /**
     * 城市名称
     */
    private String name;
    /**
     * 城市code
     */
    private Long code;
    /**
     * 上级code
     */
    private Long parentCode;

}
