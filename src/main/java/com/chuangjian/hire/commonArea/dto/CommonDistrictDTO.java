package com.chuangjian.hire.commonArea.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * FileName: CommonDistrictBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2019/07/31 14:12
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
@TableName("common_district")
public class CommonDistrictDTO {

    /**
     * 主键ID
     */
    private Long id;
    /**
     * 区域名称
     */
    private String name;
    /**
     * 区域code
     */
    private Long code;
    /**
     * 上级code
     */
    private Long parentCode;


}
