package com.chuangjian.hire.commonArea.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * FileName: CommonProvinceBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2019/07/31 14:12
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
@TableName("common_province")
public class CommonProvinceDTO {

    /**
     * 主键ID
     */
    private Long id;
    /**
     * 省份名称
     */
    private String name;
    /**
     * 省份code
     */
    private Long code;

}
