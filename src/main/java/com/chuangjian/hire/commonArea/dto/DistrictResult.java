package com.chuangjian.hire.commonArea.dto;

import java.io.Serializable;

/**
 * @author Clark
 * @create 2018/5/25
 * Copyright (C) 杭州典击科技有限公司
 * @description
 */
public class DistrictResult implements Serializable {

    private static final long serialVersionUID = 3033274504917571701L;

    /**
     * 区域名称
     */
    private String name;
    /**
     * 区域code
     */
    private Long code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }
}
