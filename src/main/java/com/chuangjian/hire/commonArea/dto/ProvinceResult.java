package com.chuangjian.hire.commonArea.dto;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author pengq
 * @create 2018/5/24 15:39
 * Copyright (C) 杭州典击科技有限公司
 * @description
 */
public class ProvinceResult implements Serializable {

    private static final long serialVersionUID = 8301006202655861489L;

    public ProvinceResult() {
    }

    public ProvinceResult(String name, Long code) {
        this.name = name;
        this.code = code;
        this.cityList = new ArrayList<>();
    }

    /**
     * 省份名称
     */
    private String name;
    /**
     * 省份code
     */
    private Long code;

    private List<CityResult> cityList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public List<CityResult> getCityList() {
        return cityList;
    }

    public void setCityList(List<CityResult> cityList) {
        this.cityList = cityList;
    }
}





