package com.chuangjian.hire.commonArea.service;


import com.chuangjian.hire.commonArea.dto.CommonCityDTO;

import java.util.List;


/**
 * FileName: CommonCityBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2019/07/31 14:12
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface CommonCityService {

    /**
     * 查询所有城市
     *
     * @return
     */
    List<CommonCityDTO> findAllList();

}