package com.chuangjian.hire.commonArea.service;


import com.chuangjian.hire.commonArea.dto.CommonDistrictDTO;

import java.util.List;


/**
 * FileName: CommonDistrictBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2019/07/31 14:12
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface CommonDistrictService {

    /**
     * 获取所有区
     *
     * @return
     */
    List<CommonDistrictDTO> findAllList();


    CommonDistrictDTO getDistrictByName(String name);

    /**
     * @param code 区Code
     * @return
     */
    CommonDistrictDTO getDistrictByCode(Long code);
}
