package com.chuangjian.hire.commonArea.service;


import com.chuangjian.hire.commonArea.dto.CommonProvinceDTO;
import com.chuangjian.hire.commonArea.dto.ProvinceResult;

import java.util.List;


/**
 * FileName: CommonProvinceBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2019/07/31 14:12
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface CommonProvinceService {

    /**
     * 查询列表，若要查询有效数据，设置valid=true
     *
     * @return
     */
    List<CommonProvinceDTO> findListAll();

    /**
     * 查询所有地区
     *
     * @param
     * @Author pengq
     * @Date 2018/5/25 14:17
     * @Return com.dianji.kangaroo.parts.biz.result.AddressModel
     */
    List<ProvinceResult> getAllList();
}