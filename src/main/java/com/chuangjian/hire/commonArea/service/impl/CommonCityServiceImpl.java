package com.chuangjian.hire.commonArea.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chuangjian.hire.commonArea.dao.CommonCityMapper;
import com.chuangjian.hire.commonArea.domain.CommonCityDO;
import com.chuangjian.hire.commonArea.dto.CommonCityDTO;
import com.chuangjian.hire.commonArea.service.CommonCityService;
import com.chuangjian.hire.util.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * FileName: CommonCityBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2019/07/31 14:12
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Transactional
@Service("commonCityService")
public class CommonCityServiceImpl extends ServiceImpl<CommonCityMapper, CommonCityDO> implements CommonCityService {

    @Override
    public List<CommonCityDTO> findAllList() {
        return BeanUtils.transformList(CommonCityDTO.class, list());
    }
}