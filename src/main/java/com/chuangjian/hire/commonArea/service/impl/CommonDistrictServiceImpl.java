package com.chuangjian.hire.commonArea.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chuangjian.hire.commonArea.dao.CommonDistrictMapper;
import com.chuangjian.hire.commonArea.domain.CommonDistrictDO;
import com.chuangjian.hire.commonArea.dto.CommonDistrictDTO;
import com.chuangjian.hire.commonArea.service.CommonDistrictService;
import com.chuangjian.hire.util.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * FileName: CommonDistrictBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2019/07/31 14:12
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Transactional
@Service("commonDistrictService")
public class CommonDistrictServiceImpl extends ServiceImpl<CommonDistrictMapper, CommonDistrictDO> implements CommonDistrictService {

    @Override
    public List<CommonDistrictDTO> findAllList() {

        return BeanUtils.transformList(CommonDistrictDTO.class, list(Wrappers.<CommonDistrictDO>lambdaQuery()));
    }

    @Override
    public CommonDistrictDTO getDistrictByName(String name) {
        CommonDistrictDO dis = new CommonDistrictDO();
        dis.setName(name);

        List<CommonDistrictDO> commonDistrictDOS = list(Wrappers.<CommonDistrictDO>lambdaQuery(dis));
        if (!CollectionUtils.isEmpty(commonDistrictDOS) && commonDistrictDOS.size() == 1) {
            return BeanUtils.transform(CommonDistrictDTO.class, commonDistrictDOS.get(0));
        }
        return null;
    }

    @Override
    public CommonDistrictDTO getDistrictByCode(Long code) {
        CommonDistrictDO district = getOne(Wrappers.<CommonDistrictDO>lambdaQuery().eq(CommonDistrictDO::getCode, code));
        return BeanUtils.transform(CommonDistrictDTO.class, district);
    }

}
