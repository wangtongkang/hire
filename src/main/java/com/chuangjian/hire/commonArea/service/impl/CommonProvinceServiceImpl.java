package com.chuangjian.hire.commonArea.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chuangjian.hire.commonArea.dao.CommonProvinceMapper;
import com.chuangjian.hire.commonArea.domain.CommonCityDO;
import com.chuangjian.hire.commonArea.domain.CommonDistrictDO;
import com.chuangjian.hire.commonArea.domain.CommonProvinceDO;
import com.chuangjian.hire.commonArea.dto.CityResult;
import com.chuangjian.hire.commonArea.dto.CommonProvinceDTO;
import com.chuangjian.hire.commonArea.dto.DistrictResult;
import com.chuangjian.hire.commonArea.dto.ProvinceResult;
import com.chuangjian.hire.commonArea.service.CommonCityService;
import com.chuangjian.hire.commonArea.service.CommonDistrictService;
import com.chuangjian.hire.commonArea.service.CommonProvinceService;
import com.chuangjian.hire.util.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * FileName: CommonProvinceBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2019/07/31 14:12
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Transactional
@Service("commonProvinceService")
public class CommonProvinceServiceImpl extends ServiceImpl<CommonProvinceMapper, CommonProvinceDO> implements CommonProvinceService {

    @Resource
    private CommonCityService commonCityService;

    @Resource
    private CommonDistrictService commonDistrictService;


    @Override
    public List<CommonProvinceDTO> findListAll() {
        return BeanUtils.transformList(CommonProvinceDTO.class, baseMapper.findListAll());
    }

    @Override
    public List<ProvinceResult> getAllList() {
        List<ProvinceResult> provinceResultList = new ArrayList<>();
        //所有
        List<CommonProvinceDO> commonProvinceDOList = BeanUtils.transformList(CommonProvinceDO.class, this.findListAll());
        List<CommonCityDO> commonCityDOList = BeanUtils.transformList(CommonCityDO.class, commonCityService.findAllList());
        List<CommonDistrictDO> commonDistrictDOList = BeanUtils.transformList(CommonDistrictDO.class, commonDistrictService.findAllList());
        //省份
        for (CommonProvinceDO commonProvinceDO : commonProvinceDOList) {
            ProvinceResult provinceResult = new ProvinceResult(commonProvinceDO.getName(), commonProvinceDO.getCode());
            List<CityResult> cityResultList = new ArrayList<>();
            //城市
            List<CommonCityDO> curCommonCity = commonCityDOList.stream().filter(commonCityDO -> commonCityDO.getParentCode().equals(provinceResult.getCode())).collect(Collectors.toList());
            for (CommonCityDO commonCityDO : curCommonCity) {
                CityResult cityResult = new CityResult(commonCityDO.getName(), commonCityDO.getCode());
                //区域
                List<CommonDistrictDO> curCommonDistrict = commonDistrictDOList.stream().filter(commonDistrictDO -> commonDistrictDO.getParentCode().equals(cityResult.getCode())).collect(Collectors.toList());
                List<DistrictResult> districtResultList = new ArrayList<>();
                for (CommonDistrictDO commonDistrictDO : curCommonDistrict) {
                    DistrictResult districtResult = BeanUtils.transform(DistrictResult.class, commonDistrictDO);
                    districtResultList.add(districtResult);
                }
                cityResult.setDistrictList(districtResultList);
                cityResultList.add(cityResult);
            }
            provinceResult.setCityList(cityResultList);
            provinceResultList.add(provinceResult);

        }
        return provinceResultList;
    }
}