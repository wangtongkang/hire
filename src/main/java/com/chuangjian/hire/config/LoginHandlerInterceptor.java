package com.chuangjian.hire.config;

import com.baomidou.kisso.web.handler.SSOHandlerInterceptor;
import com.chuangjian.hire.common.exceptionEnum.ApiExceptionEnum;
import com.chuangjian.hire.common.exption.ApiException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginHandlerInterceptor implements SSOHandlerInterceptor {
    @Override
    public boolean preTokenIsNullAjax(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        throw new ApiException(ApiExceptionEnum.USER_NOT_LOGIN);
    }

    @Override
    public boolean preTokenIsNull(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        throw new ApiException(ApiExceptionEnum.USER_NOT_LOGIN);
    }
}
