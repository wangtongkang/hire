package com.chuangjian.hire.config;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Data
@ToString
@Configuration
@ConfigurationProperties(prefix = "wx")
public class WxPayConfig {
    private String appId;
    private String appSecret;
    private String mchId;
    private String partnerKey;
    private String certPath;
    private Boolean sandbox;
    private String domain;
    private String notifyUrl;
}