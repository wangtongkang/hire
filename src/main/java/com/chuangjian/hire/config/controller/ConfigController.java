package com.chuangjian.hire.config.controller;


import com.chuangjian.hire.common.dto.R;
import com.chuangjian.hire.config.service.ConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(tags = "配置相关")
@Validated
@RestController
@RequestMapping("/config")
public class ConfigController {
    @Resource
    private ConfigService configService;

    @ApiOperation("获取首页 banner")
    @GetMapping("getIndexBanner")
    public R getIndexBanner() {
        return R.success(configService.getIndexBanner());
    }
}

