package com.chuangjian.hire.config.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.config.domain.ConfigDO;


/**
* FileName: ConfigBusiness
* Description:
* @author: CodeGenerator
* @date: 2020/07/10 14:36
* Copyright (C) 杭州同基汽车科技有限公司
*/
public interface ConfigMapper extends BaseMapper<ConfigDO> {

}
