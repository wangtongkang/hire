package com.chuangjian.hire.config.dto;

import lombok.Data;

@Data
public class BannerDTO {
    String url;
    String href;
}
