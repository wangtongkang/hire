package com.chuangjian.hire.config.dto;

import lombok.Data;

import java.util.List;

@Data
public class IndexDTO {
    List<BannerDTO> banner;
    List<BannerDTO> subBanner;
}
