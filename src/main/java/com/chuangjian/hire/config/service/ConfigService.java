package com.chuangjian.hire.config.service;


/**
 * FileName: ConfigBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/10 14:36
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface ConfigService {

    String getIndexBanner();

    Boolean saveIndexBanner(String value);

}
