package com.chuangjian.hire.config.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chuangjian.hire.config.dao.ConfigMapper;
import com.chuangjian.hire.config.domain.ConfigDO;
import com.chuangjian.hire.config.service.ConfigService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * FileName: ConfigBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/10 14:36
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Transactional
@Service("configService")
public class ConfigServiceImpl extends ServiceImpl<ConfigMapper, ConfigDO> implements ConfigService {

    public static final String INDEX_BANNER = "index_banner";


    @Override
    public String getIndexBanner() {
        ConfigDO config = getByName(INDEX_BANNER);
        if (config == null) {
            return null;
        }
        return config.getValue();
    }

    @Override
    public Boolean saveIndexBanner(String value) {
        ConfigDO config = getByName(INDEX_BANNER);
        if (config == null) {
            config = new ConfigDO();
            config.setName(INDEX_BANNER);
            config.setValid(true);
        }
        config.setValue(value);
        saveOrUpdate(config);
        return true;
    }

    ConfigDO getByName(String name) {
        return getOne(
                Wrappers.<ConfigDO>lambdaQuery()
                        .eq(ConfigDO::getName, name)
        );
    }
}
