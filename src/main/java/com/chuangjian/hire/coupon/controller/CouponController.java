//package com.chuangjian.hire.coupon.controller;
//
//
//import com.baomidou.kisso.annotation.Login;
//import com.chuangjian.hire.common.controller.BaseController;
//import com.chuangjian.hire.common.dto.R;
//import com.chuangjian.hire.coupon.service.UsersCouponService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//
//@Api(tags = "优惠券")
//@RestController
//@RequestMapping("/coupon")
//public class CouponController extends BaseController {
//
//    @Resource
//    private UsersCouponService usersCouponService;
//
//    @Login
//    @GetMapping("getUsersCouponList")
//    @ApiOperation("获取用户 优惠券")
//    public R getUsersCouponList() {
//        return R.success(usersCouponService.getUserCouponList(getUserInfo().getId()));
//    }
//}
//
