package com.chuangjian.hire.coupon.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.coupon.domain.CouponDO;


/**
* FileName: CouponBusiness
* Description:
* @author: CodeGenerator
* @date: 2020/07/14 11:28
* Copyright (C) 杭州同基汽车科技有限公司
*/
public interface CouponMapper extends BaseMapper<CouponDO> {

}
