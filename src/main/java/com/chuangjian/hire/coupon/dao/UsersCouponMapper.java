package com.chuangjian.hire.coupon.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.coupon.domain.UsersCouponDO;


/**
* FileName: UsersCouponBusiness
* Description:
* @author: CodeGenerator
* @date: 2020/07/14 10:14
* Copyright (C) 杭州同基汽车科技有限公司
*/
public interface UsersCouponMapper extends BaseMapper<UsersCouponDO> {

}
