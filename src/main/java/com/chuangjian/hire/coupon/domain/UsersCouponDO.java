package com.chuangjian.hire.coupon.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;

/**
* FileName: UsersCouponBusiness
* Description:
* @author: CodeGenerator
* @date: 2020/07/14 10:14
* Copyright (C) 杭州同基汽车科技有限公司
*/
@Data
@TableName("users_coupon")
public class UsersCouponDO extends Model<UsersCouponDO> {

	/**
	* labelId
	*/
	@TableId(type = IdType.AUTO)
	private java.lang.Long id;
	/**
	* 创建人id
	*/
	private java.lang.Long userId;
	/**
	* 优惠券Id
	*/
	private java.lang.Long couponId;
	/**
	* 优惠券名称
	*/
	private java.lang.String title;
	/**
	* 优惠券备注
	*/
	private java.lang.String remark;
	/**
	* 金额
	*/
	private java.math.BigDecimal amount;
	/**
	* 状态 1有效 0,已过期,2,已使用
	*/
	private java.lang.Integer status;
	/**
	* 开始时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private java.util.Date startDate;
	/**
	* 结束时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private java.util.Date endDate;
	/**
	* 创建人
	*/
	@TableField(value = "creator", fill = FieldFill.INSERT)
	private java.lang.String creator;
	/**
	* 创建人id
	*/
	@TableField(value = "creator_id", fill = FieldFill.INSERT)
	private java.lang.Long creatorId;
	/**
	* 创建时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField(value = "create_date", fill = FieldFill.INSERT)
	private java.util.Date createDate;
	/**
	* 修改人id
	*/
	@TableField(value = "modifier_id", fill = FieldFill.UPDATE)
	private java.lang.Long modifierId;
	/**
	* 修改人
	*/
	@TableField(value = "modifier", fill = FieldFill.UPDATE)
	private java.lang.String modifier;
	/**
	* 修改时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField(value = "modify_date", fill = FieldFill.UPDATE)
	private java.util.Date modifyDate;
	/**
	* 是否有效，0.删除，1.有效
	*/
	@TableLogic
	private java.lang.Boolean valid;

    @Override
    protected Serializable pkVal() {
    	return this.id;
    }
}
