package com.chuangjian.hire.coupon.enums;

public enum CouponStatus {
    // 1有效 0,已过期,2,已使用
    VALID(1, "有效"),
    EXPIRE(0, "已过期"),
    USED(2, "已使用");

    private Integer value;
    private String desc;

    private CouponStatus(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static CouponStatus getType(Integer value) {
        if (value == null) {
            return null;
        }
        for (CouponStatus status : values()) {
            if (status.value.equals(value)) {
                return status;
            }
        }
        return null;
    }


    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
