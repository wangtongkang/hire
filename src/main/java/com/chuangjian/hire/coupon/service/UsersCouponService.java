package com.chuangjian.hire.coupon.service;


import com.chuangjian.hire.coupon.domain.UsersCouponDO;

import java.math.BigDecimal;
import java.util.List;

/**
 * FileName: UsersCouponBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/14 10:14
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface UsersCouponService {


    /**
     * 获取用户优惠券
     *
     * @param userId
     * @return
     */
    List<UsersCouponDO> getUserCouponList(Long userId);

    /**
     * 使用优惠券
     *
     * @param userCouponId
     * @param amount
     * @param userId
     * @return
     */
    Boolean useCoupon(Long userCouponId, BigDecimal amount, Long userId);

    /**
     * 优惠券返还
     *
     * @param userCouponId
     * @return
     */
    Boolean recovery(Long userCouponId, Long userId);
}
