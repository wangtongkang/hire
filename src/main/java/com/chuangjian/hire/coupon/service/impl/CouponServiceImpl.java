package com.chuangjian.hire.coupon.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chuangjian.hire.coupon.dao.CouponMapper;
import com.chuangjian.hire.coupon.domain.CouponDO;
import com.chuangjian.hire.coupon.service.CouponService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * FileName: CouponBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/14 11:28
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Transactional
@Service("couponService")
public class CouponServiceImpl extends ServiceImpl<CouponMapper, CouponDO> implements CouponService {

}
