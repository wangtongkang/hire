//package com.chuangjian.hire.label.controller;
//
//
//import com.baomidou.kisso.annotation.Login;
//import com.chuangjian.hire.common.controller.BaseController;
//import com.chuangjian.hire.common.dto.R;
//import com.chuangjian.hire.label.dto.UsersLabelDTO;
//import com.chuangjian.hire.label.service.LabelService;
//import com.chuangjian.hire.label.service.UsersLabelService;
//import com.chuangjian.hire.users.dto.UserDTO;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.*;
//
//import javax.annotation.Resource;
//
//@Api(tags = "用户标签")
//@RestController
//@RequestMapping("/label")
//public class LabelController extends BaseController {
//    @Resource
//    private LabelService labelService;
//    @Resource
//    UsersLabelService usersLabelService;
//
//    @GetMapping("getList")
//    @ApiOperation("标签列表")
//    public R getList() {
//        return R.success(labelService.getList());
//    }
//
//    @Login
//    @GetMapping("getUsersLabelList")
//    @ApiOperation("获取用户 标签列表")
//    public R getUsersLabelList() {
//        UserDTO userInfo = getUserInfo();
//        return R.success(usersLabelService.getUsersLabels(userInfo.getId()));
//    }
//
//    @Login
//    @PostMapping("addLabels")
//    @ApiOperation("添加 用户标签")
//    public R addLabels(@Validated @RequestBody UsersLabelDTO usersLabel) {
//        UserDTO userInfo = getUserInfo();
//        return R.success(usersLabelService.addLabels(usersLabel, userInfo));
//    }
//
//    @Login
//    @PostMapping("removeLabels")
//    @ApiOperation("移除 用户标签")
//    public R removeLabels(@RequestBody UsersLabelDTO usersLabel) {
//        UserDTO userInfo = getUserInfo();
//        return R.success(usersLabelService.removeLabels(usersLabel, userInfo));
//    }
//}
//
