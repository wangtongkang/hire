package com.chuangjian.hire.label.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * FileName: UserCatalogBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/13 09:21
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
@ApiModel("用户标签")
public class UsersLabelDTO {
    @ApiModelProperty("用户Id")
    private Long userId;
    @ApiModelProperty("标签名称")
    private String label;
}
