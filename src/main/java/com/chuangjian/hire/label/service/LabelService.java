package com.chuangjian.hire.label.service;


import com.chuangjian.hire.label.domain.LabelDO;

import java.util.List;
import java.util.Map;

/**
 * FileName: LabelBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/13 14:27
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface LabelService {

    List<LabelDO> getList();

    Map<String, LabelDO> getLabelMap();
}
