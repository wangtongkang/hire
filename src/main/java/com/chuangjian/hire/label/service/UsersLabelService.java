package com.chuangjian.hire.label.service;


import com.chuangjian.hire.label.dto.UsersLabelDTO;
import com.chuangjian.hire.users.dto.UserDTO;

import java.util.List;
import java.util.Map;

/**
 * FileName: UsersLabelBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/13 14:27
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface UsersLabelService {

    List<UsersLabelDTO> getUsersLabels(Long userId);

    Boolean addLabels(UsersLabelDTO usersLabel, UserDTO userInfo);

    Boolean removeLabels(UsersLabelDTO usersLabel, UserDTO userInfo);
}
