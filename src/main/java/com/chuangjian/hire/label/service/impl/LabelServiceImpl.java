//package com.chuangjian.hire.label.service.impl;
//
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.chuangjian.hire.label.dao.LabelMapper;
//import com.chuangjian.hire.label.domain.LabelDO;
//import com.chuangjian.hire.label.service.LabelService;
//import com.chuangjian.hire.product.dto.ProductUserDTO;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;
//
///**
// * FileName: LabelBusiness
// * Description:
// *
// * @author: CodeGenerator
// * @date: 2020/07/13 14:27
// * Copyright (C) 杭州同基汽车科技有限公司
// */
//@Transactional
//@Service("labelService")
//public class LabelServiceImpl extends ServiceImpl<LabelMapper, LabelDO> implements LabelService {
//
//    @Override
//    public List<LabelDO> getList() {
//        return list(Wrappers.<LabelDO>lambdaQuery().select(LabelDO::getId, LabelDO::getName));
//    }
//
//    @Override
//    public Map<String, LabelDO> getLabelMap() {
//        return getList().stream().collect(Collectors.toMap(it -> it.getId().toString(), it -> it));
//    }
//}
