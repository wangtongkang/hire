//package com.chuangjian.hire.label.service.impl;
//
//import cn.hutool.core.collection.CollectionUtil;
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.chuangjian.hire.label.dto.UsersLabelDTO;
//import com.chuangjian.hire.label.service.UsersLabelService;
//import com.chuangjian.hire.users.dao.UsersMapper;
//import com.chuangjian.hire.users.domain.UsersDO;
//import com.chuangjian.hire.users.dto.UserDTO;
//import com.chuangjian.hire.util.StringUtil;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.Collections;
//import java.util.List;
//import java.util.stream.Collectors;
//
///**
// * FileName: UsersLabelBusiness
// * Description:
// *
// * @author: CodeGenerator
// * @date: 2020/07/13 14:27
// * Copyright (C) 杭州同基汽车科技有限公司
// */
//@Transactional
//@Service("usersLabelService")
//public class UsersLabelServiceImpl extends ServiceImpl<UsersMapper, UsersDO> implements UsersLabelService {
//
//    @Override
//    public List<UsersLabelDTO> getUsersLabels(Long userId) {
//        UsersDO users = getById(userId);
//        List<String> labelList = StringUtil.getList(users.getLabelList());
//        if (CollectionUtil.isEmpty(labelList)) {
//            return Collections.emptyList();
//        }
//        return labelList.stream().map(it -> {
//            UsersLabelDTO label = new UsersLabelDTO();
//            label.setLabel(it);
//            label.setUserId(userId);
//            return label;
//        }).collect(Collectors.toList());
//    }
//
//    @Override
//    public Boolean addLabels(UsersLabelDTO usersLabel, UserDTO userInfo) {
//        UsersDO users = getById(userInfo.getId());
//        List<String> labelIdList = StringUtil.getList(users.getLabelList());
//        labelIdList.add(usersLabel.getLabel());
//        return update(Wrappers.<UsersDO>lambdaUpdate()
//                .set(UsersDO::getLabelList, StringUtil.getDBString(labelIdList))
//                .eq(UsersDO::getId, userInfo.getId()));
//    }
//
//
//    @Override
//    public Boolean removeLabels(UsersLabelDTO usersLabel, UserDTO userInfo) {
//        UsersDO users = getById(userInfo.getId());
//        List<String> labelIds = StringUtil.getList(users.getLabelList());
//        labelIds.remove(usersLabel.getLabel());
//        return update(Wrappers.<UsersDO>lambdaUpdate()
//                .set(UsersDO::getLabelList, StringUtil.getDBString(labelIds))
//                .eq(UsersDO::getId, userInfo.getId()));
//    }
//}
