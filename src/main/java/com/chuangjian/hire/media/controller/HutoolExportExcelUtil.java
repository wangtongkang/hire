package com.chuangjian.hire.media.controller;


import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import cn.hutool.poi.excel.StyleSet;
import io.swagger.annotations.ApiModelProperty;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.lang.reflect.Field;

/**
 * @author weizhigang
 * @date 2021年07月12日 14:25
 */
@Slf4j
public class HutoolExportExcelUtil {

    /**
     * 使用hutool导出excel
     *
     * @param clazz     对象
     * @param sheetName sheet名
     */
    public static ExcelWriter export(Class clazz, String sheetName) throws IOException {

        ExcelWriter writer = ExcelUtil.getWriter();
        // 获取当前类字段
        Field[] fields = clazz.getDeclaredFields();
        // 字段中文名称集合（获取实体中@ApiModelProperty注解value的值）
        for (Field field : fields) {
            if (!field.isAccessible()) {
                // 关闭反射访问安全检查，为了提高速度
                field.setAccessible(true);
            }
            // 排除ID和序号,如果不需要过滤字段直接将字段添加即可
            // 判断是否有@ApiModelProperty注解
            boolean annotationPresent = field.isAnnotationPresent(ApiModelProperty.class);
            if (annotationPresent) {
                ApiModelProperty annotation = field.getAnnotation(ApiModelProperty.class);
                if (annotation.hidden())
                    continue;
                String titleName = annotation.value();
                String fieldName = field.getName();
                writer.addHeaderAlias(fieldName, titleName);
            }
        }
        // 自动换行
        StyleSet styleSet = new StyleSet(writer.getWorkbook());
        styleSet.setWrapText();
        writer.setStyleSet(styleSet);
        //使用hutool时导出会自动带一个空的sheet1，需要将sheet重命名
        writer.renameSheet(sheetName);
        // 默认的，未添加alias的属性也会写出，如果想只写出加了别名的字段，可以调用此方法排除之
        writer.setOnlyAlias(true);
        return writer;
    }
}