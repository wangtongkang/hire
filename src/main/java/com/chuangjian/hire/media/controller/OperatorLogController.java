package com.chuangjian.hire.media.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.kisso.annotation.Login;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chuangjian.hire.common.controller.BaseController;
import com.chuangjian.hire.common.dto.FilterDTO;
import com.chuangjian.hire.common.dto.R;
import com.chuangjian.hire.media.dao.OperatorLogMapper;
import com.chuangjian.hire.media.domain.MediaItemDO;
import com.chuangjian.hire.media.domain.OperatorLogDO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Api(tags = "用户日志")
@RestController
@RequestMapping("/operator")
public class OperatorLogController extends BaseController {


    @Resource
    OperatorLogMapper operatorLogMapper;

    @PostMapping("pageList")
    @ApiOperation("获取列表")
    public R<OperatorLogDO> pageList(Page page, @RequestBody FilterDTO param) {
        Page pageResult = operatorLogMapper.selectPage(page,
                Wrappers.lambdaQuery(OperatorLogDO.class)
                        .like(StringUtils.isNotBlank(param.getCreator()), OperatorLogDO::getCreator, param.getCreator())
                        .ge(param.getStartDate() != null, OperatorLogDO::getCreateDate, param.getStartDate())
                        .lt(param.getEndDate() != null, OperatorLogDO::getCreateDate, param.getEndDate())
                        .orderByDesc(OperatorLogDO::getId)
        );

        List<MediaItemDO> records = pageResult.getRecords();
        if (CollectionUtil.isEmpty(records)) {
            return R.success(pageResult);
        }
        return R.success(pageResult);
    }


    @Login
    @PostMapping("save")
    @ApiOperation("保存操作日志")
    public R<Boolean> save(@RequestBody OperatorLogDO param) {


        return R.success(operatorLogMapper.insert(param));
    }
}

