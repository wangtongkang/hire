package com.chuangjian.hire.media.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.media.domain.MediaActorDO;


/**
 * FileName: CouponBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/14 11:28
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface MediaActorMapper extends BaseMapper<MediaActorDO> {

}
