package com.chuangjian.hire.media.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chuangjian.hire.media.domain.MediaItemDO;
import com.chuangjian.hire.media.dto.TypesDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * FileName: CouponBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/14 11:28
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface MediaItemMapper extends BaseMapper<MediaItemDO> {


    List<TypesDTO> typePageList(Page page, @Param("subType") String subType);
}
