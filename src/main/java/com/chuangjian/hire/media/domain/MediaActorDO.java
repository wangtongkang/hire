package com.chuangjian.hire.media.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsAutoIncrement;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsKey;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("m_media_actor")
public class MediaActorDO extends Model<MediaActorDO> {

    /**
     * 类目Id
     */
    @IsKey                         //actable主键注解
    @IsAutoIncrement             //自增
    @Column
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 编剧
     */
    @Column
    private String name;

    /**
     * 导演
     */
    @Column
    private String info;

    /**
     * 演员
     */
    @Column
    private String synopsis;

    /**
     * 集数
     */
    @Column
    private String url;

    /**
     * 时长
     */
    @Column
    private String href;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
