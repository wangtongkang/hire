package com.chuangjian.hire.media.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsAutoIncrement;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsKey;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("导出记录")
@TableName("m_export")
public class MediaExportDO extends Model<MediaExportDO> {

    @IsKey                         //actable主键注解
    @IsAutoIncrement             //自增
    @Column
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("id")
    private Long id;


    @Column
    @ApiModelProperty("报表名称")
    private String name;


    @Column
    @ApiModelProperty("导出URL")
    private String url;


    @ApiModelProperty("导出状态  0 导出中 1 已完成 2 已导出")
    @Column(defaultValue = "0")
    private Integer status;


    @ApiModelProperty("角色名称")
    @Column
    @TableField(value = "roleName", fill = FieldFill.INSERT)
    private java.lang.String roleName;
    /**
     * 创建人
     */
    @Column
    @TableField(value = "creator", fill = FieldFill.INSERT)
    private java.lang.String creator;
    /**
     * 创建人id
     */
    @Column
    @TableField(value = "creator_id", fill = FieldFill.INSERT)
    private java.lang.Long creatorId;


    @ApiModelProperty("导出时间")
    @Column(type = MySqlTypeConstant.DATETIME)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private java.util.Date createDate;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
