package com.chuangjian.hire.media.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.chuangjian.hire.common.base.BaseDO;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsAutoIncrement;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsKey;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

//    ProjectName 名称
//    Director 导演
//    Screen writer 编剧
//    Cast 主演
//    Episode 集数
//    Length 时长
//    Type 类型
//    Genres 题材
//    Year Of Production 制片年份
//    Synopsis 剧情梗概
//    Broadcasting Network 首播平台
//    Production Company 制片公司
//    Formats (Cann’t download) 视频画质（无法抓取）
//    Poster 海报
//    Stills 剧照
//    Trailers 片花链接
//    Screeners(Cann’t download) 样片链接（无法抓取）
//    Rating 评分
//    Awards 获奖及提名
@Data
@TableName("m_media_item")
@ApiModel("导出记录")
public class MediaItemDO extends BaseDO<MediaItemDO> {

    @IsKey                         //actable主键注解
    @IsAutoIncrement             //自增
    @Column
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("Id")
    private java.lang.Long id;

    @Column
    @ApiModelProperty("项目名称")
    private java.lang.String projectName;

    @Column
    @ApiModelProperty("年份")
    private java.lang.String year;

    @Column
    @ApiModelProperty("又名")
    private java.lang.String aka;

    @Column
    @ApiModelProperty("又名英文 En")
    private java.lang.String akaEn;

    @Column
    @ApiModelProperty("导演")
    private java.lang.String director;

    @Column
    @ApiModelProperty("编剧")
    private java.lang.String screenWriter;

    @Column
    @ApiModelProperty("演员")
    private java.lang.String cast;


    @Column
    @ApiModelProperty("集数")
    private java.lang.String episode;


    @Column
    @ApiModelProperty("时长")
    private java.lang.String length;
    /**
     * 类型
     */
    @Column
    private java.lang.String type;

    @Column
    @ApiModelProperty("类型")
    private java.lang.String subType;

    @Column
    @ApiModelProperty("题材")
    private java.lang.String genres;


    @Column
    @ApiModelProperty("制片年份")
    private java.lang.String yearOfProduction;

    @ApiModelProperty("剧情梗概")
    @Column(type = MySqlTypeConstant.TEXT)
    private java.lang.String synopsis;

    @Column
    @ApiModelProperty("首播平台")
    private java.lang.String broadcastingNetwork;


    @Column
    @ApiModelProperty("在线播放平台")
    private java.lang.String broadcastingNetworkOnline;


    @Column
    @ApiModelProperty("播出平台")
    private java.lang.String broadcastingPlatform;


    @Column
    @ApiModelProperty("制片公司")
    private java.lang.String productionCompany;

    @Column(defaultValue = "-1")
    @ApiModelProperty(value = "照片是否完成", hidden = true)
    private java.lang.Integer relPic;

    @Column(defaultValue = "-1")
    @ApiModelProperty(value = "相关视频是否完成", hidden = true)
    private java.lang.Integer relVd;

    @Column(defaultValue = "-1")
    @ApiModelProperty(value = "新闻相关", hidden = true)
    private java.lang.Integer relNew;

    @Column
    @ApiModelProperty(value = "评分")
    private java.lang.String rating;

    @Column
    @ApiModelProperty(value = "获奖及提名")
    private java.lang.String awards;

    @Column
    @ApiModelProperty(value = "制片国家/地区")
    private java.lang.String productionArea;

    @Column
    @ApiModelProperty(value = "语言", hidden = true)
    private java.lang.String language;

    @Column
    @ApiModelProperty(value = "首播时间")
    private java.lang.String broadcastingDate;

    @Column
    @ApiModelProperty(value = "imdb", hidden = true)
    private java.lang.String imdb;


    @Column
    @ApiModelProperty(value = "头像", hidden = true)
    private java.lang.String mainPic;

    @Column
    @ApiModelProperty(value = "豆瓣链接", hidden = true)
    private java.lang.String url;

    @Column
    @ApiModelProperty(value = "百度链接", hidden = true)
    private java.lang.String baiduUrl;


    @Column
    @ApiModelProperty(value = "MyDramaList", hidden = true)
    private java.lang.String dramaUrl;

    @Column
    @Index
    @ApiModelProperty(value = "中文版本ID", hidden = true)
    private java.lang.Long pid;

    /**
     * 是否已推送
     * 0 未推送
     * 1 已推送
     */
    @Column(defaultValue = "0")
    @ApiModelProperty(value = "是否已推送", hidden = true)
    private java.lang.Integer status;


    @Column(defaultValue = "1")
    @ApiModelProperty(value = "是否有效", hidden = true)
    private java.lang.Integer valid;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
