package com.chuangjian.hire.media.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsAutoIncrement;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsKey;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("m_media_relate")
public class MediaRelateDO extends Model<MediaRelateDO> {

    /**
     * 类目Id
     */
    @IsKey                         //actable主键注解
    @IsAutoIncrement             //自增
    @Column
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 编剧
     */
    @Column
    private String name;


    @Column
    @Index
    private Long pid;

    /**
     * 导演
     */
    @Column
    private String type;


    /**
     * 集数
     */
    @Column
    private String url;


    /**
     * 引用
     */
    @Column
    private String refs;


    /**
     * 排序
     */
    @Column(defaultValue = "1000")
    private Integer sor;
    /**
     * 时长
     */
    @Column
    @Index
    private String href;


    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private java.util.Date createDate;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
