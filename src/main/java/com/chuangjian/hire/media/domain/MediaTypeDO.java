package com.chuangjian.hire.media.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsAutoIncrement;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsKey;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("m_media_type")
public class MediaTypeDO extends Model<MediaTypeDO> {
    /**
     * 类目Id
     */
    @IsKey                         //actable主键注解
    @IsAutoIncrement             //自增
    @Column
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 名称
     */
    @Column
    private String projectName;
    /**
     * 导演
     */
    @Column
    private String actorId;
    /**
     * 编剧
     */
    @Column
    private String name;
    /**
     * 演员
     */
    @Column
    private String role;

    /**
     * 集数
     */
    @Column
    private String type;

    /**
     * 时长
     */
    @Column
    private Long pid;

    /**
     * 时长
     */
    @Column
    private String href;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
