package com.chuangjian.hire.media.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsAutoIncrement;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsKey;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("m_operator_log")
public class OperatorLogDO extends Model<OperatorLogDO> {
    /**
     * 类目Id
     */
    @IsKey                         //actable主键注解
    @IsAutoIncrement             //自增
    @Column
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 内容
     */
    @Column
    private String content;
    /**
     * 创建人
     */
    @Column
    @TableField(value = "creator", fill = FieldFill.INSERT)
    private String creator;
    /**
     * 创建人id
     */
    @Column
    @TableField(value = "creator_id", fill = FieldFill.INSERT)
    private Long creatorId;

    /**
     * 导出时间
     */
    @Column(type = MySqlTypeConstant.DATETIME)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private java.util.Date createDate;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
