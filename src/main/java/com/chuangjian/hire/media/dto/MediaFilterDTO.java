package com.chuangjian.hire.media.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("参数")
public class MediaFilterDTO {

    @ApiModelProperty("全文检索")
    private String projectName;

    @ApiModelProperty("类型")
    private String subType;

    @ApiModelProperty("抓取时间开始")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date creatDateStart;

    @ApiModelProperty("抓取时间结束")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date creatDateEnd;

    @ApiModelProperty("推送状态 0 未推送 1 已推送")
    private Integer status;

    @ApiModelProperty("制片年份开始")
    private String yearOfProductionStart;

    @ApiModelProperty("制片年份结束")
    private String yearOfProductionEnd;
}
