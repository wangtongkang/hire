package com.chuangjian.hire.media.dto;

import com.chuangjian.hire.media.domain.MediaItemDO;
import lombok.Data;

import java.util.List;

@Data
public class MediaItemDTO extends MediaItemDO {


    private java.lang.String mainPicUrl;

    /**
     * 剧照
     */
    private List<MediaRelateDTO> stillsList;

    /**
     * 海报
     */
    private List<MediaRelateDTO> posterList;

    /**
     * 片花链接
     */
    private List<MediaRelateDTO> trailersList;


    /**
     * 新闻消息
     */
    private List<MediaRelateDTO> newsList;


}
