package com.chuangjian.hire.media.dto;

import lombok.Data;

@Data
public class MediaItemUpdateDTO {

    private MediaItemDTO record1;
    private MediaItemDTO record2;
}
