package com.chuangjian.hire.media.dto;

import com.chuangjian.hire.media.domain.MediaRelateDO;
import lombok.Data;

@Data
public class MediaRelateDTO extends MediaRelateDO {

    /**
     * 集数
     */
    private String tmpUrl;


}
