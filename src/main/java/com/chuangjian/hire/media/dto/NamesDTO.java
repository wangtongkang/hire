package com.chuangjian.hire.media.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@ApiModel("新闻")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NamesDTO {

    @ApiModelProperty("片名")
    private List<String> names;
}
