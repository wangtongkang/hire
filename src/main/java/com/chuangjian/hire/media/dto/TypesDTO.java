package com.chuangjian.hire.media.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 打勾和选择框 键值对
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypesDTO {


    private String subType;
    private String genres;
    private String nums;
    private String genresNew;
}
