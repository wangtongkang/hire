package com.chuangjian.hire.media.service;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.io.File;

public class S3Service {


    static String bucket = "interlib";
    static String access_key = "AKIA2KTUN67IPYYWQD4X";
    static String secret_key = "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";
    static S3Client s3;

    static {
        AwsBasicCredentials awsCreds = AwsBasicCredentials.create(
                access_key,
                secret_key);
        s3 = S3Client.builder()
                .region(Region.AP_SOUTHEAST_1)
                .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
                .build();
    }


    public static S3Client getClient() {
        return s3;
    }


    public static String upload(String key, File file) {

        PutObjectRequest objectRequest = PutObjectRequest.builder()
                .bucket(bucket)
                .key(key)
                .build();

        s3.putObject(objectRequest, RequestBody.fromFile(file));
        return key;
    }


}
