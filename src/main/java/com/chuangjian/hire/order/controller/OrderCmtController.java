//package com.chuangjian.hire.order.controller;
//
//
//import com.baomidou.kisso.annotation.Login;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.chuangjian.hire.common.controller.BaseController;
//import com.chuangjian.hire.common.dto.R;
//import com.chuangjian.hire.order.domain.OrderCmtDO;
//import com.chuangjian.hire.order.service.OrderCmtService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//
//@Slf4j
//@Api(tags = "评论相关")
//@RestController
//@RequestMapping("/orderCmt")
//public class OrderCmtController extends BaseController {
//
//    @Resource
//    OrderCmtService orderCmtService;
//
//
//    @Login
//    @PostMapping("getPageList")
//    @ApiOperation(value = "列表", tags = {
//            "后台"
//    })
//    public R getPageList(Page page, @RequestBody OrderCmtDO param) {
//        return R.success(orderCmtService.getPageList(page, param));
//    }
//
//    @Login
//    @PostMapping("addOrUpdate")
//    @ApiOperation(value = "保存评论", tags = {
//            "后台"
//    })
//    public R addOrUpdate(@RequestBody OrderCmtDO param) {
//        return R.success(orderCmtService.addOrUpdate(param));
//    }
//
//    @Login
//    @PostMapping("deleteCmt")
//    @ApiOperation(value = "删除评论", tags = {
//            "后台"
//    })
//    public R deleteCmt(@RequestBody OrderCmtDO param) {
//        return R.success(orderCmtService.deleteCmt(param));
//    }
//}
//
