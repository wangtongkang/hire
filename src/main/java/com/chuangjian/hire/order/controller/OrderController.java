//package com.chuangjian.hire.order.controller;
//
//
//import com.alipay.api.AlipayApiException;
//import com.alipay.api.internal.util.AlipaySignature;
//import com.baomidou.kisso.annotation.Login;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.chuangjian.hire.common.controller.BaseController;
//import com.chuangjian.hire.common.dto.R;
//import com.chuangjian.hire.config.AliPayConfig;
//import com.chuangjian.hire.order.dto.*;
//import com.chuangjian.hire.order.service.*;
//import com.ijpay.alipay.AliPayApi;
//import com.ijpay.core.enums.SignType;
//import com.ijpay.core.kit.HttpKit;
//import com.ijpay.core.kit.WxPayKit;
//import com.ijpay.wxpay.WxPayApiConfigKit;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.*;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//import java.util.HashMap;
//import java.util.Map;
//
//@Slf4j
//@Api(tags = "订单相关")
//@RestController
//@RequestMapping("/order")
//public class OrderController extends BaseController {
//
//    @Resource
//    private OrderMainService orderMainService;
//    @Resource
//    OrderWorkService orderWorkService;
//    @Resource
//    OrderSuspendService orderSuspendService;
//    @Resource
//    OrderSignService orderSignService;
//    @Resource
//    OrderPayService orderPayService;
//    @Resource
//    AliPayConfig aliPayConfig;
//    @Resource
//    OrderCmtService orderCmtService;
//    @Resource
//    WechatService wechatService;
//
//    @Login
//    @PostMapping("createOrder")
//    @ApiOperation(value = "下单")
//    public R createOrder(@Validated @RequestBody OrderDTO order) {
//        return R.success(orderMainService.createOrder(order, getUserInfo()));
//    }
//
//    @Login
//    @PostMapping("payFailOrCancel")
//    @ApiOperation(value = "取消 订单")
//    public R payFailOrCancel(@Validated @RequestBody OrderDTO order) {
//        return R.success(orderMainService.payFailOrCancel(order.getId(), false));
//    }
//
//
//    @Login
//    @PostMapping("createPay")
//    @ApiOperation(value = "支付")
//    public R createPay(@Validated @RequestBody PayDTO pay) {
//        return R.success(orderPayService.createPay(pay));
//    }
//
//    @PostMapping("wxNotify")
//    @ApiOperation(value = "微信支付回调")
//    public String wxNotify(HttpServletRequest request) {
//        orderPayService.initPay();
//        String xmlMsg = HttpKit.readData(request);
//        log.info("微信支付通知=" + xmlMsg);
//        Map<String, String> params = WxPayKit.xmlToMap(xmlMsg);
//
//        String returnCode = params.get("return_code");
//
//        // 注意重复通知的情况，同一订单号可能收到多次通知，请注意一定先判断订单状态
//        // 注意此处签名方式需与统一下单的签名类型一致
//        if (WxPayKit.verifyNotify(params, WxPayApiConfigKit.getWxPayApiConfig().getPartnerKey(), SignType.HMACSHA256)) {
//            if (WxPayKit.codeIsOk(returnCode)) {
//                orderPayService.wxNotify(params);
//                // 更新订单信息
//                // 发送通知等
//                Map<String, String> xml = new HashMap<String, String>(2);
//                xml.put("return_code", "SUCCESS");
//                xml.put("return_msg", "OK");
//                return WxPayKit.toXml(xml);
//            }
//        }
//        return null;
//    }
//
//    @PostMapping("aliNotify")
//    @ApiOperation(value = "支付宝支付回调")
//    public String aliNotify(HttpServletRequest request) {
//        try {
//            // 获取支付宝POST过来反馈信息
//            Map<String, String> params = AliPayApi.toMap(request);
//
//            for (Map.Entry<String, String> entry : params.entrySet()) {
//                log.info("支付宝回调参数:{}", entry.getKey() + " = " + entry.getValue());
//            }
//
//            boolean verifyResult = AlipaySignature.rsaCheckV1(params, aliPayConfig.getPublicKey(), "UTF-8", "RSA2");
//
//            if (verifyResult) {
//                orderPayService.aliNotify(params);
//                // TODO 请在这里加上商户的业务逻辑程序代码 异步通知可能出现订单重复通知 需要做去重处理
//                log.info("aliNotify 验证成功succcess");
//                return "success";
//            } else {
//                log.info("aliNotify 验证失败");
//                return "failure";
//            }
//        } catch (AlipayApiException e) {
//            log.error("支付保回调异常:", e);
//            return "failure";
//        }
//    }
//
//
//    @GetMapping("getInitJSSDKData")
//    @ApiOperation(value = "初始化JS")
//    public R getInitJSSDKData(String url) {
//        return R.success(wechatService.getInitJSSDKData(url));
//    }
//
//    @GetMapping("getUserOpenId")
//    @ApiOperation(value = "获取openId")
//    public R getUserOpenId(String code) {
//        return R.success(wechatService.getUserOpenId(code));
//    }
//
//
//    @Login
//    @PostMapping("getOrderInfoById")
//    @ApiOperation(value = "获取订单详情")
//    public R getOrderInfoById(@RequestBody OrderDTO order) {
//        return R.success(orderMainService.getOrderInfoById(order.getId()));
//    }
//
//    @Login
//    @PostMapping("getPayingOrderInfo")
//    @ApiOperation(value = "获取未支付订单详情")
//    public R getPayingOrderInfo() {
//        return R.success(orderMainService.getPayingOrderInfo(getUserInfo().getId()));
//    }
//
//
//    @Login
//    @PostMapping("hirePay")
//    @ApiOperation(value = "发工资")
//    public R hirePay(@Validated @RequestBody HirePayDTO pay) {
//        return R.success(
//                orderMainService.hirePay(
//                        pay.getOrderId(),
//                        pay.getPayAmount(),
//                        pay.getFireRemark(),
//                        pay.getPayPwd(),
//                        getUserInfo().getId())
//        );
//
//    }
//
//    @Login
//    @PostMapping("hirePayConfirm")
//    @ApiOperation(value = "确认发工资")
//    public R hirePayConfirm(@Validated @RequestBody HirePayDTO pay) {
//        return R.success(orderMainService.hirePayConfirm(pay.getOrderId(), getUserInfo().getId()));
//    }
//
//    @Login
//    @PostMapping("employerCmt")
//    @ApiOperation(value = "雇主评价")
//    public R employerCmt(@Validated @RequestBody OrderCmtDTO orderCmt) {
//        return R.success(orderMainService.employerCmt(orderCmt));
//    }
//
//    @Login
//    @PostMapping("employeeCmt")
//    @ApiOperation(value = "设计师添加雇主印象")
//    public R employeeCmt(@Validated @RequestBody OrderCmtDTO orderCmt) {
//        return R.success(orderMainService.employeeCmt(orderCmt));
//    }
//
//
//    @Login
//    @PostMapping("applyFire")
//    @ApiOperation(value = "雇主 申请解雇")
//    public R applyFire(@Validated @RequestBody FireDTO fire) {
//        return R.success(orderMainService.applyFire(fire, getUserInfo()));
//    }
//
//    @Login
//    @PostMapping("getFireInfo")
//    @ApiOperation(value = "设计师 获取解雇备注")
//    public R getFireInfo(@RequestBody FireDTO fire) {
//        return R.success(orderMainService.getFireInfo(fire.getOrderId()));
//    }
//
//    @Login
//    @PostMapping("fireAgree")
//    @ApiOperation(value = "设计师 是否同意解雇")
//    public R fireAgree(@RequestBody FireDTO fire) {
//        return R.success(orderMainService.fireAgree(fire, getUserInfo()));
//    }
//
//
//    @PostMapping("getCmtByEmployeeId")
//    @ApiOperation(value = "获取设计师的评价")
//    public R getCmtByEmployeeId(Page page, Long employeeId) {
//        return R.success(orderCmtService.getCmtByEmployeeId(page, employeeId));
//    }
//
//    @Login
//    @PostMapping("getOrderWorkList")
//    @ApiOperation(value = "获取订单 已经有的工作")
//    public R getOrderWorkList(@RequestBody OrderWorkDTO orderWork) {
//        return R.success(orderWorkService.getOrderWorkList(orderWork.getOrderId()));
//    }
//
//    @Login
//    @PostMapping("addOrderWork")
//    @ApiOperation(value = "雇主添加工作")
//    public R addOrderWork(@Validated @RequestBody OrderWorkDTO orderWork) {
//        return R.success(orderWorkService.addOrderWork(orderWork));
//    }
//
//
//    @Login
//    @PostMapping("cancelWork")
//    @ApiOperation(value = "雇主 取消工作")
//    public R cancelWork(@RequestBody OrderWorkDTO orderWork) {
//        return R.success(orderWorkService.cancelWork(orderWork));
//    }
//
//    @Login
//    @PostMapping("confirmWork")
//    @ApiOperation(value = "雇主 确认工作")
//    public R confirmWork(@RequestBody OrderWorkDTO orderWork) {
//        return R.success(orderWorkService.confirmWork(orderWork));
//    }
//
//
//    @Login
//    @PostMapping("finishWork")
//    @ApiOperation(value = "设计师 完成工作")
//    public R finishWork(@RequestBody OrderWorkDTO orderWork) {
//        return R.success(orderWorkService.finishWork(orderWork));
//    }
//
//
//    @Login
//    @PostMapping("employeeSuspend")
//    @ApiOperation(value = "设计师 请假申请")
//    public R employeeSuspend(@Validated @RequestBody SuspendDTO suspend) {
//        return R.success(orderSuspendService.employeeSuspend(suspend, getUserInfo()));
//    }
//
//    @Login
//    @PostMapping("employerSuspend")
//    @ApiOperation(value = "雇主 暂停申请")
//    public R employerSuspend(@Validated @RequestBody SuspendDTO suspend) {
//        return R.success(orderSuspendService.employerSuspend(suspend, getUserInfo()));
//    }
//
//
//    @Login
//    @PostMapping("getSuspendById")
//    @ApiOperation(value = "雇主或者设计师 获取暂停详情")
//    public R getSuspendById(@RequestBody SuspendDTO suspend) {
//        return R.success(orderSuspendService.getSuspendById(suspend.getId()));
//    }
//
//    @Login
//    @PostMapping("employerSuspendCancel")
//    @ApiOperation(value = "雇主 取消暂停")
//    public R employerSuspendCancel(@RequestBody SuspendDTO suspend) {
//        return R.success(orderSuspendService.employerSuspendCancel(suspend, getUserInfo()));
//    }
//
//
//    @Login
//    @PostMapping("auditSuspendApply")
//    @ApiOperation(value = "设计师或者雇主 同意或者不同意 暂停申请")
//    public R auditSuspendApply(@RequestBody SuspendDTO suspend) {
//        return R.success(orderSuspendService.auditSuspendApply(suspend, getUserInfo()));
//    }
//
//
//    @Login
//    @GetMapping("getUnSignList")
//    @ApiOperation(value = "设计师 获取 未签到工作")
//    public R getUnSignList() {
//        return R.success(orderSignService.getUnSignList(getUserInfo().getId()));
//    }
//
//    @Login
//    @PostMapping("signList")
//    @ApiOperation(value = "设计师 签到")
//    public R signList(@RequestBody SignListDTO sign) {
//        return R.success(orderSignService.signList(sign.getOrderIdList()));
//    }
//
//
//    @Login
//    @PostMapping("getOverviewByEmployerId")
//    @ApiOperation(value = "雇主获取概览 ")
//    public R getOverviewByEmployerId() {
//        return R.success(orderMainService.getOverviewByEmployerId(getUserInfo().getId()));
//    }
//
//
//    @Login
//    @PostMapping("getMyEmployeePage")
//    @ApiOperation(value = "雇主 我的员工")
//    public R getMyEmployeePage(Page page, @RequestBody OrderListParam param) {
//        return R.success(orderMainService.getMyEmployeePage(page, param, getUserInfo().getId()));
//    }
//
//
//    @Login
//    @PostMapping("getMyEmployerPage")
//    @ApiOperation(value = "设计师 工作项目")
//    public R getMyEmployerPage(Page page, @RequestBody OrderListParam param) {
//        return R.success(orderMainService.getMyEmployerPage(page, param, getUserInfo().getId()));
//    }
//
//    @Login
//    @PostMapping("getOverviewByEmployeeId")
//    @ApiOperation(value = "设计师 获取概览")
//    public R getOverviewByEmployeeId() {
//        return R.success(orderMainService.getOverviewByEmployeeId(getUserInfo().getId()));
//    }
//
//    @Login
//    @PostMapping("getMyChargePage")
//    @ApiOperation(value = "充值记录")
//    public R getMyChargePage(Page page, @RequestBody OrderParam param) {
//        return R.success(orderMainService.getMyChargePage(page, param, getUserInfo().getId()));
//    }
//
//    @Login
//    @PostMapping("getMyEmployPage")
//    @ApiOperation(value = "雇佣记录")
//    public R getMyEmployPage(Page page, @RequestBody OrderParam param) {
//        return R.success(orderMainService.getMyEmployPage(page, param, getUserInfo().getId()));
//    }
//
//    @Login
//    @PostMapping("getOrderPagePlatform")
//    @ApiOperation(value = "订单列表", tags = {
//            "后台"
//    })
//    public R getOrderPagePlatform(Page page, @RequestBody OrderMainPOParam param) {
//        return R.success(orderMainService.getOrderPagePlatform(page, param));
//    }
//
//    @Login
//    @PostMapping("getHirePagePlatform")
//    @ApiOperation(value = "雇佣列表", tags = {
//            "后台"
//    })
//    public R getHirePagePlatform(Page page, @RequestBody OrderHirePOParam param) {
//        return R.success(orderMainService.getHirePagePlatform(page, param));
//    }
//
//    @Login
//    @PostMapping("changeOrderAmount")
//    @ApiOperation(value = "修改订单金额", tags = {
//            "后台"
//    })
//    public R changeOrderAmount(@Validated @RequestBody OrderAmountParam orderAmountParam) {
//        return R.success(orderMainService.changeOrderAmount(orderAmountParam));
//    }
//
//    @Login
//    @PostMapping("hirePayPlatform")
//    @ApiOperation(value = "后台支付雇佣单", tags = {
//            "后台"
//    })
//    public R hirePayPlatform(@Validated @RequestBody PayPlatformDTO param) {
//        return R.success(orderMainService.hirePayPlatform(param));
//    }
//
//    @Login
//    @PostMapping("changeWorkTypePlatform")
//    @ApiOperation(value = "修改工作类型", tags = {
//            "后台"
//    })
//    public R changeWorkTypePlatform(@Validated @RequestBody WorkTypeParam param) {
//        return R.success(orderMainService.changeWorkTypePlatform(param));
//    }
//}
//
