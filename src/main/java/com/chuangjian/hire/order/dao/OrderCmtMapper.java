package com.chuangjian.hire.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.order.domain.OrderCmtDO;


/**
* FileName: OrderCmtBusiness
* Description:
* @author: CodeGenerator
* @date: 2020/07/14 20:51
* Copyright (C) 杭州同基汽车科技有限公司
*/
public interface OrderCmtMapper extends BaseMapper<OrderCmtDO> {

}
