package com.chuangjian.hire.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.order.domain.OrderPayDO;


/**
* FileName: OrderPayBusiness
* Description:
* @author: CodeGenerator
* @date: 2020/07/18 15:50
* Copyright (C) 杭州同基汽车科技有限公司
*/
public interface OrderPayMapper extends BaseMapper<OrderPayDO> {

}
