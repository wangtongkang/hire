package com.chuangjian.hire.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.order.domain.OrderSignDO;
import com.chuangjian.hire.order.dto.SignDTO;

import java.util.List;


/**
 * FileName: OrderSignBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/17 09:08
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface OrderSignMapper extends BaseMapper<OrderSignDO> {


    List<SignDTO> getUnSign(Long userId);
}
