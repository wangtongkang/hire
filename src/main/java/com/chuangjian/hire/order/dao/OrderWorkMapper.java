package com.chuangjian.hire.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.order.domain.OrderWorkDO;


/**
* FileName: OrderWorkBusiness
* Description:
* @author: CodeGenerator
* @date: 2020/07/13 22:56
* Copyright (C) 杭州同基汽车科技有限公司
*/
public interface OrderWorkMapper extends BaseMapper<OrderWorkDO> {

}
