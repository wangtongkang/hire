package com.chuangjian.hire.order.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;

/**
* FileName: OrderCmtBusiness
* Description:
* @author: CodeGenerator
* @date: 2020/07/14 20:51
* Copyright (C) 杭州同基汽车科技有限公司
*/
@Data
@TableName("order_cmt")
public class OrderCmtDO extends Model<OrderCmtDO> {

	/**
	* labelId
	*/
	@TableId(type = IdType.AUTO)
	private java.lang.Long id;
	/**
	* 订单ID
	*/
	private java.lang.Long orderId;
	/**
	* 评价人ID
	*/
	private java.lang.Long userId;
	/**
	* 评价人
	*/
	private java.lang.String userName;
	/**
	* 评价人头像
	*/
	private java.lang.String userIcon;
	/**
	* 被评价人ID
	*/
	private java.lang.Long cmtId;
	/**
	* 被评价人
	*/
	private java.lang.String cmtName;
	/**
	* 评价
	*/
	private java.lang.String remark;
	/**
	* 评价图片
	*/
	private java.lang.String remarkUrl;
	/**
	* 评价等级 1-5
	*/
	private java.math.BigDecimal grade;
	/**
	* 创建人
	*/
	@TableField(value = "creator", fill = FieldFill.INSERT)
	private java.lang.String creator;
	/**
	* 创建人id
	*/
	@TableField(value = "creator_id", fill = FieldFill.INSERT)
	private java.lang.Long creatorId;
	/**
	* 创建时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField(value = "create_date", fill = FieldFill.INSERT)
	private java.util.Date createDate;
	/**
	* 修改人id
	*/
	@TableField(value = "modifier_id", fill = FieldFill.UPDATE)
	private java.lang.Long modifierId;
	/**
	* 修改人
	*/
	@TableField(value = "modifier", fill = FieldFill.UPDATE)
	private java.lang.String modifier;
	/**
	* 修改时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField(value = "modify_date", fill = FieldFill.UPDATE)
	private java.util.Date modifyDate;
	/**
	* 是否有效，0.删除，1.有效
	*/
	@TableLogic
	private java.lang.Boolean valid;

    @Override
    protected Serializable pkVal() {
    	return this.id;
    }
}
