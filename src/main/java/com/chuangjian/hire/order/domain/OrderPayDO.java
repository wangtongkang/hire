package com.chuangjian.hire.order.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;

/**
* FileName: OrderPayBusiness
* Description:
* @author: CodeGenerator
* @date: 2020/07/20 13:49
* Copyright (C) 杭州同基汽车科技有限公司
*/
@Data
@TableName("order_pay")
public class OrderPayDO extends Model<OrderPayDO> {

	/**
	* labelId
	*/
	@TableId(type = IdType.AUTO)
	private java.lang.Long id;
	/**
	* 订单ID
	*/
	private java.lang.Long orderId;
	/**
	* 支付金额
	*/
	private java.math.BigDecimal amount;
	/**
	* 支付单号
	*/
	private java.lang.String code;
	/**
	* 订单状态 10, 待支付 20,已支付 30 过期 40 已取消
	*/
	private java.lang.Integer status;
	/**
	* 外部支付单号
	*/
	private java.lang.String outCode;
	/**
	* 支付类型 10,支付宝web,11支付宝h5,20微信web,21微信公众号支付
	*/
	private java.lang.Integer payType;
	/**
	* 支付结果
	*/
	private java.lang.String payResult;
	/**
	* 创建人
	*/
	@TableField(value = "creator", fill = FieldFill.INSERT)
	private java.lang.String creator;
	/**
	* 创建人id
	*/
	@TableField(value = "creator_id", fill = FieldFill.INSERT)
	private java.lang.Long creatorId;
	/**
	* 创建时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField(value = "create_date", fill = FieldFill.INSERT)
	private java.util.Date createDate;
	/**
	* 修改人id
	*/
	@TableField(value = "modifier_id", fill = FieldFill.UPDATE)
	private java.lang.Long modifierId;
	/**
	* 修改人
	*/
	@TableField(value = "modifier", fill = FieldFill.UPDATE)
	private java.lang.String modifier;
	/**
	* 修改时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField(value = "modify_date", fill = FieldFill.UPDATE)
	private java.util.Date modifyDate;
	/**
	* 是否有效，0.删除，1.有效
	*/
	@TableLogic
	private java.lang.Boolean valid;

    @Override
    protected Serializable pkVal() {
    	return this.id;
    }
}
