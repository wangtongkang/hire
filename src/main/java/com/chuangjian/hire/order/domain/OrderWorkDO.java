package com.chuangjian.hire.order.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;

/**
* FileName: OrderWorkBusiness
* Description:
* @author: CodeGenerator
* @date: 2020/07/15 22:06
* Copyright (C) 杭州同基汽车科技有限公司
*/
@Data
@TableName("order_work")
public class OrderWorkDO extends Model<OrderWorkDO> {

	/**
	* 工作id
	*/
	@TableId(type = IdType.AUTO)
	private java.lang.Long id;
	/**
	* 订单ID
	*/
	private java.lang.Long orderId;
	/**
	* 雇佣者
	*/
	private java.lang.String employer;
	/**
	* 雇佣者ID
	*/
	private java.lang.Long employerId;
	/**
	* 被雇佣者
	*/
	private java.lang.String employee;
	/**
	* 被雇佣者ID
	*/
	private java.lang.Long employeeId;
	/**
	* 工作状态 10,工作中 20,已完成 30,已经取消
	*/
	private java.lang.Integer status;
	/**
	* 区域
	*/
	private java.lang.String area;
	/**
	* 区域Id
	*/
	private java.lang.Long areaId;
	/**
	* 工作要求
	*/
	private java.lang.String intro;
	/**
	* 开始时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private java.util.Date startDate;
	/**
	* 结束时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private java.util.Date endDate;
	/**
	* 创建人
	*/
	@TableField(value = "creator", fill = FieldFill.INSERT)
	private java.lang.String creator;
	/**
	* 创建人id
	*/
	@TableField(value = "creator_id", fill = FieldFill.INSERT)
	private java.lang.Long creatorId;
	/**
	* 创建时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField(value = "create_date", fill = FieldFill.INSERT)
	private java.util.Date createDate;
	/**
	* 修改人id
	*/
	@TableField(value = "modifier_id", fill = FieldFill.UPDATE)
	private java.lang.Long modifierId;
	/**
	* 修改人
	*/
	@TableField(value = "modifier", fill = FieldFill.UPDATE)
	private java.lang.String modifier;
	/**
	* 修改时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField(value = "modify_date", fill = FieldFill.UPDATE)
	private java.util.Date modifyDate;
	/**
	* 是否有效，0.删除，1.有效
	*/
	@TableLogic
	private java.lang.Boolean valid;

    @Override
    protected Serializable pkVal() {
    	return this.id;
    }
}
