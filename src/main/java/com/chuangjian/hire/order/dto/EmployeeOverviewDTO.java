package com.chuangjian.hire.order.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("设计师总览")
public class EmployeeOverviewDTO {

    @ApiModelProperty("项目数量")
    private Long orderMainCount = 0L;
    @ApiModelProperty("雇佣总金额")
    private BigDecimal orderMainHistoryAmount = BigDecimal.ZERO;
    @ApiModelProperty("当前雇佣金额")
    private BigDecimal orderMainAmount = BigDecimal.ZERO;
    @ApiModelProperty("包月完结")
    private Long payMonthCount = 0L;
    @ApiModelProperty("定制完结")
    private Long payCustomCount = 0L;
    @ApiModelProperty("非正常完结")
    private Long fireCount = 0L;
}
