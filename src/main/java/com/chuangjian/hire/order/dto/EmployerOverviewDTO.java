package com.chuangjian.hire.order.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("雇主总览")
public class EmployerOverviewDTO {

    @ApiModelProperty("我的员工")
    private Long employeeCount = 0L;
    @ApiModelProperty("历史雇佣记录")
    private Integer employeeHistoryCount = 0;
    @ApiModelProperty("托管工资")
    private BigDecimal orderMainAmount = BigDecimal.ZERO;
    @ApiModelProperty("当前未完成工作")
    private Integer unfinishedCount = 0;
    @ApiModelProperty("待发工资")
    private BigDecimal unPayAmount = BigDecimal.ZERO;
}
