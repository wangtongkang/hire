package com.chuangjian.hire.order.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel("解雇")
public class FireDTO {

    @ApiModelProperty("订单Id")
    @NotNull(message = "订单id 不能为空")
    private Long orderId;

    @ApiModelProperty("设计师签到天数")
    private Integer getSignDay;

    @ApiModelProperty("应支付 解雇金额")
    BigDecimal duePayAmount;

    @ApiModelProperty("开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private java.util.Date startDate;

    @ApiModelProperty("结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private java.util.Date endDate;


    @ApiModelProperty("解雇备注")
    @NotNull(message = "解雇备注 不能为空")
    private String fireRemark;

    @ApiModelProperty("解雇金额")
    @NotNull(message = "解雇金额 不能为空")
    BigDecimal payAmount;

    @ApiModelProperty("是否解雇")
    @NotNull(message = "是否解雇 不能为空")
    private Boolean fire;
}
