package com.chuangjian.hire.order.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel("发工资")
public class HirePayDTO {

    @ApiModelProperty("订单Id")
    @NotNull(message = "订单id不能为空")
    private Long orderId;

    /**
     * 订单金额
     */
    @NotNull(message = "订单支付金额不能为空")
    @ApiModelProperty("订单支付金额")
    BigDecimal payAmount;

    @ApiModelProperty("支付密码")
    @NotNull(message = "支付密码不能为空")
    String payPwd;

    @ApiModelProperty("工资备注")
    private String fireRemark;

}
