package com.chuangjian.hire.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * FileName: OrderMainBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/18 16:26
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
public class OrderAmountParam {

    @ApiModelProperty("订单ID")
    @NotNull(message = "订单ID 不能为空")
    private Long id;

    @ApiModelProperty("订单金额")
    @NotNull(message = "订单金额 不能为空")
    private java.math.BigDecimal amount;
}
