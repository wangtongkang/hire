package com.chuangjian.hire.order.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("评价")
public class OrderCmtDTO {

    @ApiModelProperty("订单Id")
    @NotNull(message = "订单id不能为空")
    private Long orderId;

    @ApiModelProperty("评价")
    @NotNull(message = "评价 不能为空")
    private java.lang.String remark;

    @ApiModelProperty("评评价图片价")
    private java.lang.String remarkUrl;

    @ApiModelProperty("评价等级 1-5")
    @NotNull(message = "评价等级 1-5 不能为空")
    private java.math.BigDecimal grade;

    @ApiModelProperty("评价人ID")
    private java.lang.Long userId;

    @ApiModelProperty("评价人姓名")
    private java.lang.String userName;

    @ApiModelProperty("评价人icon")
    private java.lang.String userIcon;
}
