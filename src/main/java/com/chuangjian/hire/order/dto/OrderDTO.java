package com.chuangjian.hire.order.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("订单")
public class OrderDTO {
    /**
     * 订单Id
     */
    @ApiModelProperty("订单Id")
    private java.lang.Long id;
    /**
     * 订单类型(1,包月,2,定制,3,充值)
     */
    @ApiModelProperty("(1,包月,2,定制,3,充值)")
    private java.lang.Integer type;

    @ApiModelProperty("订单状态 10, 待支付 20,已支付 30 过期 40 已取消")
    private java.lang.Integer status;
    /**
     * 订单金额
     */
    @ApiModelProperty("订单金额")
    private java.math.BigDecimal amount;
    /**
     * 支付金额
     */
    @ApiModelProperty("实际支付金额")
    private java.math.BigDecimal actualPay;
    /**
     * 优惠券ID
     */
    @ApiModelProperty("优惠券ID")
    private java.lang.Long userCouponId;
    /**
     * 优惠券抵扣金额
     */
    @ApiModelProperty("优惠券抵扣金额")
    private java.math.BigDecimal userCouponAmount;
    @ApiModelProperty("被雇佣者")
    private java.lang.String employee;
    /**
     * 被雇佣者ID
     */
    @ApiModelProperty("被雇佣者ID")
    private java.lang.Long employeeId;
    /**
     * 雇佣开始时间
     */
    @ApiModelProperty("雇佣开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private java.util.Date startDate;
    /**
     * 雇佣结束时间
     */
    @ApiModelProperty("雇佣结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private java.util.Date endDate;

    @ApiModelProperty("包月雇佣月份")
    private java.lang.Integer workMonth;
    /**
     * 订单备注
     */
    @ApiModelProperty("订单备注")
    private java.lang.String remark;
}
