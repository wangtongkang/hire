package com.chuangjian.hire.order.dto;

import lombok.Data;

/**
 * FileName: OrderMainBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/18 16:26
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
public class OrderHirePOParam {

    /**
     * 雇佣单号
     */
    private String code;
    /**
     * 雇佣类型 1,未支付 10,进行中 20,暂停中 30,待完结 40,已完结 50,已解雇
     */
    private Integer empStatus;
    /**
     * 雇佣者
     */
    private String employer;
    /**
     * 被雇佣者
     */
    private String employee;
}
