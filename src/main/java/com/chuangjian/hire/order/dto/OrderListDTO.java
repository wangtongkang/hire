package com.chuangjian.hire.order.dto;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.chuangjian.hire.product.dto.ProductUserDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("订单")
public class OrderListDTO {

    @ApiModelProperty("订单Id")
    private Long id;

    @ApiModelProperty("订单号")
    private java.lang.String code;

    @ApiModelProperty("(1,包月,2,定制,3,充值)")
    private Integer type;

    @ApiModelProperty("订单状态 10, 待支付 20,已支付 30 过期 40 已取消")
    private java.lang.Integer status;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private java.util.Date createDate;

    @ApiModelProperty("订单金额")
    private java.math.BigDecimal amount;

    @ApiModelProperty("雇佣状态 1,未支付 10,进行中 20,暂停中 30,待完结 40,已完结 50,已解雇")
    private java.lang.Integer empStatus;

    @ApiModelProperty("雇佣者ID")
    private Long employerId;

    @ApiModelProperty("雇佣者ID")
    private ProductUserDTO employer;

    @ApiModelProperty("被雇佣者ID")
    private Long employeeId;

    @ApiModelProperty("被雇佣者")
    private ProductUserDTO employee;

    @ApiModelProperty("雇佣开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private java.util.Date startDate;

    @ApiModelProperty("雇佣结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private java.util.Date endDate;

    private java.lang.String fireRemark;

    @ApiModelProperty("雇主暂停Id")
    private java.lang.Long employerSuspendId;
    @ApiModelProperty("员工请假Id")
    private java.lang.Long employeeSuspendId;

    @ApiModelProperty("雇主是否评价 0.未评价 1,已经评价")
    private java.lang.Boolean employerCmt;
    @ApiModelProperty("设计师是否评价 0.未评价 1,已经评价")
    private java.lang.Boolean employeeCmt;

    @ApiModelProperty("工作类型 1,单休 2,双休")
    private java.lang.Integer workType;
}
