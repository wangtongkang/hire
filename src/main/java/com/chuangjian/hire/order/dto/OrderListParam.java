package com.chuangjian.hire.order.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("订单请求参数")
public class OrderListParam {

    @ApiModelProperty("是否历史雇佣")
    private Boolean isHistory = false;


    @ApiModelProperty("雇佣姓名")
    private String employName;

    @ApiModelProperty("雇佣状态 1,未支付 10,进行中 20,暂停中 30,待完结 40,已完结 50,已解雇")
    private java.lang.Integer empStatus;

    @ApiModelProperty("雇佣开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private java.util.Date startDate;


    @ApiModelProperty("雇佣结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private java.util.Date endDate;
}
