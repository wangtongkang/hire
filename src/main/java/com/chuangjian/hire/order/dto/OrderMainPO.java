package com.chuangjian.hire.order.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * FileName: OrderMainBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/18 16:26
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
public class OrderMainPO {

    /**
     * 订单ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 订单号
     */
    private String code;
    /**
     * 订单类型(1,包月,2,定制,3,充值)
     */
    private Integer type;
    /**
     * 订单状态 10, 待支付 20,已支付 30 过期 40 已取消
     */
    private Integer status;
    /**
     * 订单金额
     */
    private java.math.BigDecimal amount;
    /**
     * 支付金额
     */
    private java.math.BigDecimal actualPay;
    /**
     * 支付类型 10,支付宝web,11支付宝h5,20微信web,21微信公众号支付
     */
    private Integer payType;
    /**
     * 优惠券抵扣金额
     */
    private java.math.BigDecimal userCouponAmount;
    /**
     * 下单人
     */
    private java.lang.String employer;

    /**
     * 被雇佣者
     */
    private java.lang.String employee;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date createDate;
}
