package com.chuangjian.hire.order.dto;

import lombok.Data;

/**
 * FileName: OrderMainBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/18 16:26
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
public class OrderMainPOParam {

    /**
     * 订单号
     */
    private String code;
    /**
     * 订单类型(1,包月,2,定制,3,充值)
     */
    private Integer type;
    /**
     * 订单状态 10, 待支付 20,已支付 30 过期 40 已取消
     */
    private Integer status;
    /**
     * 下单人
     */
    private String employer;
    /**
     * 被雇佣者
     */
    private String employee;
}
