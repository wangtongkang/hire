package com.chuangjian.hire.order.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("支付")
public class PayDTO {

    @ApiModelProperty("订单Id")
    @NotNull(message = "订单id不能为空")
    private Long orderId;

    @ApiModelProperty("支付类型 10,支付宝web,11支付宝h5,20微信web,21微信公众号支付 30,余额支付")
    @NotNull(message = "支付类型 不能为空")
    private java.lang.Integer payType;

    @ApiModelProperty("支付成功跳转页面")
    private java.lang.String returnUrl;

    @ApiModelProperty(" 微信公众号支付 将用户的openId")
    private java.lang.String openId;
}
