package com.chuangjian.hire.order.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel("后台支付")
public class PayPlatformDTO {

    @ApiModelProperty("订单Id")
    @NotNull(message = "订单id 不能为空")
    private Long orderId;

    @ApiModelProperty("支付金额")
    @NotNull(message = "支付金额 不能为空")
    BigDecimal duePayAmount;

    @ApiModelProperty("支付备注")
    @NotNull(message = "支付备注 不能为空")
    private String fireRemark;

    @ApiModelProperty("订单状态 40 已完结 50 已解雇")
    @NotNull(message = "订单状态 不能为空")
    private Integer empStatus;

}
