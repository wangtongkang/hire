package com.chuangjian.hire.order.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("发工资")
public class SignDTO {

    @ApiModelProperty("订单Id")
    @NotNull(message = "订单id不能为空")
    private Long orderId;
    @ApiModelProperty("雇佣者")
    private java.lang.String employer;
    @ApiModelProperty("雇佣者ID")
    private java.lang.Long employerId;
}
