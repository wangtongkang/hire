package com.chuangjian.hire.order.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ApiModel("签到")
public class SignListDTO {
    @NotNull(message = "签到参数不能为空")
    List<Long> orderIdList;
}
