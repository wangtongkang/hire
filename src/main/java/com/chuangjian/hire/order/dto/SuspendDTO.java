package com.chuangjian.hire.order.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel("请假")
public class SuspendDTO {

    @ApiModelProperty("请假Id")
    private java.lang.Long id;

    @ApiModelProperty("订单Id")
    @NotNull(message = "订单id不能为空")
    private Long orderId;

    @ApiModelProperty("暂停状态 1,申请中 2,申请成功 3,申请失败")
    private java.lang.Integer status;

    @ApiModelProperty("请假备注")
    @NotNull(message = "请假备注 不能为空")
    private java.lang.String remark;

    @ApiModelProperty("开始时间")
    @NotNull(message = "开始时间 不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private java.util.Date startDate;

    @ApiModelProperty("结束时间")
    @NotNull(message = "结束时间 不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private java.util.Date endDate;


    @ApiModelProperty("请假几天")
    private Long day;

}
