package com.chuangjian.hire.order.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("设计师总览")
public class WalletDTO {

    @ApiModelProperty("项目数量")
    private Integer orderMainCount;
    @ApiModelProperty("雇佣总金额")
    private Integer orderMainHistoryAmount;
    @ApiModelProperty("当前雇佣金额")
    private BigDecimal orderMainAmount;
    @ApiModelProperty("包月完结")
    private Integer payMonthCount;
    @ApiModelProperty("定制完结")
    private Integer payCustomCount;
    @ApiModelProperty("非正常完结")
    private Integer fireCount;
}
