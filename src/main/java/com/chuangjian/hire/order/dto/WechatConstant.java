package com.chuangjian.hire.order.dto;

public class WechatConstant {
    /**
     * 授权类型
     */
    public static final String WECHAT_OFFICE_GRANT_TYPE = "client_credential";

    /**
     * 公众号放在缓存里面的KEY
     */
    public static final String WECHAT_OFFICIAL_ACCESS_TOKEN_KEY = "wechat_official_access_token_key";


    /**
     * 公众号放在缓存里面的KEY
     */
    public static final String WECHAT_OFFICIAL_TICKET_KEY = "wechat_official_ticket_key";
}
