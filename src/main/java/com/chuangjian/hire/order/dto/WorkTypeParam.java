package com.chuangjian.hire.order.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("订单")
public class WorkTypeParam {

    @ApiModelProperty("订单Id")
    private Long orderId;


    @ApiModelProperty("工作类型 1,单休 2,双休 ")
    private java.lang.Integer workType;
}
