package com.chuangjian.hire.order.enums;

import java.util.Arrays;
import java.util.List;

public enum EmpStatusEnum {
    // 1,未支付 10,进行中 11 申请暂停中 12 申请请假中 13 申请解雇中 20,暂停中 30,待完结 40,已完结 50,已解雇
    WAITING(1, "未支付"),
    RUNNING(10, "进行中"),
    APPLY_1(11, "申请暂停中"),
    APPLY_2(12, "申请请假中"),
    APPLY_3(13, "申请解雇中"),
    SUSPEND(20, "暂停中"),
    UN_FINISH(30, "待完结"),
    FINISH(40, "已完结"),
    FIRE(50, "已解雇"),
    ;

    private Integer value;
    private String desc;

    private EmpStatusEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static EmpStatusEnum getType(Integer value) {
        if (value == null) {
            return null;
        }
        for (EmpStatusEnum typeEnum : values()) {
            if (typeEnum.value.equals(value)) {
                return typeEnum;
            }
        }
        return null;
    }

    public static List<Integer> canSettleStatus() {
        return Arrays.asList(
                EmpStatusEnum.RUNNING.getValue(),
                EmpStatusEnum.UN_FINISH.getValue(),
                EmpStatusEnum.APPLY_3.getValue()
        );
    }

    public static List<Integer> getRunningList() {
        return Arrays.asList(
                EmpStatusEnum.RUNNING.getValue(),
                EmpStatusEnum.APPLY_1.getValue(),
                EmpStatusEnum.APPLY_2.getValue(),
                EmpStatusEnum.APPLY_3.getValue(),
                EmpStatusEnum.SUSPEND.getValue(),
                EmpStatusEnum.UN_FINISH.getValue()
        );
    }

    public static List<Integer> getHistoryList() {
        return Arrays.asList(
                EmpStatusEnum.FINISH.getValue(),
                EmpStatusEnum.FIRE.getValue()
        );
    }


    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
