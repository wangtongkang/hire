//package com.chuangjian.hire.order.enums;
//
//public enum EmpSubStatusEnum {
//    // 雇佣类型  11 申请暂停中 12,申请请假中
//    RUNNING(10, "正常中"),
//    APPLY_1(11, "申请暂停中"),
//    APPLY_2(12, "申请请假中"),
//    APPLY_3(13, "申请解雇中"),
//    SUSPEND(20, "暂停中"),
//    ;
//
//    private Integer value;
//    private String desc;
//
//    private EmpSubStatusEnum(Integer value, String desc) {
//        this.value = value;
//        this.desc = desc;
//    }
//
//    public static EmpSubStatusEnum getType(Integer value) {
//        if (value == null) {
//            return null;
//        }
//        for (EmpSubStatusEnum typeEnum : values()) {
//            if (typeEnum.value.equals(value)) {
//                return typeEnum;
//            }
//        }
//        return null;
//    }
//
//
//    public Integer getValue() {
//        return this.value;
//    }
//
//    public void setValue(Integer value) {
//        this.value = value;
//    }
//
//    public String getDesc() {
//        return this.desc;
//    }
//
//    public void setDesc(String desc) {
//        this.desc = desc;
//    }
//}
