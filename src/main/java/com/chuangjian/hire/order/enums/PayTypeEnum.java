package com.chuangjian.hire.order.enums;

/**
 * 支付类型 10,支付宝web,11支付宝h5,20微信web,21微信公众号支付
 */
public enum PayTypeEnum {

    ALI_WEB(10, "支付宝web"),
    ALI_H5(11, "支付宝h5"),
    WE_CHAT_WEB(20, "微信web"),
    WE_CHAT_H5(21, "21微信公众号支付"),
    BALANCE_PAY(30, "余额支付"),
    ;

    private Integer value;
    private String desc;

    private PayTypeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static PayTypeEnum getType(Integer value) {
        if (value == null) {
            return null;
        }
        for (PayTypeEnum typeEnum : values()) {
            if (typeEnum.value.equals(value)) {
                return typeEnum;
            }
        }
        return null;
    }


    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
