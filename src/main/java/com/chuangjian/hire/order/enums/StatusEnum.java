package com.chuangjian.hire.order.enums;

public enum StatusEnum {
    //订单状态 10, 待支付 20,已支付 30 过期 40 已取消
    PAYING(10, "待支付"),
    PAID(20, "已支付"),
    EXPIRE(30, "已过期"),
    CANCEL(40, "已取消"),
    ;

    private Integer value;
    private String desc;

    private StatusEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static StatusEnum getType(Integer value) {
        if (value == null) {
            return null;
        }
        for (StatusEnum typeEnum : values()) {
            if (typeEnum.value.equals(value)) {
                return typeEnum;
            }
        }
        return null;
    }


    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
