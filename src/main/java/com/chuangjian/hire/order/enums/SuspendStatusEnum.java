package com.chuangjian.hire.order.enums;

public enum SuspendStatusEnum {
    //  1,申请中 2,申请成功 3,申请失败 4,已经取消
    APPLYING(1, "申请中"),
    SUCCESS(2, "申请成功"),
    FAIL(3, "申请失败"),
    CANCEL(4, "已经取消"),
    ;

    private Integer value;
    private String desc;

    private SuspendStatusEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static SuspendStatusEnum getType(Integer value) {
        if (value == null) {
            return null;
        }
        for (SuspendStatusEnum typeEnum : values()) {
            if (typeEnum.value.equals(value)) {
                return typeEnum;
            }
        }
        return null;
    }


    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
