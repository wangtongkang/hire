package com.chuangjian.hire.order.enums;

public enum SuspendTypeEnum {
    //(1,包月,2,定制,3,充值)
    HIRE(1, "雇主暂停"),
    EMP(2, "设计师请假"),
    ;

    private Integer value;
    private String desc;

    private SuspendTypeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static SuspendTypeEnum getType(Integer value) {
        if (value == null) {
            return null;
        }
        for (SuspendTypeEnum typeEnum : values()) {
            if (typeEnum.value.equals(value)) {
                return typeEnum;
            }
        }
        return null;
    }


    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
