package com.chuangjian.hire.order.enums;

public enum TypeEnum {
    //(1,包月,2,定制,3,充值)
    HIRE(1, "包月"),
    CUSTOM(2, "定制"),
    CHARGE(3, "充值");

    private Integer value;
    private String desc;

    private TypeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static TypeEnum getType(Integer value) {
        if (value == null) {
            return null;
        }
        for (TypeEnum typeEnum : values()) {
            if (typeEnum.value.equals(value)) {
                return typeEnum;
            }
        }
        return null;
    }


    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
