package com.chuangjian.hire.order.enums;

public enum WorkEnum {
    // 工作类型 1,单休 2,双休
    SINGLE(1, "单休"),
    DOUBLE(2, "双休"),
    ;

    private Integer value;
    private String desc;

    private WorkEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static WorkEnum getType(Integer value) {
        if (value == null) {
            return null;
        }
        for (WorkEnum typeEnum : values()) {
            if (typeEnum.value.equals(value)) {
                return typeEnum;
            }
        }
        return null;
    }


    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
