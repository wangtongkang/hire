package com.chuangjian.hire.order.enums;

public enum WorkStatusEnum {
    // 工作状态 10,工作中 20,已完成 30,已经取消
    RUNNING(10, "工作中"),
    FINISH(20, "已完成"),
    CANCEL(30, "已取消"),
    ;

    private Integer value;
    private String desc;

    private WorkStatusEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static WorkStatusEnum getType(Integer value) {
        if (value == null) {
            return null;
        }
        for (WorkStatusEnum typeEnum : values()) {
            if (typeEnum.value.equals(value)) {
                return typeEnum;
            }
        }
        return null;
    }


    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
