//package com.chuangjian.hire.order.scheduling;
//
//import cn.hutool.core.collection.CollectionUtil;
//import cn.hutool.core.date.DateTime;
//import cn.hutool.core.date.DateUtil;
//import com.alibaba.fastjson.JSON;
//import com.chuangjian.hire.order.domain.OrderMainDO;
//import com.chuangjian.hire.order.dto.SuspendDTO;
//import com.chuangjian.hire.order.enums.EmpStatusEnum;
//import com.chuangjian.hire.order.enums.SuspendStatusEnum;
//import com.chuangjian.hire.order.service.OrderMainService;
//import com.chuangjian.hire.order.service.OrderSuspendService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Configurable;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.util.Date;
//import java.util.List;
//
//@Slf4j
//@Component                //实例化
//@Configurable             //注入bean
//@EnableScheduling         //开启计划任务
//public class OrderScheduling {
//
//
//    @Resource
//    OrderMainService orderMainService;
//
//    @Resource
//    OrderSuspendService orderSuspendService;
//
//    //声明计划任务
//    @Scheduled(cron = "0 0 * * * *")
//    public void autoHirePay() {
//
//        log.info("雇主 自动发工资");
//        List<OrderMainDO> autoHirePayList = orderMainService.getAutoHirePayList();
//        if (CollectionUtil.isNotEmpty(autoHirePayList)) {
//            autoHirePayList.forEach(it -> {
//                try {
//                    orderMainService.autoHirePay(it);
//                } catch (Exception e) {
//                    log.error("雇主 自动发工资 {} 异常", JSON.toJSONString(it), e);
//                }
//            });
//        }
//        log.info("设计师 自动确认工资");
//        List<OrderMainDO> autoHirePayConfirmList = orderMainService.getAutoHirePayConfirmList();
//        if (CollectionUtil.isNotEmpty(autoHirePayConfirmList)) {
//            autoHirePayConfirmList.forEach(it -> {
//                try {
//                    orderMainService.autoHirePayConfirm(it);
//                } catch (Exception e) {
//                    log.error("设计师 自动确认工资 {} 异常", JSON.toJSONString(it), e);
//                }
//            });
//        }
//
//    }
//
//
//    //声明计划任务
//    @Scheduled(cron = "0 0 * * * *")
//    public void autoStopOrRunning() {
//
//        log.info("自动 暂停工作");
//
////        List<OrderMainDO> needSuspendOrderMain = orderMainService.getNeedSuspendOrderMain();
////
////        if (CollectionUtil.isEmpty(needSuspendOrderMain)) {
////            return;
////        }
////        Date date = new Date();
////        needSuspendOrderMain.forEach(it -> {
////            SuspendDTO suspend = orderSuspendService.getSuspendById(it.getEmployerSuspendId());
////            if (SuspendStatusEnum.SUCCESS.getValue().equals(suspend.getStatus())) {
////                DateTime endDate = DateUtil.endOfDay(DateUtil.offsetDay(suspend.getEndDate(), -1));
////                if (suspend.getStartDate().before(date) && endDate.after(date)) {
////                    orderMainService.changeEmpStatus(suspend.getOrderId(), EmpStatusEnum.SUSPEND.getValue());
////                } else if (endDate.before(date)) {
////                    orderMainService.changeEmpStatus(suspend.getOrderId(), EmpStatusEnum.RUNNING.getValue());
////                    orderMainService.setEmployerSuspend(suspend.getOrderId(), null);
////                }
////            }
////        });
//
//
//    }
//
//}
