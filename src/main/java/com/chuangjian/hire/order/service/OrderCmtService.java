package com.chuangjian.hire.order.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chuangjian.hire.order.domain.OrderCmtDO;
import com.chuangjian.hire.order.dto.OrderCmtDTO;

import java.util.List;

/**
 * FileName: OrderCmtBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/14 20:51
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface OrderCmtService {

    Page<OrderCmtDO> getPageList(Page page, OrderCmtDO orderCmt);

    Boolean addOrUpdate(OrderCmtDO orderCmt);

    Boolean deleteCmt(OrderCmtDO orderCmt);


    Boolean employerCmt(OrderCmtDTO cmt);

    Boolean employeeCmt(OrderCmtDTO cmt);

    Page getCmtByEmployeeId(Page page,Long employeeId);
}
