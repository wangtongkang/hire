//package com.chuangjian.hire.order.service;
//
//
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.chuangjian.hire.order.domain.OrderMainDO;
//import com.chuangjian.hire.order.dto.*;
//import com.chuangjian.hire.users.dto.UserDTO;
//
//import java.math.BigDecimal;
//import java.util.List;
//
///**
// * FileName: OrderMainBusiness
// * Description:
// *
// * @author: CodeGenerator
// * @date: 2020/07/13 22:56
// * Copyright (C) 杭州同基汽车科技有限公司
// */
//public interface OrderMainService {
//
//    OrderMainDO getOrderById(Long orderId);
//
//    List<OrderMainDO> getNeedSuspendOrderMain();
//
//
//    List<OrderMainDO> getOrderByIdList(List<Long> orderIdList);
//
//    Boolean changeEmpStatus(Long orderId, Integer empStatus);
//
//    Boolean setEmployerSuspend(Long orderId, Long employerSuspendId);
//
//    Boolean setEmployeeSuspend(Long orderId, Long employerSuspendId);
//
//    Boolean changeEndDate(Long orderId, Long delayDay);
//
//    OrderDTO createOrder(OrderDTO order, UserDTO user);
//
//    OrderDTO getOrderInfoById(Long orderId);
//
//    OrderDTO getPayingOrderInfo(Long userId);
//
//    String payBalance(PayDTO pay);
//
//    Boolean paySuccess(Long orderId, Integer payType);
//
//    Boolean changeOrderAmount(OrderAmountParam orderAmountParam);
//
//    Boolean payFailOrCancel(Long orderId, Boolean expire);
//
//
//    Boolean hirePay(Long orderId, BigDecimal payAmount, String hirePayRemark, String payPwd, Long employerId);
//
//    Boolean hirePayPlatform(PayPlatformDTO param);
//
//    Boolean changeWorkTypePlatform(WorkTypeParam param);
//
//    List<OrderMainDO> getAutoHirePayList();
//
//    Boolean autoHirePay(OrderMainDO orderMain);
//
//    List<OrderMainDO> getAutoHirePayConfirmList();
//
//    Boolean autoHirePayConfirm(OrderMainDO orderMain);
//
//
//    Boolean hirePayConfirm(Long orderId, Long employeeId);
//
//    Boolean employerCmt(OrderCmtDTO cmt);
//
//    Boolean employeeCmt(OrderCmtDTO cmt);
//
//    FireDTO getFireInfo(Long orderId);
//
//    Boolean applyFire(FireDTO fire, UserDTO user);
//
//    Boolean fireAgree(FireDTO fire, UserDTO user);
//
//    List<SignDTO> getRunningOrderList(Long userId);
//
//    EmployerOverviewDTO getOverviewByEmployerId(Long userId);
//
//    EmployeeOverviewDTO getOverviewByEmployeeId(Long userId);
//
//    Page getMyEmployeePage(Page page, OrderListParam param, Long userId);
//
//    Page getMyEmployerPage(Page page, OrderListParam param, Long userId);
//
//    Page getMyChargePage(Page page, OrderParam param, Long userId);
//
//    Page getMyEmployPage(Page page, OrderParam param, Long userId);
//
//    Page getOrderPagePlatform(Page page, OrderMainPOParam param);
//
//    Page getHirePagePlatform(Page page, OrderHirePOParam param);
//}
