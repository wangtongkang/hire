package com.chuangjian.hire.order.service;


import com.chuangjian.hire.order.dto.PayDTO;

import java.util.Map;

/**
 * FileName: OrderPayBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/18 15:50
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface OrderPayService {


    String createPay(PayDTO pay);

    void initPay();


    void wxNotify(Map<String, String> parameter);

    void aliNotify(Map<String, String> parameter);

}
