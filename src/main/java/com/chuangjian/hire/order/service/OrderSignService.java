package com.chuangjian.hire.order.service;


import com.chuangjian.hire.order.dto.SignDTO;

import java.util.List;

/**
 * FileName: OrderSignBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/17 09:08
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface OrderSignService {

    List<SignDTO> getUnSignList(Long userId);

    Boolean signList(List<Long> orderIdList);

    Integer getSingDay(Long orderId);
}
