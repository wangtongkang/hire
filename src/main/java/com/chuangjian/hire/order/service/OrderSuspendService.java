package com.chuangjian.hire.order.service;


import com.chuangjian.hire.order.dto.SuspendDTO;
import com.chuangjian.hire.users.dto.UserDTO;

/**
 * FileName: OrderSuspendBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/17 09:08
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface OrderSuspendService {

    Boolean employerSuspend(SuspendDTO suspend, UserDTO user);

    Boolean employerSuspendCancel(SuspendDTO suspend, UserDTO user);

    Boolean employeeSuspend(SuspendDTO suspend, UserDTO user);


    SuspendDTO getSuspendById(Long suspendId);

    Boolean auditSuspendApply(SuspendDTO suspend, UserDTO user);
}
