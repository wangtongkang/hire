package com.chuangjian.hire.order.service;


import com.chuangjian.hire.order.dto.OrderWorkDTO;

import java.util.List;

/**
 * FileName: OrderWorkBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/13 22:56
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface OrderWorkService {

    Boolean addOrderWork(OrderWorkDTO orderWork);

    Boolean cancelWork(OrderWorkDTO orderWork);

    Boolean confirmWork(OrderWorkDTO orderWork);

    Boolean finishWork(OrderWorkDTO orderWork);

    List<OrderWorkDTO> getOrderWorkList(Long orderId);
}
