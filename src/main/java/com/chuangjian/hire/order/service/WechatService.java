package com.chuangjian.hire.order.service;


import com.chuangjian.hire.users.dto.AccessTokenDTO;
import com.chuangjian.hire.users.dto.initJSSDKDataDTO;

public interface WechatService {

    Boolean deleteTokenAndTicket();

    initJSSDKDataDTO getInitJSSDKData(String url);


    AccessTokenDTO getUserOpenId(String code);


}
