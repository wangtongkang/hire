//package com.chuangjian.hire.order.service.impl;
//
//import cn.hutool.core.bean.BeanUtil;
//import com.baomidou.mybatisplus.core.toolkit.IdWorker;
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.chuangjian.hire.order.dao.OrderCmtMapper;
//import com.chuangjian.hire.order.domain.OrderCmtDO;
//import com.chuangjian.hire.order.domain.OrderMainDO;
//import com.chuangjian.hire.order.dto.OrderCmtDTO;
//import com.chuangjian.hire.order.service.OrderCmtService;
//import com.chuangjian.hire.order.service.OrderMainService;
//import com.chuangjian.hire.users.dto.UserDTO;
//import com.chuangjian.hire.users.dto.UsersDTO;
//import com.chuangjian.hire.users.service.UsersService;
//import com.chuangjian.hire.util.BeanUtils;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.annotation.Resource;
//import java.util.List;
//
///**
// * FileName: OrderCmtBusiness
// * Description:
// *
// * @author: CodeGenerator
// * @date: 2020/07/14 20:51
// * Copyright (C) 杭州同基汽车科技有限公司
// */
//@Transactional
//@Service("orderCmtService")
//public class OrderCmtServiceImpl extends ServiceImpl<OrderCmtMapper, OrderCmtDO> implements OrderCmtService {
//
//    @Resource
//    OrderMainService orderMainService;
//    @Resource
//    UsersService usersService;
//
//    @Override
//    public Page<OrderCmtDO> getPageList(Page page, OrderCmtDO orderCmt) {
//
//        return page(page, Wrappers.
//                <OrderCmtDO>lambdaQuery()
//                .like(OrderCmtDO::getCmtName, orderCmt.getCmtName()));
//    }
//
//    @Override
//    public Boolean addOrUpdate(OrderCmtDO orderCmt) {
//        UserDTO user = usersService.getUserById(orderCmt.getUserId());
//        orderCmt.setUserIcon(user.getIcon());
//        orderCmt.setUserName(user.getNickName());
//        if (orderCmt.getId() == null && orderCmt.getOrderId() == null) {
//            orderCmt.setOrderId(IdWorker.getId());
//        }
//        return saveOrUpdate(orderCmt);
//    }
//
//    @Override
//    public Boolean deleteCmt(OrderCmtDO orderCmt) {
//        return removeById(orderCmt.getId());
//    }
//
//    @Override
//    public Boolean employerCmt(OrderCmtDTO cmt) {
//        OrderCmtDO orderCmtDO = BeanUtil.toBean(cmt, OrderCmtDO.class);
//        OrderMainDO orderMain = orderMainService.getOrderById(cmt.getOrderId());
//        orderCmtDO.setCmtId(orderMain.getEmployeeId());
//        orderCmtDO.setCmtName(orderMain.getEmployee());
//        orderCmtDO.setUserId(orderMain.getEmployerId());
//        orderCmtDO.setUserName(orderMain.getEmployer());
//        UsersDTO usersDTO = usersService.getUserDetailById(orderMain.getEmployerId());
//        orderCmtDO.setUserIcon(usersDTO.getIcon());
//        return save(orderCmtDO);
//    }
//
//    @Override
//    public Boolean employeeCmt(OrderCmtDTO cmt) {
//        OrderCmtDO orderCmtDO = BeanUtil.toBean(cmt, OrderCmtDO.class);
//        OrderMainDO orderMain = orderMainService.getOrderById(cmt.getOrderId());
//        orderCmtDO.setCmtId(orderMain.getEmployerId());
//        orderCmtDO.setCmtName(orderMain.getEmployer());
//        orderCmtDO.setUserId(orderMain.getEmployeeId());
//        orderCmtDO.setUserName(orderMain.getEmployee());
//        UsersDTO usersDTO = usersService.getUserDetailById(orderMain.getEmployerId());
//        orderCmtDO.setUserIcon(usersDTO.getIcon());
//        return save(orderCmtDO);
//    }
//
//    @Override
//    public Page getCmtByEmployeeId(Page pages, Long employeeId) {
//        Page pageResult = page(pages,
//                Wrappers.<OrderCmtDO>lambdaQuery()
//                        .eq(OrderCmtDO::getCmtId, employeeId)
//        );
//        pageResult.setRecords(BeanUtils.transformList(OrderCmtDTO.class, pageResult.getRecords()));
//        return pageResult;
//    }
//}
