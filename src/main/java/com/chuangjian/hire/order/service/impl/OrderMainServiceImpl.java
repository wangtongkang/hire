//package com.chuangjian.hire.order.service.impl;
//
//import cn.hutool.core.bean.BeanUtil;
//import cn.hutool.core.collection.CollectionUtil;
//import cn.hutool.core.date.DateTime;
//import cn.hutool.core.date.DateUnit;
//import cn.hutool.core.date.DateUtil;
//import com.alibaba.fastjson.JSON;
//import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
////import com.chuangjian.hire.active.OrderProducer;
//import com.chuangjian.hire.common.exceptionEnum.ApiExceptionEnum;
//import com.chuangjian.hire.common.exption.ApiException;
//import com.chuangjian.hire.coupon.service.UsersCouponService;
//import com.chuangjian.hire.order.dao.OrderMainMapper;
//import com.chuangjian.hire.order.domain.OrderMainDO;
//import com.chuangjian.hire.order.dto.*;
//import com.chuangjian.hire.order.enums.*;
//import com.chuangjian.hire.order.service.OrderCmtService;
//import com.chuangjian.hire.order.service.OrderMainService;
//import com.chuangjian.hire.order.service.OrderSignService;
//import com.chuangjian.hire.product.dto.ProductUserDTO;
//import com.chuangjian.hire.users.domain.UsersDO;
//import com.chuangjian.hire.users.dto.UserDTO;
//import com.chuangjian.hire.users.service.UsersBalanceLogService;
//import com.chuangjian.hire.users.service.UsersService;
//import com.chuangjian.hire.util.BeanUtils;
//import com.chuangjian.hire.util.OrderNumUtils;
//import com.chuangjian.hire.util.StringUtil;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.annotation.Resource;
//import java.math.BigDecimal;
//import java.util.*;
//import java.util.stream.Collectors;
//
///**
// * FileName: OrderMainBusiness
// * Description:
// *
// * @author: CodeGenerator
// * @date: 2020/07/13 22:56
// * Copyright (C) 杭州同基汽车科技有限公司
// */
//@Slf4j
//@Transactional
//@Service("orderMainService")
//public class OrderMainServiceImpl extends ServiceImpl<OrderMainMapper, OrderMainDO> implements OrderMainService {
//
//    @Resource
//    UsersBalanceLogService usersBalanceLogService;
//    @Resource
//    UsersCouponService usersCouponService;
//    @Resource
//    OrderCmtService orderCmtService;
//
//    @Resource
//    UsersService usersService;
////    @Resource
////    OrderProducer orderProducer;
//
//    @Resource
//    OrderSignService orderSignService;
//
//
//    @Override
//    public OrderMainDO getOrderById(Long orderId) {
//        return getById(orderId);
//    }
//
//    @Override
//    public List<OrderMainDO> getNeedSuspendOrderMain() {
//
//
//        LambdaQueryWrapper<OrderMainDO> wrapper = Wrappers.<OrderMainDO>lambdaQuery()
//                .select(OrderMainDO::getAmount, OrderMainDO::getType, OrderMainDO::getEmpStatus, OrderMainDO::getEndDate)
//                .eq(OrderMainDO::getStatus, StatusEnum.PAID.getValue())
//                .in(OrderMainDO::getType, Arrays.asList(TypeEnum.HIRE.getValue(), TypeEnum.CUSTOM.getValue()))
//                .isNotNull(OrderMainDO::getEmployerSuspendId);
//        return list(wrapper);
//
//    }
//
//    @Override
//    public List<OrderMainDO> getOrderByIdList(List<Long> orderIdList) {
//
//        if (CollectionUtil.isEmpty(orderIdList)) {
//            return Collections.emptyList();
//        }
//        return listByIds(orderIdList);
//    }
//
//
//    @Override
//    public Boolean changeEmpStatus(Long orderId, Integer empStatus) {
//        OrderMainDO orderMain = getById(orderId);
//        orderMain.setEmpStatus(empStatus);
//        return updateById(orderMain);
//    }
//
//    @Override
//    public Boolean setEmployerSuspend(Long orderId, Long employerSuspendId) {
//
//        return update(Wrappers.<OrderMainDO>lambdaUpdate()
//                .set(OrderMainDO::getEmployerSuspendId, employerSuspendId)
//                .eq(OrderMainDO::getId, orderId)
//        );
//    }
//
//    @Override
//    public Boolean setEmployeeSuspend(Long orderId, Long employerSuspendId) {
//        return update(Wrappers.<OrderMainDO>lambdaUpdate()
//                .set(OrderMainDO::getEmployeeSuspendId, employerSuspendId)
//                .eq(OrderMainDO::getId, orderId)
//        );
//    }
//
//    @Override
//    public Boolean changeEndDate(Long orderId, Long delayDay) {
//        OrderMainDO orderMain = getById(orderId);
//        Date endDate = orderMain.getEndDate();
//        Date end = DateUtil.offsetDay(endDate, delayDay.intValue());
//        orderMain.setEndDate(end);
//        return updateById(orderMain);
//    }
//
//    @Override
//    public OrderDTO createOrder(OrderDTO order, UserDTO user) {
//        TypeEnum type = TypeEnum.getType(order.getType());
//        if (type == null) {
//            throw new ApiException(ApiExceptionEnum.ERROR_PARAM);
//        }
//        if (order.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
//            throw new ApiException(ApiExceptionEnum.ORDER_AMOUNT_ERROR);
//        }
//        OrderDTO payingOrderInfo = getPayingOrderInfo(user.getId());
//        if (payingOrderInfo != null) {
//            throw new ApiException(ApiExceptionEnum.ORDER_MULTI_STATUS_ERROR);
//        }
//        if (isHire(order.getType()) && !user.getType().equals(com.chuangjian.hire.users.enums.TypeEnum.HIRE.getValue())) {
//            throw new ApiException(ApiExceptionEnum.ORDER_HIRE_ERROR);
//        }
//        if (TypeEnum.HIRE.getValue().equals(type) && order.getWorkMonth() == null) {
//            throw new ApiException(ApiExceptionEnum.ERROR_PARAM);
//        }
//        OrderMainDO main = new OrderMainDO();
//        main.setCode(OrderNumUtils.getOrderNum());
//        main.setType(order.getType());
//        main.setStatus(StatusEnum.PAYING.getValue());
//        main.setAmount(order.getAmount());
//        main.setEmpStatus(EmpStatusEnum.WAITING.getValue());
//        main.setEmployer(user.getNickName());
//        main.setEmployerId(user.getId());
//        main.setValid(true);
//        if (isHire(main)) {
//            main.setEmployee(order.getEmployee());
//            main.setEmployeeId(order.getEmployeeId());
//            main.setStartDate(DateUtil.beginOfDay(order.getStartDate()));
//            main.setEndDate(DateUtil.endOfDay(order.getEndDate()));
//            main.setUserCouponId(order.getUserCouponId());
//            main.setUserCouponAmount(order.getUserCouponAmount());
//            main.setWorkMonth(order.getWorkMonth());
//            if (isCouponPay(main)) {
//                usersCouponService.useCoupon(main.getUserCouponId(), main.getUserCouponAmount(), user.getId());
//                main.setActualPay(main.getAmount().subtract(main.getUserCouponAmount()));
//            }
//        }
//        if (main.getActualPay() == null) {
//            main.setActualPay(order.getAmount());
//        }
//        save(main);
////        orderProducer.delaySend(OrderProducer.DEFAULT_QUEUE, main.getId(), 30);
//        return BeanUtil.toBean(main, OrderDTO.class);
//    }
//
//
//    @Override
//    public OrderDTO getOrderInfoById(Long orderId) {
//        return BeanUtil.toBean(getById(orderId), OrderDTO.class);
//    }
//
//    @Override
//    public OrderDTO getPayingOrderInfo(Long userId) {
//
//        OrderMainDO orderMain = getOne(
//                Wrappers.<OrderMainDO>lambdaQuery()
//                        .eq(OrderMainDO::getEmployerId, userId)
//                        .eq(OrderMainDO::getStatus, StatusEnum.PAYING.getValue())
//        );
//        if (orderMain == null) {
//            return null;
//        }
//        return BeanUtil.toBean(
//                orderMain
//                , OrderDTO.class);
//    }
//
//    @Override
//    public String payBalance(PayDTO pay) {
//        OrderMainDO orderMainDO = getById(pay.getOrderId());
//        //除非是待支付 其他状态不允许变动订单状态
//        if (!Objects.equals(StatusEnum.PAYING.getValue(), orderMainDO.getStatus())) {
//            throw new ApiException(ApiExceptionEnum.ORDER_MAIN_STATUS_ERROR);
//        }
//        checkBalance(orderMainDO);
//        orderMainDO.setStatus(StatusEnum.PAID.getValue());
//        orderMainDO.setEmpStatus(EmpStatusEnum.RUNNING.getValue());
//        orderMainDO.setPayType(PayTypeEnum.BALANCE_PAY.getValue());
//        updateById(orderMainDO);
//
//        usersBalanceLogService.balancePay(orderMainDO.getId(), orderMainDO.getActualPay(), orderMainDO.getEmployerId());
//
//        return JSON.toJSONString(BeanUtil.toBean(orderMainDO, OrderDTO.class));
//    }
//
//    @Override
//    public Boolean paySuccess(Long orderId, Integer payType) {
//        OrderMainDO main = getById(orderId);
//        if (Objects.equals(StatusEnum.PAID.getValue(), main.getStatus())) {
//            return true;
//        }
//        if (isHire(main)) {
//            usersBalanceLogService.hirePaySuccess(orderId, main.getAmount(), main.getEmployerId());
//        } else if (isCharge(main)) {
//            usersBalanceLogService.chargePaySuccess(orderId, main.getActualPay(), main.getEmployerId());
//        }
//        main.setStatus(StatusEnum.PAID.getValue());
//        main.setEmpStatus(EmpStatusEnum.RUNNING.getValue());
//        main.setPayType(payType);
//        return updateById(main);
//    }
//
//    @Override
//    public Boolean changeOrderAmount(OrderAmountParam orderAmountParam) {
//        OrderMainDO main = getById(orderAmountParam.getId());
//        if (!StatusEnum.PAYING.getValue().equals(main.getStatus())) {
//            throw new ApiException(ApiExceptionEnum.ORDER_MAIN_STATUS_ERROR);
//        }
//        if (main.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
//            throw new ApiException(ApiExceptionEnum.ORDER_AMOUNT_ERROR);
//        }
//        main.setAmount(orderAmountParam.getAmount());
//        main.setActualPay(orderAmountParam.getAmount());
//        return updateById(main);
//    }
//
//
//    @Override
//    public Boolean payFailOrCancel(Long orderId, Boolean expire) {
//        OrderMainDO main = getById(orderId);
//        log.info("取消订单为: {}", JSON.toJSONString(main));
//        if (main == null) {
//            return true;
//        }
//        if (!StatusEnum.PAYING.getValue().equals(main.getStatus())) {
//            return false;
//        }
//        if (isHire(main)) {
//            if (isCouponPay(main)) {
//                usersCouponService.recovery(main.getUserCouponId(), main.getEmployerId());
//            }
//        }
//        main.setStatus(expire ? StatusEnum.EXPIRE.getValue() : StatusEnum.CANCEL.getValue());
//        main.setEmpStatus(EmpStatusEnum.WAITING.getValue());
//        return updateById(main);
//    }
//
//    void checkBalance(OrderMainDO orderMainDO) {
//        UsersDO usersDO = usersService.findById(orderMainDO.getEmployerId());
//        if (isHire(orderMainDO) && usersDO.getBalance() != null && usersDO.getBalance().compareTo(orderMainDO.getAmount()) >= 0) {
//            return;
//        }
//        throw new ApiException(ApiExceptionEnum.PAY_BALANCE_ERROR);
//    }
//
//    @Override
//    public Boolean hirePay(Long orderId, BigDecimal payAmount, String hirePayRemark, String payPwd, Long employerId) {
//        OrderMainDO main = getById(orderId);
//        main.setEmpStatus(EmpStatusEnum.UN_FINISH.getValue());
//        if (payAmount.compareTo(main.getAmount()) > 0) {
//            throw new ApiException(ApiExceptionEnum.HIRE_PAY_ERROR);
//        }
//        if (!Objects.equals(main.getEmployerId(), employerId)) {
//            throw new ApiException(ApiExceptionEnum.HIRE_ONLY_EMPLOYER_ERROR);
//        }
//        Boolean isRightPassword = usersService.confirmPayPwd(employerId, payPwd);
//        if (!isRightPassword) {
//            throw new ApiException(ApiExceptionEnum.HIRE_PAY_PASSWORD_ERROR);
//        }
//        main.setFireRemark(hirePayRemark);
//        main.setHirePayAmount(payAmount);
//        return updateById(main);
//    }
//
//    @Override
//    public Boolean hirePayPlatform(PayPlatformDTO param) {
//
//        OrderMainDO main = getById(param.getOrderId());
//        if (!EmpStatusEnum.canSettleStatus().contains(main.getEmpStatus())) {
//            throw new ApiException(ApiExceptionEnum.ORDER_MAIN_EMP_STATUS_ERROR);
//        }
//        if (!EmpStatusEnum.getHistoryList().contains(param.getEmpStatus())) {
//            throw new ApiException(ApiExceptionEnum.ERROR_PARAM);
//        }
//        if (param.getDuePayAmount().compareTo(main.getAmount()) > 0) {
//            throw new ApiException(ApiExceptionEnum.HIRE_PAY_ERROR);
//        }
//        main.setEmpStatus(param.getEmpStatus());
//        main.setFireRemark(param.getFireRemark());
//        main.setHirePayAmount(param.getDuePayAmount());
//        updateById(main);
//
//
//        BigDecimal remainAmount = main.getAmount().subtract(main.getHirePayAmount());
//        if (remainAmount.compareTo(BigDecimal.ZERO) == 0) {
//            usersBalanceLogService.hirePayConfirmPartEmployee(main.getId(), main.getHirePayAmount(), main.getEmployeeId());
//        } else if (remainAmount.compareTo(main.getAmount()) == 0) {
//            usersBalanceLogService.hirePayConfirmPartEmployer(main.getId(), main.getAmount(), main.getEmployerId());
//        } else if (remainAmount.compareTo(BigDecimal.ZERO) > 0) {
//            usersBalanceLogService.hirePayConfirmPartEmployer(main.getId(), remainAmount, main.getEmployerId());
//            usersBalanceLogService.hirePayConfirmPartEmployee(main.getId(), main.getHirePayAmount(), main.getEmployeeId());
//        }
//        //雇主账户冻结的金额减少
//        usersBalanceLogService.hirePayConfirm(main.getAmount(), main.getEmployerId());
//        return true;
//    }
//
//    @Override
//    public Boolean changeWorkTypePlatform(WorkTypeParam param) {
//        if (WorkEnum.getType(param.getWorkType()) == null) {
//            throw new ApiException(ApiExceptionEnum.ERROR_PARAM);
//        }
//        OrderMainDO orderMain = getById(param.getOrderId());
//        orderMain.setWorkType(param.getWorkType());
//        updateById(orderMain);
//        return true;
//    }
//
//    @Override
//    public List<OrderMainDO> getAutoHirePayList() {
//
//        Date date = new Date();
//        DateTime dateTime = DateUtil.offsetDay(date, -7);
//        List<OrderMainDO> needAutoHirePayList = list(Wrappers.<OrderMainDO>lambdaQuery()
//                .eq(OrderMainDO::getStatus, StatusEnum.PAID.getValue())
//                .in(OrderMainDO::getType, Arrays.asList(TypeEnum.HIRE.getValue(), TypeEnum.CUSTOM.getValue()))
//                .eq(OrderMainDO::getEmpStatus, EmpStatusEnum.RUNNING.getValue())
//                .lt(OrderMainDO::getEndDate, dateTime)
//        );
//
//        return needAutoHirePayList;
//    }
//
//    @Override
//    public Boolean autoHirePay(OrderMainDO orderMainDO) {
//
//        log.info("雇主 自动发工资 : {}", JSON.toJSONString(orderMainDO));
//
//        orderMainDO.setEmpStatus(EmpStatusEnum.UN_FINISH.getValue());
//        orderMainDO.setHirePayAmount(orderMainDO.getAmount());
//
//        return updateById(orderMainDO);
//    }
//
//    @Override
//    public List<OrderMainDO> getAutoHirePayConfirmList() {
//        Date date = new Date();
//        DateTime dateTime = DateUtil.offsetDay(date, -7);
//        return list(Wrappers.<OrderMainDO>lambdaQuery()
//                .eq(OrderMainDO::getStatus, StatusEnum.PAID.getValue())
//                .in(OrderMainDO::getType, Arrays.asList(TypeEnum.HIRE.getValue(), TypeEnum.CUSTOM.getValue()))
//                .eq(OrderMainDO::getEmpStatus, EmpStatusEnum.UN_FINISH.getValue())
//                .lt(OrderMainDO::getEndDate, dateTime)
//        );
//    }
//
//    @Override
//    public Boolean autoHirePayConfirm(OrderMainDO orderMainDO) {
//        log.info("设计师 自动确认工资 : {}", JSON.toJSONString(orderMainDO));
//        return hirePayConfirm(orderMainDO.getId(), orderMainDO.getEmployeeId());
//    }
//
//    @Override
//    public Boolean hirePayConfirm(Long orderId, Long employeeId) {
//        OrderMainDO main = getById(orderId);
//        if (!(EmpStatusEnum.UN_FINISH.getValue().equals(main.getEmpStatus()) || EmpStatusEnum.APPLY_3.getValue().equals(main.getEmpStatus()))) {
//            throw new ApiException(ApiExceptionEnum.ORDER_MAIN_STATUS_ERROR);
//        }
//
//        if (!Objects.equals(main.getEmployeeId(), employeeId)) {
//            throw new ApiException(ApiExceptionEnum.PAY_CONFIRM_ERROR);
//        }
//        main.setEmpStatus(EmpStatusEnum.FINISH.getValue());
//        updateById(main);
//
//        BigDecimal remainAmount = main.getAmount().subtract(main.getHirePayAmount());
//        if (remainAmount.compareTo(BigDecimal.ZERO) == 0) {
//            usersBalanceLogService.hirePayConfirmPartEmployee(main.getId(), main.getHirePayAmount(), main.getEmployeeId());
//        } else if (remainAmount.compareTo(main.getAmount()) == 0) {
//            usersBalanceLogService.hirePayConfirmPartEmployer(main.getId(), main.getAmount(), main.getEmployerId());
//        } else if (remainAmount.compareTo(BigDecimal.ZERO) > 0) {
//            usersBalanceLogService.hirePayConfirmPartEmployer(main.getId(), remainAmount, main.getEmployerId());
//            usersBalanceLogService.hirePayConfirmPartEmployee(main.getId(), main.getHirePayAmount(), main.getEmployeeId());
//        }
//        //雇主账户冻结的金额减少
//        usersBalanceLogService.hirePayConfirm(main.getAmount(), main.getEmployerId());
//        return true;
//    }
//
//    @Override
//    public Boolean employerCmt(OrderCmtDTO cmt) {
//        Long orderId = cmt.getOrderId();
//        OrderMainDO main = getById(orderId);
//        if (main.getEmployerCmt()) {
//            throw new ApiException(ApiExceptionEnum.Cmt_ERROR);
//        }
//        main.setEmployerCmt(true);
//        updateById(main);
//        return orderCmtService.employerCmt(cmt);
//    }
//
//    @Override
//    public Boolean employeeCmt(OrderCmtDTO cmt) {
//        Long orderId = cmt.getOrderId();
//        OrderMainDO main = getById(orderId);
//        if (main.getEmployeeCmt()) {
//            throw new ApiException(ApiExceptionEnum.Cmt_ERROR);
//        }
//        main.setEmployeeCmt(true);
//        updateById(main);
//        return orderCmtService.employeeCmt(cmt);
//    }
//
//    @Override
//    public FireDTO getFireInfo(Long orderId) {
//        OrderMainDO orderMain = getById(orderId);
//        FireDTO fireDTO = new FireDTO();
//        fireDTO.setFireRemark(orderMain.getFireRemark());
//        fireDTO.setOrderId(orderId);
//        fireDTO.setPayAmount(orderMain.getHirePayAmount());
//
//        Integer singDay = orderSignService.getSingDay(orderId);
//        BigDecimal amount = orderMain.getAmount();
//        if (TypeEnum.HIRE.getValue().equals(orderMain.getType())) {
//            if (orderMain.getWorkMonth() != null && orderMain.getWorkType() != null) {
//                if (WorkEnum.SINGLE.getValue().equals(orderMain.getWorkType())) {
//                    BigDecimal workday = new BigDecimal(orderMain.getWorkMonth() * 26);
//                    if (singDay > 0) {
//                        amount = orderMain.getAmount().divide(workday, 2, BigDecimal.ROUND_HALF_EVEN).multiply(new BigDecimal(singDay));
//                    }
//                } else if (WorkEnum.DOUBLE.getValue().equals(orderMain.getWorkType())) {
//                    BigDecimal workday = new BigDecimal(orderMain.getWorkMonth() * 22);
//                    if (singDay > 0) {
//                        amount = orderMain.getAmount().divide(workday, 2, BigDecimal.ROUND_HALF_EVEN).multiply(new BigDecimal(singDay));
//                    }
//                }
//            }
//        }
//        if (DateUtil.between(orderMain.getCreateDate(), new Date(), DateUnit.DAY) <= 3) {
//            amount = BigDecimal.ZERO;
//        }
//        fireDTO.setGetSignDay(singDay);
//        fireDTO.setDuePayAmount(amount);
//        fireDTO.setStartDate(orderMain.getStartDate());
//        fireDTO.setEndDate(orderMain.getEndDate());
//        return fireDTO;
//    }
//
//    @Override
//    public Boolean applyFire(FireDTO fire, UserDTO user) {
//        OrderMainDO orderMain = getById(fire.getOrderId());
//        if (fire.getPayAmount().compareTo(orderMain.getAmount()) > 0) {
//            throw new ApiException(ApiExceptionEnum.HIRE_PAY_ERROR);
//        }
//        if (!orderMain.getEmployerId().equals(user.getId())) {
//            throw new ApiException(ApiExceptionEnum.HIRE_ONLY_EMPLOYER_ERROR);
//        }
//
//        orderMain.setEmpStatus(EmpStatusEnum.APPLY_3.getValue());
//        orderMain.setFireRemark(fire.getFireRemark());
//        orderMain.setHirePayAmount(fire.getPayAmount());
//        updateById(orderMain);
//        return true;
//    }
//
//    @Override
//    public Boolean fireAgree(FireDTO fire, UserDTO user) {
//        OrderMainDO orderMain = getById(fire.getOrderId());
//        if (fire.getFire()) {
//            hirePayConfirm(orderMain.getId(), user.getId());
//            orderMain.setEmpStatus(EmpStatusEnum.FIRE.getValue());
//        } else {
//            orderMain.setEmpStatus(EmpStatusEnum.RUNNING.getValue());
//        }
//        updateById(orderMain);
//        return true;
//    }
//
//    @Override
//    public List<SignDTO> getRunningOrderList(Long userId) {
//        List<OrderMainDO> mainDOList = list(
//
//                Wrappers.<OrderMainDO>lambdaQuery()
//                        .eq(OrderMainDO::getEmployeeId, userId)
//                        .eq(OrderMainDO::getEmpStatus, EmpStatusEnum.RUNNING.getValue())
//        );
//        if (CollectionUtil.isEmpty(mainDOList)) {
//            return Collections.emptyList();
//        }
//        return mainDOList.stream().map(it -> {
//            SignDTO transform = BeanUtils.transform(SignDTO.class, it);
//            transform.setOrderId(it.getId());
//            return transform;
//        }).collect(Collectors.toList());
//    }
//
//    @Override
//    public EmployerOverviewDTO getOverviewByEmployerId(Long userId) {
//
//        EmployerOverviewDTO overview = new EmployerOverviewDTO();
//
//        LambdaQueryWrapper<OrderMainDO> wrapper = Wrappers.<OrderMainDO>lambdaQuery()
//                .select(OrderMainDO::getAmount, OrderMainDO::getEmpStatus, OrderMainDO::getEndDate)
//                .eq(OrderMainDO::getStatus, StatusEnum.PAID.getValue())
//                .in(OrderMainDO::getType, Arrays.asList(TypeEnum.HIRE.getValue(), TypeEnum.CUSTOM.getValue()))
//                .eq(OrderMainDO::getEmployerId, userId);
//        List<OrderMainDO> orderList = list(wrapper);
//
//        if (CollectionUtil.isEmpty(orderList)) {
//            return overview;
//        }
//        overview.setEmployeeHistoryCount(orderList.size());
//
//        long count = orderList.stream().filter(it -> EmpStatusEnum.getRunningList().contains(it.getEmpStatus())).count();
//        overview.setEmployeeCount(count);
//
//        BigDecimal runningOrderAmount = orderList.stream()
//                .filter(it -> EmpStatusEnum.getRunningList().contains(it.getEmpStatus()))
//                .map(OrderMainDO::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
//        overview.setOrderMainAmount(runningOrderAmount);
//
//        Date now = new Date();
//        BigDecimal unPayAmount = orderList.stream()
//                .filter(it -> EmpStatusEnum.getRunningList().contains(it.getEmpStatus()) && now.after(it.getEndDate()))
//                .map(OrderMainDO::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
//        overview.setUnPayAmount(unPayAmount);
//        return overview;
//
//    }
//
//    @Override
//    public EmployeeOverviewDTO getOverviewByEmployeeId(Long userId) {
//        EmployeeOverviewDTO overview = new EmployeeOverviewDTO();
//
//        LambdaQueryWrapper<OrderMainDO> wrapper = Wrappers.<OrderMainDO>lambdaQuery()
//                .select(OrderMainDO::getAmount, OrderMainDO::getType, OrderMainDO::getEmpStatus, OrderMainDO::getEndDate)
//                .eq(OrderMainDO::getStatus, StatusEnum.PAID.getValue())
//                .in(OrderMainDO::getType, Arrays.asList(TypeEnum.HIRE.getValue(), TypeEnum.CUSTOM.getValue()))
//                .eq(OrderMainDO::getEmployeeId, userId);
//
//        List<OrderMainDO> orderList = list(wrapper);
//
//        if (CollectionUtil.isEmpty(orderList)) {
//            return overview;
//        }
//        long count = orderList.stream().filter(it -> EmpStatusEnum.getRunningList().contains(it.getEmpStatus())).count();
//        overview.setOrderMainCount(count);
//        BigDecimal totalAmount = orderList.stream()
//                .map(OrderMainDO::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
//        overview.setOrderMainHistoryAmount(totalAmount);
//
//        BigDecimal currentAmount = orderList.stream()
//                .filter(it -> EmpStatusEnum.getRunningList().contains(it.getEmpStatus()))
//                .map(OrderMainDO::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
//        overview.setOrderMainAmount(currentAmount);
//
//        long count1 = orderList.stream().filter(it -> TypeEnum.HIRE.getValue().equals(it.getType()) && EmpStatusEnum.FINISH.getValue().equals(it.getEmpStatus())).count();
//        overview.setPayMonthCount(count1);
//
//        long count2 = orderList.stream().filter(it -> TypeEnum.CUSTOM.getValue().equals(it.getType()) && EmpStatusEnum.FINISH.getValue().equals(it.getEmpStatus())).count();
//        overview.setPayCustomCount(count2);
//        long count3 = orderList.stream().filter(it -> EmpStatusEnum.FIRE.getValue().equals(it.getEmpStatus())).count();
//        overview.setFireCount(count3);
//        return overview;
//    }
//
//    @Override
//    public Page getMyEmployeePage(Page page, OrderListParam param, Long userId) {
//
//        LambdaQueryWrapper<OrderMainDO> wrapper = Wrappers.<OrderMainDO>lambdaQuery()
//                .eq(OrderMainDO::getEmployerId, userId)
//                .eq(OrderMainDO::getStatus, StatusEnum.PAID.getValue())
//                .in(OrderMainDO::getType, Arrays.asList(TypeEnum.HIRE.getValue(), TypeEnum.CUSTOM.getValue()));
//
//        if (param.getIsHistory()) {
//            wrapper.in(OrderMainDO::getEmpStatus, EmpStatusEnum.getHistoryList());
//        } else {
//            wrapper.in(OrderMainDO::getEmpStatus, EmpStatusEnum.getRunningList());
//        }
//
//        Page pageResult = page(page, wrapper);
//
//        List<OrderMainDO> records = pageResult.getRecords();
//
//        if (CollectionUtil.isEmpty(records)) {
//            return pageResult;
//        }
//
//        List<Long> employeeIdList = records.stream().map(OrderMainDO::getEmployeeId).collect(Collectors.toList());
//        Map<Long, ProductUserDTO> userIdAndUserList = usersService.getUserByIds(employeeIdList).stream().collect(Collectors.toMap(ProductUserDTO::getId, it -> it));
//        List<OrderListDTO> orderListDTO = records.stream().map(it -> {
//            OrderListDTO transform = BeanUtil.toBean(it, OrderListDTO.class);
//            transform.setEmployee(userIdAndUserList.get(it.getEmployeeId()));
//            return transform;
//        }).collect(Collectors.toList());
//        pageResult.setRecords(orderListDTO);
//        return pageResult;
//    }
//
//    @Override
//    public Page getMyEmployerPage(Page page, OrderListParam param, Long userId) {
//
//        LambdaQueryWrapper<OrderMainDO> wrapper = Wrappers.<OrderMainDO>lambdaQuery()
//                .eq(OrderMainDO::getStatus, StatusEnum.PAID.getValue())
//                .in(OrderMainDO::getType, Arrays.asList(TypeEnum.HIRE.getValue(), TypeEnum.CUSTOM.getValue()))
//                .eq(OrderMainDO::getEmployeeId, userId);
//
//        if (param.getIsHistory()) {
//            wrapper.in(OrderMainDO::getEmpStatus, EmpStatusEnum.getHistoryList());
//        } else {
//            wrapper.in(OrderMainDO::getEmpStatus, EmpStatusEnum.getRunningList());
//        }
//
//
//        Page pageResult = page(page, wrapper);
//        List<OrderMainDO> records = pageResult.getRecords();
//
//        if (CollectionUtil.isEmpty(records)) {
//            return pageResult;
//        }
//        List<Long> employerIdList = records.stream().map(OrderMainDO::getEmployerId).collect(Collectors.toList());
//        Map<Long, ProductUserDTO> userIdAndUserList = usersService.getUserByIds(employerIdList).stream().collect(Collectors.toMap(ProductUserDTO::getId, it -> it));
//        List<OrderListDTO> orderListDTO = records.stream().map(it -> {
//            OrderListDTO transform = BeanUtil.toBean(it, OrderListDTO.class);
//            transform.setEmployer(userIdAndUserList.get(it.getEmployerId()));
//            return transform;
//        }).collect(Collectors.toList());
//        pageResult.setRecords(orderListDTO);
//        return pageResult;
//    }
//
//    @Override
//    public Page getMyChargePage(Page page, OrderParam param, Long userId) {
//
//        LambdaQueryWrapper<OrderMainDO> wrapper = Wrappers.<OrderMainDO>lambdaQuery()
//                .eq(OrderMainDO::getType, TypeEnum.CHARGE.getValue())
//                .eq(OrderMainDO::getEmployerId, userId);
//
//        if (param.getStartDate() != null && param.getEndDate() != null) {
//            wrapper.between(OrderMainDO::getCreateDate, DateUtil.beginOfDay(param.getStartDate()), DateUtil.endOfDay(param.getEndDate()));
//        }
//
//        Page pageResult = page(page, wrapper);
//        List<OrderMainDO> records = pageResult.getRecords();
//
//        if (CollectionUtil.isEmpty(records)) {
//            return pageResult;
//        }
//        List<OrderListDTO> orderListDTO = records.stream().map(it -> BeanUtil.toBean(it, OrderListDTO.class)).collect(Collectors.toList());
//        pageResult.setRecords(orderListDTO);
//        return pageResult;
//    }
//
//    @Override
//    public Page getMyEmployPage(Page page, OrderParam param, Long userId) {
//
//        LambdaQueryWrapper<OrderMainDO> wrapper = Wrappers.<OrderMainDO>lambdaQuery()
//                .in(OrderMainDO::getType, Arrays.asList(TypeEnum.HIRE.getValue(), TypeEnum.CUSTOM.getValue()))
//                .eq(OrderMainDO::getEmployerId, userId);
//
//        if (param.getStartDate() != null && param.getEndDate() != null) {
//            wrapper.between(OrderMainDO::getCreateDate, DateUtil.beginOfDay(param.getStartDate()), DateUtil.endOfDay(param.getEndDate()));
//        }
//
//        Page pageResult = page(page, wrapper);
//        List<OrderMainDO> records = pageResult.getRecords();
//
//        if (CollectionUtil.isEmpty(records)) {
//            return pageResult;
//        }
//        List<OrderListDTO> orderListDTO = records.stream().map(it -> BeanUtil.toBean(it, OrderListDTO.class)).collect(Collectors.toList());
//        pageResult.setRecords(orderListDTO);
//        return pageResult;
//    }
//
//    @Override
//    public Page getOrderPagePlatform(Page page, OrderMainPOParam param) {
//
//        LambdaQueryWrapper<OrderMainDO> wrapper = Wrappers.<OrderMainDO>lambdaQuery()
//                .eq(StringUtil.isNotBlank(param.getCode()), OrderMainDO::getCode, param.getCode())
//                .eq(param.getType() != null, OrderMainDO::getType, param.getType())
//                .eq(param.getStatus() != null, OrderMainDO::getStatus, param.getStatus())
//                .like(StringUtil.isNotBlank(param.getEmployer()), OrderMainDO::getEmployer, param.getEmployer())
//                .eq(StringUtil.isNotBlank(param.getEmployee()), OrderMainDO::getEmployee, param.getEmployee())
//                .orderByDesc(OrderMainDO::getId);
//
//        Page pageResult = page(page, wrapper);
//        List<OrderMainDO> records = pageResult.getRecords();
//
//        if (CollectionUtil.isEmpty(records)) {
//            return pageResult;
//        }
//        List<OrderMainPO> orderListDTO = records.stream().map(it -> BeanUtil.toBean(it, OrderMainPO.class)).collect(Collectors.toList());
//        pageResult.setRecords(orderListDTO);
//        return pageResult;
//    }
//
//    @Override
//    public Page getHirePagePlatform(Page page, OrderHirePOParam param) {
//        LambdaQueryWrapper<OrderMainDO> wrapper = Wrappers.<OrderMainDO>lambdaQuery()
//                .eq(StringUtil.isNotBlank(param.getCode()), OrderMainDO::getCode, param.getCode())
//                .in(OrderMainDO::getType, Arrays.asList(TypeEnum.HIRE.getValue(), TypeEnum.CUSTOM.getValue()))
//                .eq(param.getEmpStatus() != null, OrderMainDO::getEmpStatus, param.getEmpStatus())
//                .like(StringUtil.isNotBlank(param.getEmployer()), OrderMainDO::getEmployer, param.getEmployer())
//                .eq(StringUtil.isNotBlank(param.getEmployee()), OrderMainDO::getEmployee, param.getEmployee())
//                .orderByDesc(OrderMainDO::getId);
//
//        Page pageResult = page(page, wrapper);
//
//        List<OrderMainDO> records = pageResult.getRecords();
//
//        if (CollectionUtil.isEmpty(records)) {
//            return pageResult;
//        }
//
//        List<OrderHirePO> orderListDTO = records.stream().map(it -> BeanUtil.toBean(it, OrderHirePO.class)).collect(Collectors.toList());
//        pageResult.setRecords(orderListDTO);
//        return pageResult;
//    }
//
//
//    Boolean isCharge(OrderMainDO main) {
//        return TypeEnum.CHARGE.getValue().equals(main.getType());
//    }
//
//    Boolean isHire(OrderMainDO main) {
//        return TypeEnum.CUSTOM.getValue().equals(main.getType()) || TypeEnum.HIRE.getValue().equals(main.getType());
//    }
//
//    Boolean isHire(Integer type) {
//        return TypeEnum.CUSTOM.getValue().equals(type) || TypeEnum.HIRE.getValue().equals(type);
//    }
//
//    Boolean isCouponPay(OrderMainDO main) {
//        if (isHire(main)) {
//            if (main.getUserCouponId() != null && main.getUserCouponAmount().compareTo(BigDecimal.ZERO) > 0) {
//                return true;
//            }
//        }
//        return false;
//    }
//}
