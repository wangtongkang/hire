//package com.chuangjian.hire.order.service.impl;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.alipay.api.domain.AlipayTradePrecreateModel;
//import com.alipay.api.domain.AlipayTradeWapPayModel;
//import com.alipay.api.request.AlipayTradePagePayRequest;
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.chuangjian.hire.common.exceptionEnum.ApiExceptionEnum;
//import com.chuangjian.hire.common.exption.ApiException;
//import com.chuangjian.hire.config.AliPayConfig;
//import com.chuangjian.hire.config.WxPayConfig;
//import com.chuangjian.hire.order.dao.OrderPayMapper;
//import com.chuangjian.hire.order.domain.OrderMainDO;
//import com.chuangjian.hire.order.domain.OrderPayDO;
//import com.chuangjian.hire.order.dto.PayDTO;
//import com.chuangjian.hire.order.enums.PayTypeEnum;
//import com.chuangjian.hire.order.enums.StatusEnum;
//import com.chuangjian.hire.order.enums.TypeEnum;
//import com.chuangjian.hire.order.service.OrderMainService;
//import com.chuangjian.hire.order.service.OrderPayService;
//import com.chuangjian.hire.util.OrderNumUtils;
//import com.chuangjian.hire.util.StringUtil;
//import com.ijpay.alipay.AliPayApi;
//import com.ijpay.alipay.AliPayApiConfig;
//import com.ijpay.alipay.AliPayApiConfigKit;
//import com.ijpay.core.enums.SignType;
//import com.ijpay.core.enums.TradeType;
//import com.ijpay.core.kit.WxPayKit;
//import com.ijpay.wxpay.WxPayApi;
//import com.ijpay.wxpay.WxPayApiConfig;
//import com.ijpay.wxpay.WxPayApiConfigKit;
//import com.ijpay.wxpay.model.UnifiedOrderModel;
//import lombok.SneakyThrows;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.annotation.Resource;
//import java.math.BigDecimal;
//import java.util.Map;
//
///**
// * FileName: OrderPayBusiness
// * Description:
// *
// * @author: CodeGenerator
// * @date: 2020/07/18 15:50
// * Copyright (C) 杭州同基汽车科技有限公司
// */
//@Slf4j
//@Transactional
//@Service("orderPayService")
//public class OrderPayServiceImpl extends ServiceImpl<OrderPayMapper, OrderPayDO> implements OrderPayService {
//
//    @Resource
//    OrderMainService orderMainService;
//
//    @Resource
//    AliPayConfig aliPayConfig;
//    @Resource
//    WxPayConfig wxPayConfig;
//
//
//    String subject = "创舰网-支付";
//
//    @Override
//    public String createPay(PayDTO pay) {
//
//        initPay();
//
//        OrderMainDO orderMainDO = orderMainService.getOrderById(pay.getOrderId());
//        OrderPayDO orderPayDO = new OrderPayDO();
//        orderPayDO.setOrderId(orderMainDO.getId());
//        orderPayDO.setCode(OrderNumUtils.getPayNum());
//        orderPayDO.setPayType(PayTypeEnum.getType(pay.getPayType()).getValue());
//        orderPayDO.setAmount(orderMainDO.getActualPay());
//        orderPayDO.setStatus(StatusEnum.PAYING.getValue());
//
//        String qrCodeUrl;
//
//        if (PayTypeEnum.WE_CHAT_WEB.getValue().equals(pay.getPayType())) {
//
//            qrCodeUrl = payWeChatScanCode(orderPayDO, orderMainDO);
//
//        } else if (PayTypeEnum.WE_CHAT_H5.getValue().equals(pay.getPayType())) {
//
//            qrCodeUrl = payWeChatH5(orderPayDO, pay, orderMainDO);
//
//        } else if (PayTypeEnum.ALI_WEB.getValue().equals(pay.getPayType())) {
//
//            qrCodeUrl = payALIWeb(orderPayDO, pay.getReturnUrl(), orderMainDO);
//
//        } else if (PayTypeEnum.ALI_H5.getValue().equals(pay.getPayType())) {
//
//            qrCodeUrl = payAliH5(orderPayDO, pay.getReturnUrl(), orderMainDO);
//
//        } else if (PayTypeEnum.BALANCE_PAY.getValue().equals(pay.getPayType())) {
//
//            qrCodeUrl = orderMainService.payBalance(pay);
//
//            orderPayDO.setStatus(StatusEnum.PAID.getValue());
//
//        } else {
//            throw new ApiException(ApiExceptionEnum.ERROR_PARAM);
//        }
//        save(orderPayDO);
//        return qrCodeUrl;
//    }
//
//
//    @SneakyThrows
//    String payWeChatScanCode(OrderPayDO orderPayDO, OrderMainDO orderMainDO) {
//
//        String notifyUrl = wxPayConfig.getDomain().concat(wxPayConfig.getNotifyUrl());
//
//        WxPayApiConfig wxPayApiConfig = WxPayApiConfigKit.getWxPayApiConfig();
//
////        String signKey = WxPayApi.getSignKey(WxPayApiConfigKit.getWxPayApiConfig().getMchId(),
////                WxPayApiConfigKit.getWxPayApiConfig().getPartnerKey(), SignType.MD5);
////        Map<String, String> signKeys = WxPayKit.xmlToMap(signKey);
//
//        Map<String, String> params = UnifiedOrderModel
//                .builder()
//                .appid(wxPayApiConfig.getAppId())
//                .mch_id(wxPayApiConfig.getMchId())
//                .nonce_str(WxPayKit.generateStr())
//                .body(getPayBody(orderMainDO))
//                .attach(subject)
//                .out_trade_no(orderPayDO.getCode())
//                .total_fee(orderPayDO.getAmount().multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_UP).toString())
//                .notify_url(notifyUrl)
//                .trade_type(TradeType.NATIVE.getTradeType())
//                .build()
////                .createSign(signKeys.get("sandbox_signkey"), SignType.MD5);
//                .createSign(wxPayConfig.getPartnerKey(), SignType.HMACSHA256);
//
//        String xmlResult = WxPayApi.pushOrder(wxPayConfig.getSandbox(), params);
//        log.info("统一下单:" + xmlResult);
//        Map<String, String> result = WxPayKit.xmlToMap(xmlResult);
//        String returnCode = result.get("return_code");
//        String returnMsg = result.get("return_msg");
//        log.info(returnMsg);
//        if (!WxPayKit.codeIsOk(returnCode)) {
//            throw new ApiException(ApiExceptionEnum.PAY_ERROR);
//        }
//        String resultCode = result.get("result_code");
//        if (!WxPayKit.codeIsOk(resultCode)) {
//            throw new ApiException(ApiExceptionEnum.PAY_ERROR);
//        }
//        //生成预付订单success
//        String qrCodeUrl = result.get("code_url");
////        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
////        QrCodeKit.encodeOutPutSteam(outputStream, qrCodeUrl, BarcodeFormat.QR_CODE, 3, ErrorCorrectionLevel.H, "png", 200, 200);
//
//        return qrCodeUrl;
//    }
//
//    @SneakyThrows
//    String payWeChatH5(OrderPayDO orderPayDO, PayDTO pay, OrderMainDO orderMainDO) {
//
//        if (StringUtil.isBlank(pay.getOpenId())) {
//            throw new ApiException(ApiExceptionEnum.ERROR_PARAM);
//        }
//
//
//        String notifyUrl = wxPayConfig.getDomain().concat(wxPayConfig.getNotifyUrl());
//
//        WxPayApiConfig wxPayApiConfig = WxPayApiConfigKit.getWxPayApiConfig();
//
//        Map<String, String> params = UnifiedOrderModel
//                .builder()
//                .appid(wxPayApiConfig.getAppId())
//                .mch_id(wxPayApiConfig.getMchId())
//                .nonce_str(WxPayKit.generateStr())
//                .body(getPayBody(orderMainDO))
//                .attach(subject)
//                .out_trade_no(orderPayDO.getCode())
//                .total_fee(orderPayDO.getAmount().multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_UP).toString())
//                .notify_url(notifyUrl)
//                .trade_type(TradeType.JSAPI.getTradeType())
//                .openid(pay.getOpenId())
//                .build()
//                .createSign(wxPayApiConfig.getPartnerKey(), SignType.HMACSHA256);
//
//        String xmlResult = WxPayApi.pushOrder(wxPayConfig.getSandbox(), params);
//
//        log.info(xmlResult);
//
//        Map<String, String> resultMap = WxPayKit.xmlToMap(xmlResult);
//        String returnCode = resultMap.get("return_code");
//        String returnMsg = resultMap.get("return_msg");
//        if (!WxPayKit.codeIsOk(returnCode)) {
//            throw new ApiException(ApiExceptionEnum.PAY_ERROR);
//        }
//        String resultCode = resultMap.get("result_code");
//        if (!WxPayKit.codeIsOk(resultCode)) {
//            throw new ApiException(ApiExceptionEnum.PAY_ERROR);
//        }
//
//        // 以下字段在 return_code 和 result_code 都为 SUCCESS 的时候有返回
//
//        String prepayId = resultMap.get("prepay_id");
//
//        Map<String, String> packageParams = WxPayKit.prepayIdCreateSign(prepayId, wxPayApiConfig.getAppId(),
//                wxPayApiConfig.getPartnerKey(), SignType.HMACSHA256);
//
//        String form = JSON.toJSONString(packageParams);
//        return form;
//    }
//
//    @SneakyThrows
//    String payALIWeb(OrderPayDO orderPayDO, String returnUrl, OrderMainDO orderMainDO) {
////        String storeId = "123";
//        String notifyUrl = aliPayConfig.getDomain().concat(aliPayConfig.getNotifyUrl());
//        AlipayTradePrecreateModel model = new AlipayTradePrecreateModel();
//        model.setProductCode("FAST_INSTANT_TRADE_PAY");
//        model.setSubject(subject);
//        model.setBody(getPayBody(orderMainDO));
//        model.setTotalAmount(orderPayDO.getAmount().toString());
////        model.setStoreId(storeId);
//        model.setTimeoutExpress("120m");
//        model.setOutTradeNo(orderPayDO.getCode());
//        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
//        request.setBizModel(model);
//        request.setNotifyUrl(notifyUrl);
//        request.setReturnUrl(returnUrl);
//        String form = AliPayApi.pageExecute(request).getBody();
//        return form;
//    }
//
//    @SneakyThrows
//    String payAliH5(OrderPayDO orderPayDO, String returnUrl, OrderMainDO orderMainDO) {
//        String passBackParams = "1";
//        String notifyUrl = aliPayConfig.getDomain().concat(aliPayConfig.getNotifyUrl());
//        AlipayTradeWapPayModel model = new AlipayTradeWapPayModel();
//        model.setSubject(subject);
//        model.setBody(getPayBody(orderMainDO));
//        model.setTotalAmount(orderPayDO.getAmount().toString());
//        model.setPassbackParams(passBackParams);
//        model.setOutTradeNo(orderPayDO.getCode());
//        model.setProductCode("QUICK_WAP_PAY");
//        String form = AliPayApi.wapPayStr(model, returnUrl, notifyUrl);
//        return form;
//    }
//
//
//    private String getPayBody(OrderMainDO orderMainDO) {
//
//        StringBuilder builder = new StringBuilder();
//        builder.append("创舰网");
//        if (TypeEnum.HIRE.getValue().equals(orderMainDO.getType())) {
//            builder.append("-包月雇佣设计师");
//        } else if (TypeEnum.CUSTOM.getValue().equals(orderMainDO.getType())) {
//            builder.append("-定制雇佣设计师");
//        } else {
//            builder.append("-充值");
//        }
//        return builder.toString();
//    }
//
//
//    @Override
//    @SneakyThrows
//    public void initPay() {
//
//        log.info("wxPayConfig {}", wxPayConfig);
//        log.info("aliPayConfig {}", aliPayConfig);
//        WxPayApiConfig apiConfig = WxPayApiConfig.builder()
//                .appId(wxPayConfig.getAppId())
//                .mchId(wxPayConfig.getMchId())
//                .partnerKey(wxPayConfig.getPartnerKey())
//                .certPath(wxPayConfig.getCertPath())
//                .domain(wxPayConfig.getDomain())
//                .build();
//        WxPayApiConfigKit.setThreadLocalWxPayApiConfig(apiConfig);
//
//        AliPayApiConfig aliPayApiConfig = AliPayApiConfig.builder()
//                .setAppId(aliPayConfig.getAppId())
//                .setAliPayPublicKey(aliPayConfig.getPublicKey())
//                .setAppCertPath(aliPayConfig.getAppCertPath())
//                .setAliPayCertPath(aliPayConfig.getAliPayCertPath())
//                .setAliPayRootCertPath(aliPayConfig.getAliPayRootCertPath())
//                .setCharset("UTF-8")
//                .setPrivateKey(aliPayConfig.getPrivateKey())
//                .setServiceUrl(aliPayConfig.getServerUrl())
//                .setSignType("RSA2")
//                // 普通公钥方式
//                .build();
//        // 证书模式
////                .buildByCert();
//        AliPayApiConfigKit.setThreadLocalAliPayApiConfig(aliPayApiConfig);
//    }
//
//    @Override
//    public void wxNotify(Map<String, String> parameter) {
//        String payCode = parameter.get("out_trade_no");
//        String trade_no = parameter.get("trade_id");
//        OrderPayDO orderPay = getOrderPay(payCode);
//        orderPay.setStatus(StatusEnum.PAID.getValue());
//        orderPay.setOutCode(trade_no);
//        String data = JSON.toJSONString(parameter);
//        orderPay.setPayResult(data);
//        updateById(orderPay);
//        orderMainService.paySuccess(orderPay.getOrderId(), orderPay.getPayType());
//    }
//
//    @Override
//    public void aliNotify(Map<String, String> parameter) {
//        String payCode = parameter.get("out_trade_no");
//        String trade_no = parameter.get("trade_no");
//        OrderPayDO orderPay = getOrderPay(payCode);
//        orderPay.setStatus(StatusEnum.PAID.getValue());
//        orderPay.setOutCode(trade_no);
//        String data = JSON.toJSONString(parameter);
//        orderPay.setPayResult(data);
//        updateById(orderPay);
//        orderMainService.paySuccess(orderPay.getOrderId(), orderPay.getPayType());
//    }
//
//    OrderPayDO getOrderPay(String payCode) {
//        return getOne(Wrappers.<OrderPayDO>lambdaQuery()
//                .eq(OrderPayDO::getCode, payCode)
//        );
//    }
//
//}
