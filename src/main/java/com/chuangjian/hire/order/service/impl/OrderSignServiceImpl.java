//package com.chuangjian.hire.order.service.impl;
//
//import cn.hutool.core.collection.CollectionUtil;
//import cn.hutool.core.date.DateUtil;
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.chuangjian.hire.order.dao.OrderSignMapper;
//import com.chuangjian.hire.order.domain.OrderMainDO;
//import com.chuangjian.hire.order.domain.OrderSignDO;
//import com.chuangjian.hire.order.dto.SignDTO;
//import com.chuangjian.hire.order.service.OrderMainService;
//import com.chuangjian.hire.order.service.OrderSignService;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.annotation.Resource;
//import java.util.Date;
//import java.util.List;
//import java.util.stream.Collectors;
//
///**
// * FileName: OrderSignBusiness
// * Description:
// *
// * @author: CodeGenerator
// * @date: 2020/07/17 09:08
// * Copyright (C) 杭州同基汽车科技有限公司
// */
//@Transactional
//@Service("orderSignService")
//public class OrderSignServiceImpl extends ServiceImpl<OrderSignMapper, OrderSignDO> implements OrderSignService {
//
//    @Resource
//    OrderMainService orderMainService;
//
//    @Override
//    public List<SignDTO> getUnSignList(Long userId) {
//        List<SignDTO> orderList = orderMainService.getRunningOrderList(userId);
//        List<Long> orderIdList = getHaveSignOrderId(userId);
//
//        return orderList.stream().filter(it -> !orderIdList.contains(it.getOrderId())).collect(Collectors.toList());
//    }
//
//    @Override
//    public Boolean signList(List<Long> orderIdList) {
//        if (CollectionUtil.isEmpty(orderIdList)) {
//            return Boolean.FALSE;
//        }
//
//        Date toDay = new Date();
//        List<OrderMainDO> mainDOList = orderMainService.getOrderByIdList(orderIdList);
//        List<OrderSignDO> signDOList = mainDOList.stream().map(it -> {
//            OrderSignDO sign = new OrderSignDO();
//            sign.setOrderId(it.getId());
//            sign.setEmployerId(it.getEmployerId());
//            sign.setEmployer(it.getEmployer());
//            sign.setEmployee(it.getEmployee());
//            sign.setEmployeeId(it.getEmployeeId());
//            sign.setSignDate(toDay);
//            return sign;
//        }).collect(Collectors.toList());
//        saveBatch(signDOList);
//        return true;
//    }
//
//    @Override
//    public Integer getSingDay(Long orderId) {
//        return count(Wrappers.<OrderSignDO>lambdaQuery().eq(OrderSignDO::getOrderId, orderId));
//    }
//
//
//    List<Long> getHaveSignOrderId(Long userId) {
//        Date toDay = new Date();
//        return list(Wrappers.<OrderSignDO>lambdaQuery()
//                .ge(OrderSignDO::getSignDate, DateUtil.beginOfDay(toDay))
//                .lt(OrderSignDO::getSignDate, DateUtil.endOfDay(toDay))
//                .eq(OrderSignDO::getEmployeeId, userId)
//        ).stream().map(OrderSignDO::getOrderId).collect(Collectors.toList());
//
//    }
//}
