//package com.chuangjian.hire.order.service.impl;
//
//import cn.hutool.core.bean.BeanUtil;
//import cn.hutool.core.date.DateUnit;
//import cn.hutool.core.date.DateUtil;
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.chuangjian.hire.common.exceptionEnum.ApiExceptionEnum;
//import com.chuangjian.hire.common.exption.ApiException;
//import com.chuangjian.hire.order.dao.OrderSuspendMapper;
//import com.chuangjian.hire.order.domain.OrderMainDO;
//import com.chuangjian.hire.order.domain.OrderSuspendDO;
//import com.chuangjian.hire.order.dto.SuspendDTO;
//import com.chuangjian.hire.order.enums.EmpStatusEnum;
//import com.chuangjian.hire.order.enums.SuspendStatusEnum;
//import com.chuangjian.hire.order.enums.SuspendTypeEnum;
//import com.chuangjian.hire.order.service.OrderSuspendService;
//import com.chuangjian.hire.users.dto.UserDTO;
//import com.chuangjian.hire.users.enums.TypeEnum;
//import com.chuangjian.hire.util.BeanUtils;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.annotation.Resource;
//
///**
// * FileName: OrderSuspendBusiness
// * Description:
// *
// * @author: CodeGenerator
// * @date: 2020/07/17 09:08
// * Copyright (C) 杭州同基汽车科技有限公司
// */
//@Slf4j
//@Transactional
//@Service("orderSuspendService")
//public class OrderSuspendServiceImpl extends ServiceImpl<OrderSuspendMapper, OrderSuspendDO> implements OrderSuspendService {
//
//    @Resource
//    OrderMainServiceImpl orderMainService;
//
//    @Override
//    public Boolean employerSuspend(SuspendDTO suspend, UserDTO user) {
//        if (suspend.getStartDate().after(suspend.getEndDate())) {
//            throw new ApiException(ApiExceptionEnum.ORDER_SUSPEND_TIME_ERROR);
//        }
//        OrderMainDO orderMain = orderMainService.getOrderById(suspend.getOrderId());
//        if (orderMain.getEmployerSuspendId() != null) {
//            throw new ApiException(ApiExceptionEnum.ORDER_SUSPEND_1_ERROR);
//        }
//        OrderSuspendDO suspendDO = new OrderSuspendDO();
//        suspendDO.setRemark(suspend.getRemark());
//        suspendDO.setOrderId(orderMain.getId());
//        suspendDO.setStartDate(DateUtil.beginOfDay(suspend.getStartDate()));
//        suspendDO.setEndDate(DateUtil.endOfDay(suspend.getEndDate()));
//
//        suspendDO.setType(SuspendTypeEnum.HIRE.getValue());
//        suspendDO.setStatus(SuspendStatusEnum.APPLYING.getValue());
//        suspendDO.setEmployee(orderMain.getEmployee());
//        suspendDO.setEmployeeId(orderMain.getEmployeeId());
//        suspendDO.setEmployer(orderMain.getEmployer());
//        suspendDO.setEmployerId(orderMain.getEmployerId());
//        save(suspendDO);
//        orderMainService.setEmployerSuspend(orderMain.getId(), suspendDO.getId());
//        return true;
//    }
//
//    @Override
//    public Boolean employerSuspendCancel(SuspendDTO suspend, UserDTO user) {
//        OrderSuspendDO suspendDO = getById(suspend.getId());
//        if (SuspendStatusEnum.SUCCESS.getValue().equals(suspendDO.getStatus())) {
//            long between = DateUtil.between(suspendDO.getStartDate(), suspendDO.getEndDate(), DateUnit.DAY);
//            orderMainService.changeEndDate(suspendDO.getOrderId(), -between);
//        }
//        suspendDO.setStatus(SuspendStatusEnum.CANCEL.getValue());
//        updateById(suspendDO);
//        orderMainService.setEmployerSuspend(suspendDO.getOrderId(), null);
//        return true;
//    }
//
//    @Override
//    public Boolean employeeSuspend(SuspendDTO suspend, UserDTO user) {
//        if (suspend.getStartDate().after(suspend.getEndDate())) {
//            throw new ApiException(ApiExceptionEnum.ORDER_SUSPEND_TIME_ERROR);
//        }
//        OrderMainDO orderMain = orderMainService.getOrderById(suspend.getOrderId());
//        if (orderMain.getEmployeeSuspendId() != null) {
//            throw new ApiException(ApiExceptionEnum.ORDER_SUSPEND_2_ERROR);
//        }
//        OrderSuspendDO suspendDO = new OrderSuspendDO();
//        suspendDO.setRemark(suspend.getRemark());
//        suspendDO.setOrderId(orderMain.getId());
//        suspendDO.setStartDate(DateUtil.beginOfDay(suspend.getStartDate()));
//        suspendDO.setEndDate(DateUtil.endOfDay(suspend.getEndDate()));
//
//        suspendDO.setType(SuspendTypeEnum.EMP.getValue());
//        suspendDO.setStatus(SuspendStatusEnum.APPLYING.getValue());
//        suspendDO.setEmployee(orderMain.getEmployee());
//        suspendDO.setEmployeeId(orderMain.getEmployeeId());
//        suspendDO.setEmployer(orderMain.getEmployer());
//        suspendDO.setEmployerId(orderMain.getEmployerId());
//        save(suspendDO);
//        orderMainService.setEmployeeSuspend(suspend.getOrderId(), suspendDO.getId());
//        return true;
//    }
//
//    @Override
//    public SuspendDTO getSuspendById(Long suspendId) {
//
//        SuspendDTO suspendDTO = BeanUtil.toBean(getById(suspendId), SuspendDTO.class);
//        long between = DateUtil.between(suspendDTO.getStartDate(), suspendDTO.getEndDate(), DateUnit.DAY);
//        suspendDTO.setDay(between);
//
//        return suspendDTO;
//    }
//
//    @Override
//    public Boolean auditSuspendApply(SuspendDTO suspend, UserDTO user) {
//        OrderSuspendDO suspendDO = getById(suspend.getId());
//        if (SuspendStatusEnum.SUCCESS.getValue().equals(suspend.getStatus())) {
//            suspendDO.setStatus(SuspendStatusEnum.SUCCESS.getValue());
//            if (SuspendTypeEnum.HIRE.getValue().equals(suspendDO.getType())) {
//                long between = DateUtil.between(suspendDO.getStartDate(), suspendDO.getEndDate(), DateUnit.DAY);
//                orderMainService.changeEndDate(suspendDO.getOrderId(), between);
//            }
//        } else if (SuspendStatusEnum.FAIL.getValue().equals(suspend.getStatus())) {
//            suspendDO.setStatus(SuspendStatusEnum.FAIL.getValue());
//            Integer type = user.getType();
//            if (TypeEnum.HIRE.getValue().equals(type)) {
//
//                orderMainService.setEmployeeSuspend(suspendDO.getOrderId(), null);
//
//            } else if (TypeEnum.DESIGNER.getValue().equals(type)) {
//
//                orderMainService.setEmployerSuspend(suspendDO.getOrderId(), null);
//
//            }
//
//        } else {
//            throw new ApiException(ApiExceptionEnum.ERROR_PARAM);
//        }
//        updateById(suspendDO);
//        return true;
//    }
//
//}
