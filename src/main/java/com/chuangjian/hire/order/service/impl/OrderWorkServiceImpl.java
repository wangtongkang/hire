//package com.chuangjian.hire.order.service.impl;
//
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.chuangjian.hire.order.dao.OrderWorkMapper;
//import com.chuangjian.hire.order.domain.OrderMainDO;
//import com.chuangjian.hire.order.domain.OrderWorkDO;
//import com.chuangjian.hire.order.dto.OrderWorkDTO;
//import com.chuangjian.hire.order.enums.WorkStatusEnum;
////import com.chuangjian.hire.order.service.OrderMainService;
//import com.chuangjian.hire.order.service.OrderWorkService;
//import com.chuangjian.hire.util.BeanUtils;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.annotation.Resource;
//import java.util.List;
//
///**
// * FileName: OrderWorkBusiness
// * Description:
// *
// * @author: CodeGenerator
// * @date: 2020/07/13 22:56
// * Copyright (C) 杭州同基汽车科技有限公司
// */
//@Transactional
//@Service("orderWorkService")
//public class OrderWorkServiceImpl extends ServiceImpl<OrderWorkMapper, OrderWorkDO> implements OrderWorkService {
//
//    @Resource
//    OrderMainService orderMainService;
//
//    @Override
//    public Boolean addOrderWork(OrderWorkDTO orderWork) {
//        OrderWorkDO orderWorkDO = BeanUtils.transform(OrderWorkDO.class, orderWork);
//        OrderMainDO orderMain = orderMainService.getOrderById(orderWork.getOrderId());
//        orderWorkDO.setEmployee(orderMain.getEmployee());
//        orderWorkDO.setEmployeeId(orderMain.getEmployeeId());
//        orderWorkDO.setEmployer(orderMain.getEmployer());
//        orderWorkDO.setEmployerId(orderMain.getEmployerId());
//        orderWorkDO.setStatus(WorkStatusEnum.RUNNING.getValue());
//        return save(orderWorkDO);
//    }
//
//    @Override
//    public Boolean cancelWork(OrderWorkDTO orderWork) {
//        OrderWorkDO work = getById(orderWork.getId());
//        work.setStatus(WorkStatusEnum.CANCEL.getValue());
//        return updateById(work);
//    }
//
//    @Override
//    public Boolean confirmWork(OrderWorkDTO orderWork) {
//        OrderWorkDO work = getById(orderWork.getId());
//        work.setStatus(WorkStatusEnum.FINISH.getValue());
//        return updateById(work);
//    }
//
//    @Override
//    public Boolean finishWork(OrderWorkDTO orderWork) {
//        OrderWorkDO work = getById(orderWork.getId());
//        work.setStatus(WorkStatusEnum.FINISH.getValue());
//        return updateById(work);
//    }
//
//    @Override
//    public List<OrderWorkDTO> getOrderWorkList(Long orderId) {
//        return BeanUtils.transformList(OrderWorkDTO.class,
//                list(Wrappers.<OrderWorkDO>lambdaQuery().eq(OrderWorkDO::getOrderId, orderId)
//                        .orderByDesc(OrderWorkDO::getEndDate)));
//    }
//}
