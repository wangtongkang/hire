//package com.chuangjian.hire.product.controller;
//
//import com.alibaba.fastjson.JSON;
//import com.baomidou.kisso.annotation.Login;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.chuangjian.hire.common.controller.BaseController;
//import com.chuangjian.hire.common.dto.R;
//import com.chuangjian.hire.common.exceptionEnum.ApiExceptionEnum;
//import com.chuangjian.hire.common.exption.ApiException;
//import com.chuangjian.hire.product.dto.SearchProductDTO;
//import com.chuangjian.hire.product.dto.UsersProductDTO;
//import com.chuangjian.hire.product.dto.UsersProductParam;
//import com.chuangjian.hire.product.service.UsersProductService;
//import com.chuangjian.hire.users.dto.UserDTO;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.*;
//
//import javax.annotation.Resource;
//
//
//@Slf4j
//@RestController
//@Api(tags = {"用户作品相关接口"})
//@RequestMapping("/usersProduct")
//public class UsersProductController extends BaseController {
//
//    @Resource
//    UsersProductService usersProductService;
//
//
//    @GetMapping("getProductListByUserId")
//    @ApiOperation(value = "获取用户作品")
//    public R getProductListByUserId(Page page, Long userId) {
//        return R.success(usersProductService.getProductListByUserId(page, userId, false));
//    }
//
//    @Login
//    @GetMapping("getMyProductList")
//    @ApiOperation(value = "获取自己的用户作品")
//    public R getMyProductList(Page page) {
//        return R.success(usersProductService.getProductListByUserId(page, getUserInfo().getId(), true));
//    }
//
//
//    @GetMapping("getProductById")
//    @ApiOperation(value = "获取用户作品详情")
//    public R getProductById(Long productId) {
//        return R.success(usersProductService.getProductById(productId));
//    }
//
//
//    @GetMapping("refreshThumbnail")
//    @ApiOperation(value = "刷新缩略图")
//    public R refreshThumbnail() {
//        usersProductService.refreshThumbnail();
//        return R.success(true);
//    }
//
//
//    @Login
//    @PostMapping("addOrUpdate")
//    @ApiOperation(value = "保存用户作品")
//    public R addOrUpdate(@Validated @RequestBody UsersProductDTO product) {
//        UserDTO userInfo = getUserInfo();
//        return R.success(usersProductService.addOrUpdate(product, userInfo.getId()));
//    }
//
//    @Login
//    @PostMapping("deleteProduct")
//    @ApiOperation(value = "删除 用户作品")
//    public R deleteProduct(@Validated @RequestBody UsersProductDTO product) {
//        return R.success(usersProductService.deleteProduct(product));
//    }
//
//
//    @PostMapping("searchProduct")
//    @ApiOperation(value = "搜索用户作品")
//    public R searchProduct(Page page, @RequestBody SearchProductDTO search) {
//        return R.success(usersProductService.searchProduct(page, search));
//    }
//
//
//    @PostMapping("notify")
//    @ApiOperation(value = "压缩图片通知")
//    public R notify(@RequestBody Object o) {
//        log.info("压缩图片通知: {}", JSON.toJSONString(o));
//        return R.success(true);
//    }
//
//
//    @PostMapping("getRecommendProduct")
//    @ApiOperation(value = "获取推荐用户作品")
//    public R getRecommendProduct(Page page) {
//        return R.success(usersProductService.getRecommendProduct(page));
//    }
//
//
//    @Login
//    @PostMapping("addOrUpdateByPlatform")
//    @ApiOperation(value = "后台 新增修改用户作品", tags = {
//            "后台作品"
//    })
//    public R addOrUpdateByPlatform(@Validated @RequestBody UsersProductDTO product) {
//        if (product.getUserId() == null) {
//            throw new ApiException(ApiExceptionEnum.ERROR_PARAM);
//        }
//        return R.success(usersProductService.addOrUpdate(product, product.getUserId()));
//    }
//
//    @Login
//    @PostMapping("getProductPageListPlatform")
//    @ApiOperation(value = "后台用户作品列表", tags = {
//            "后台作品"
//    })
//    public R getProductPageListPlatform(Page page, @RequestBody UsersProductParam search) {
//        return R.success(usersProductService.getProductPageListPlatform(page, search));
//    }
//}
