package com.chuangjian.hire.product.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.product.domain.UsersProductDO;
import com.github.flyingglass.mybatis.cache.MybatisRedisCache;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Property;



@CacheNamespace(
        implementation = MybatisRedisCache.class,
        properties = {@Property(
                name = "flushInterval",
                value = "3600000"
        )}
)
public interface UsersProductMapper extends BaseMapper<UsersProductDO> {

}
