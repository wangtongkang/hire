package com.chuangjian.hire.product.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;

/**
* FileName: UsersProductBusiness
* Description:
* @author: CodeGenerator
* @date: 2020/07/20 16:40
* Copyright (C) 杭州同基汽车科技有限公司
*/
@Data
@TableName("users_product")
public class UsersProductDO extends Model<UsersProductDO> {

	/**
	* 经验ID
	*/
	@TableId(type = IdType.AUTO)
	private java.lang.Long id;
	/**
	* 用户Id
	*/
	private java.lang.Long userId;
	/**
	* 工作年限
	*/
	private java.lang.Integer workAge;
	/**
	* 作品标题
	*/
	private java.lang.String title;
	/**
	* 作品url
	*/
	private java.lang.String url;
	/**
	* 作品url
	*/
	private java.lang.String thumbnail;
	/**
	* 工作理念
	*/
	private java.lang.String intro;
	/**
	* 作品标签 逗号隔开
	*/
	private java.lang.String label;
	/**
	* 作品标题
	*/
	private java.lang.String catalog;
	/**
	* 目录Id
	*/
	private java.lang.Long catalogId;
	/**
	* 区域
	*/
	private java.lang.String area;
	/**
	* 区域Id
	*/
	private java.lang.Long areaId;
	/**
	* 是否可见，0.不可见，1.可见
	*/
	private java.lang.Integer visible;
	/**
	* 作品排序 数字越大越靠前
	*/
	private java.lang.Byte weight;
	/**
	* 作品浏览量
	*/
	private java.lang.Integer hits;
	/**
	* 喜欢数量
	*/
	private java.lang.Integer liked;
	/**
	* 推荐作品顺序 0 不推荐
	*/
	private java.lang.Integer recommendSort;
	/**
	* 创建人
	*/
	@TableField(value = "creator", fill = FieldFill.INSERT)
	private java.lang.String creator;
	/**
	* 创建人id
	*/
	@TableField(value = "creator_id", fill = FieldFill.INSERT)
	private java.lang.Long creatorId;
	/**
	* 创建时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField(value = "create_date", fill = FieldFill.INSERT)
	private java.util.Date createDate;
	/**
	* 修改人id
	*/
	@TableField(value = "modifier_id", fill = FieldFill.UPDATE)
	private java.lang.Long modifierId;
	/**
	* 修改人
	*/
	@TableField(value = "modifier", fill = FieldFill.UPDATE)
	private java.lang.String modifier;
	/**
	* 修改时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField(value = "modify_date", fill = FieldFill.UPDATE)
	private java.util.Date modifyDate;
	/**
	* 是否有效，0.删除，1.有效
	*/
	@TableLogic
	private java.lang.Boolean valid;

    @Override
    protected Serializable pkVal() {
    	return this.id;
    }
}
