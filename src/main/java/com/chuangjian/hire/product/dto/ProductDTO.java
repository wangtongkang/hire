package com.chuangjian.hire.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
@ApiModel("用户作品")
public class ProductDTO {

    @ApiModelProperty("作品id")
    private Long id;

    @ApiModelProperty("用户 id")
    private java.lang.Long userId;

    @ApiModelProperty("作品url")
    private String url;

    @ApiModelProperty("作品url")
    private java.lang.String thumbnail;
}
