package com.chuangjian.hire.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("介绍")
public class ProductUserDTO {

    @ApiModelProperty("用户")
    private Long id;
    @ApiModelProperty("头像")
    private String icon;
    @ApiModelProperty("昵称")
    private String nickName;
    @ApiModelProperty("年龄")
    private java.lang.Integer age;
    @ApiModelProperty("设计师级别 == 工作年限")
    private java.lang.Integer workAge;
    @ApiModelProperty("薪资")
    private java.math.BigDecimal payMonth;
    @ApiModelProperty("城市")
    private java.lang.String city;

    //订单列表才有的字段
    @ApiModelProperty("手机号")
    private java.lang.String phone;

    @ApiModelProperty("QQ")
    private java.lang.String qq;
}
