package com.chuangjian.hire.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("搜索内容")
public class SearchProductDTO {

    @ApiModelProperty("搜索内容")
    private String content;

    @ApiModelProperty("区域Id")
    private java.lang.Long areaId;

    @ApiModelProperty("擅长类目")
    private Long catalogId;

    @ApiModelProperty("设计师级别")
    private Integer level;
}
