package com.chuangjian.hire.product.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;


@Data
@ApiModel("用户作品")
public class UsersProductDTO {

    ProductUserDTO user;

    @ApiModelProperty("作品id")
    private java.lang.Long id;


    @ApiModelProperty("用户Id")
    private java.lang.Long userId;

    @ApiModelProperty("作品标题")
    @NotNull(message = "作品标题 不能为空")
    private java.lang.String title;

    @ApiModelProperty("作品url")
    @NotNull(message = "作品url 不能为空")
    private java.lang.String url;

    @ApiModelProperty("作品缩略图url")
    private java.lang.String thumbnail;

    @ApiModelProperty("工作理念")
    private java.lang.String intro;

    @ApiModelProperty("作品标签 逗号隔开")
    @NotNull(message = "作品工作理念")
    private java.lang.String label;

    @ApiModelProperty("目录")
    @NotNull(message = "目录不能为空")
    private java.lang.String catalog;

    @ApiModelProperty("目录Id")
    @NotNull(message = "目录Id不能为空")
    private java.lang.Long catalogId;

    @ApiModelProperty("区域")
    @NotNull(message = "区域 不能为空")
    private java.lang.String area;

    @ApiModelProperty("区域Id")
    @NotNull(message = "区域Id 不能为空")
    private java.lang.Long areaId;

    @ApiModelProperty("是否可见，0.不可见，1.可见")
    private java.lang.Integer visible;

    @ApiModelProperty("作品排序 数字越大越靠前")
    private java.lang.Integer weight;

    @ApiModelProperty("作品浏览量")
    private java.lang.Integer hits;

    @ApiModelProperty("喜欢数量")
    private java.lang.Integer liked;

    @ApiModelProperty("推荐作品顺序 0 不推荐")
    private java.lang.Integer recommendSort;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("创建时间")
    private java.util.Date createDate;
}
