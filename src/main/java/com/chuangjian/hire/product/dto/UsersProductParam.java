package com.chuangjian.hire.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
@ApiModel("用户作品列表搜索参数")
public class UsersProductParam {

    @ApiModelProperty("设计师Id")
    private Long userId;

    @ApiModelProperty("作品标题")
    private java.lang.String title;

}
