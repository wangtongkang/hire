package com.chuangjian.hire.product.enums;

public enum Visible {
    //(1,雇主 2,设计师,3,后台用户)
    INVISIBLE(0, "不可见"),
    VISIBLE(1, "可见");

    private Integer value;
    private String desc;

    private Visible(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static Visible getType(Integer value) {
        if (value == null) {
            return null;
        }
        for (Visible visiable : values()) {
            if (visiable.value.equals(value)) {
                return visiable;
            }
        }
        return null;
    }


    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
