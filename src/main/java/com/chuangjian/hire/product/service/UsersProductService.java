//package com.chuangjian.hire.product.service;
//
//
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.chuangjian.hire.product.dto.ProductDTO;
//import com.chuangjian.hire.product.dto.SearchProductDTO;
//import com.chuangjian.hire.product.dto.UsersProductDTO;
//import com.chuangjian.hire.product.dto.UsersProductParam;
//import com.chuangjian.hire.users.dto.UserDTO;
//
//import java.util.List;
//import java.util.Map;
//
//
///**
// * FileName: UsersProductBusiness
// * Description:
// *
// * @author: CodeGenerator
// * @date: 2020/07/12 23:29
// * Copyright (C) 杭州同基汽车科技有限公司
// */
//public interface UsersProductService {
//
//    UsersProductDTO getProductById(Long productId);
//
//    void refreshThumbnail();
//
//    Boolean addOrUpdate(UsersProductDTO product, Long userId);
//
//    Boolean deleteProduct(UsersProductDTO product);
//
//    Page<UsersProductDTO> searchProduct(Page page, SearchProductDTO search);
//
//    Page<UsersProductDTO> getRecommendProduct(Page page);
//
//    Map<Long, List<ProductDTO>> getProductList(List<Long> userIdList);
//
//    Page getProductListByUserId(Page page, Long userId, Boolean showAll);
//
//    Page<UsersProductDTO> getProductPageListPlatform(Page page, UsersProductParam param);
//}
