//package com.chuangjian.hire.product.service.impl;
//
//import cn.hutool.core.bean.BeanUtil;
//import cn.hutool.core.collection.CollectionUtil;
//import cn.hutool.core.util.RandomUtil;
//import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.chuangjian.hire.product.dao.UsersProductMapper;
//import com.chuangjian.hire.product.domain.UsersProductDO;
//import com.chuangjian.hire.product.dto.*;
//import com.chuangjian.hire.product.enums.Visible;
//import com.chuangjian.hire.product.service.UsersProductService;
//import com.chuangjian.hire.qiniu.config.QiNiuConfig;
//import com.chuangjian.hire.qiniu.service.QiNiuTokenGetter;
//import com.chuangjian.hire.users.domain.UsersDO;
//import com.chuangjian.hire.users.dto.UserDTO;
//import com.chuangjian.hire.users.service.UsersService;
//import com.chuangjian.hire.util.StringUtil;
//import com.qiniu.util.UrlSafeBase64;
//import lombok.SneakyThrows;
//import lombok.extern.slf4j.Slf4j;
//import okhttp3.OkHttpClient;
//import okhttp3.Request;
//import okhttp3.Response;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.annotation.Resource;
//import java.util.*;
//import java.util.stream.Collectors;
//
///**
// * FileName: UsersProductBusiness
// * Description:
// *
// * @author: CodeGenerator
// * @date: 2020/07/12 23:29
// * Copyright (C) 杭州同基汽车科技有限公司
// */
//@Slf4j
//@Transactional
//@Service("usersProductService")
//public class UsersProductServiceImpl extends ServiceImpl<UsersProductMapper, UsersProductDO> implements UsersProductService {
//
//    @Resource
//    UsersService usersService;
//
//    @Resource
//    QiNiuTokenGetter qiNiuTokenGetter;
//    @Resource
//    QiNiuConfig qiNiuConfig;
//
//    @Override
//    public UsersProductDTO getProductById(Long productId) {
//
//        UsersProductDO productDO = getById(productId);
//        UsersProductDTO product = BeanUtil.toBean(productDO, UsersProductDTO.class);
//        product.setUser(usersService.getProductUserById(productDO.getUserId()));
//        return product;
//    }
//
//    OkHttpClient okHttpClient = new OkHttpClient(); // 创建OkHttpClient对象
//
//    @Override
//    @SneakyThrows
//    public Boolean addOrUpdate(UsersProductDTO product, Long userId) {
//        UsersProductDO productDO = BeanUtil.toBean(product, UsersProductDO.class);
//        if (productDO.getId() == null) {
//            productDO.setUserId(userId);
//            productDO.setHits(RandomUtil.randomInt(10, 1000));
//            productDO.setLiked(RandomUtil.randomInt(10, 600));
//        }
//        if (StringUtils.isBlank(productDO.getThumbnail()) && StringUtils.isNotBlank(productDO.getUrl())) {
//            String newKy = getNewKy(productDO.getUrl());
//            String bukKey = qiNiuConfig.getBucket_name() + ":" + newKy;
//            String saves = "|saveas/" + UrlSafeBase64.encodeToString(bukKey);
//            String thumbnail = "image.meigong999.com/" + productDO.getUrl() + "?imageMogr2/crop/x900/thumbnail/!292" + saves;
//            String sign = qiNiuTokenGetter.getAuth().sign(thumbnail);
//            thumbnail = "http://" + thumbnail + "/sign/" + sign;
//            Request request = new Request.Builder().url(thumbnail).build(); // 创建一个请求
//            Response response = okHttpClient.newCall(request).execute(); // 返回实体
//            log.info("图片压缩结果:{}", response.body().string());
//            productDO.setThumbnail(newKy);
//        }
//        UsersDO users = usersService.findById(userId);
//        productDO.setWorkAge(users.getWorkAge());
//        return saveOrUpdate(productDO);
//    }
//
//
//    @Override
//    public void refreshThumbnail() {
//        List<UsersProductDO> list = list(Wrappers.lambdaQuery());
//
//        list.forEach(it -> {
//            try {
//                if (StringUtils.isBlank(it.getThumbnail()) && StringUtils.isNotBlank(it.getUrl())) {
//                    String newKy = getNewKy(it.getUrl());
//                    String bukKey = qiNiuConfig.getBucket_name() + ":" + newKy;
//                    String saves = "|saveas/" + UrlSafeBase64.encodeToString(bukKey);
//                    String thumbnail = "image.meigong999.com/" + it.getUrl() + "?imageMogr2/crop/x900/thumbnail/!292" + saves;
//                    String sign = qiNiuTokenGetter.getAuth().sign(thumbnail);
//                    thumbnail = "http://" + thumbnail + "/sign/" + sign;
//                    Request request = new Request.Builder().url(thumbnail).build(); // 创建一个请求
//                    Response response = okHttpClient.newCall(request).execute(); // 返回实体
//                    log.info("图片压缩结果:{}", response.body().string());
//                    it.setThumbnail(newKy);
//                    updateById(it);
//                }
//            } catch (Exception e) {
//                log.error("错误", e);
//            }
//        });
//    }
//
//    private static String getNewKy(String key) {
//        int dot = key.lastIndexOf(".");
//        String key1 = key.substring(0, dot);
//        String suffix = key.substring(dot);
//        String newFileName = key1 + "-thumbnail" + suffix;
//        return newFileName;
//    }
//
//    public static void main(String[] args) {
//        String url = "user/Fidx0U61KyaKHPYzIXHbjqKu0x6P.jpg";
//        String newKy = getNewKy(url);
//        System.out.println(newKy);
//    }
//
//    @Override
//    public Boolean deleteProduct(UsersProductDTO product) {
//        return removeById(product.getId());
//    }
//
//    @Override
//    public Page<UsersProductDTO> searchProduct(Page page, SearchProductDTO search) {
//
//        LambdaQueryWrapper wrapper = Wrappers.<UsersProductDO>lambdaQuery()
//                .and(StringUtils.isNotBlank(search.getContent()), it -> it.like(UsersProductDO::getTitle, search.getContent()).or().like(UsersProductDO::getLabel, search.getContent()))
//                .eq(search.getAreaId() != null, UsersProductDO::getAreaId, search.getAreaId())
//                .eq(search.getCatalogId() != null, UsersProductDO::getCatalogId, search.getCatalogId())
//                .eq(search.getLevel() != null, UsersProductDO::getWorkAge, search.getLevel())
//                .eq(UsersProductDO::getVisible, Visible.VISIBLE.getValue())
//                .orderByDesc(UsersProductDO::getLiked, UsersProductDO::getHits, UsersProductDO::getWeight);
//
//        Page<UsersProductDO> pageResult = page(page, wrapper);
//        List<UsersProductDO> productDOList = pageResult.getRecords();
//        Map<Long, ProductUserDTO> userIDAndProductionUsers = usersService.getUserByIds(productDOList.stream().map(UsersProductDO::getUserId).collect(Collectors.toList()))
//                .stream().collect(Collectors.toMap(ProductUserDTO::getId, it -> it));
//
//        List<UsersProductDTO> records = productDOList.stream().map(it -> {
//            UsersProductDTO product = BeanUtil.toBean(it, UsersProductDTO.class);
//            product.setUser(userIDAndProductionUsers.get(it.getUserId()));
//            return product;
//        }).collect(Collectors.toList());
//        page.setRecords(records);
//        return page;
//    }
//
//
//    @Override
//    public Page<UsersProductDTO> getRecommendProduct(Page page) {
//        LambdaQueryWrapper wrapper = Wrappers.<UsersProductDO>lambdaQuery()
//                .eq(UsersProductDO::getVisible, Visible.VISIBLE.getValue())
//                .orderByDesc(UsersProductDO::getRecommendSort)
//                .gt(UsersProductDO::getRecommendSort, 0);
//        Page<UsersProductDO> pageResult = page(page, wrapper);
//        List<UsersProductDO> productDOList = pageResult.getRecords();
//        Map<Long, ProductUserDTO> userIDAndProductionUsers = usersService.getUserByIds(productDOList.stream().map(UsersProductDO::getUserId).collect(Collectors.toList()))
//                .stream().collect(Collectors.toMap(ProductUserDTO::getId, it -> it));
//
//        List<UsersProductDTO> records = productDOList.stream().map(it -> {
//            UsersProductDTO product = BeanUtil.toBean(it, UsersProductDTO.class);
//            product.setUser(userIDAndProductionUsers.get(it.getUserId()));
//            return product;
//        }).collect(Collectors.toList());
//        page.setRecords(records);
//        return page;
//    }
//
//    @Override
//    public Map<Long, List<ProductDTO>> getProductList(List<Long> userIdList) {
//        return list(
//                Wrappers.<UsersProductDO>lambdaQuery()
//                        .in(UsersProductDO::getUserId, userIdList)
//                        .eq(UsersProductDO::getVisible, Visible.VISIBLE.getValue())
//                        .orderByDesc(UsersProductDO::getWeight))
//                .stream()
//                .map(it -> BeanUtil.toBean(it, ProductDTO.class))
//                .collect(Collectors.groupingBy(ProductDTO::getUserId, HashMap::new, Collectors.toCollection(ArrayList::new)));
//    }
//
//    @Override
//    public Page getProductListByUserId(Page pages, Long userId, Boolean showAll) {
//
//
//        Page pageResult = page(pages,
//                Wrappers.<UsersProductDO>lambdaQuery()
//                        .eq(UsersProductDO::getUserId, userId)
//                        .eq(showAll, UsersProductDO::getVisible, Visible.VISIBLE.getValue())
//                        .orderByDesc(UsersProductDO::getWeight));
//
//        List<UsersProductDO> usersProductDOList = pageResult.getRecords();
//
//        if (CollectionUtil.isEmpty(usersProductDOList)) {
//            return pageResult;
//        }
//        Map<Long, ProductUserDTO> userIDAndProductionUsers = usersService.getUserByIds(usersProductDOList.stream().map(UsersProductDO::getUserId).collect(Collectors.toList()))
//                .stream().collect(Collectors.toMap(ProductUserDTO::getId, it -> it));
//
//        pageResult.setRecords(usersProductDOList.stream()
//                .map(it -> {
//                    UsersProductDTO usersProduct = BeanUtil.toBean(it, UsersProductDTO.class);
//                    usersProduct.setUser(userIDAndProductionUsers.get(it.getUserId()));
//                    return usersProduct;
//                }).collect(Collectors.toList()));
//
//        return pageResult;
//    }
//
//    @Override
//    public Page<UsersProductDTO> getProductPageListPlatform(Page page, UsersProductParam param) {
//
//
//        Page pageResult = page(page,
//                Wrappers.<UsersProductDO>lambdaQuery()
//                        .eq(param.getUserId() != null, UsersProductDO::getUserId, param.getUserId())
//                        .like(StringUtil.isNotBlank(param.getTitle()), UsersProductDO::getTitle, param.getTitle())
//                        .orderByDesc(UsersProductDO::getRecommendSort, UsersProductDO::getLiked, UsersProductDO::getHits, UsersProductDO::getWeight));
//
//        List<UsersProductDO> records = pageResult.getRecords();
//        if (CollectionUtil.isEmpty(records)) {
//            return pageResult;
//        }
//
//        Map<Long, ProductUserDTO> userIDAndProductionUsers = usersService.getUserByIds(
//                records.stream().map(UsersProductDO::getUserId).collect(Collectors.toList()))
//                .stream().collect(Collectors.toMap(ProductUserDTO::getId, it -> it));
//
//        List<UsersProductDTO> usersProductList = records.stream()
//                .map(it -> {
//                    UsersProductDTO usersProduct = BeanUtil.toBean(it, UsersProductDTO.class);
//                    usersProduct.setUser(userIDAndProductionUsers.get(it.getUserId()));
//                    return usersProduct;
//                }).collect(Collectors.toList());
//
//        pageResult.setRecords(usersProductList);
//
//        return page;
//    }
//}
