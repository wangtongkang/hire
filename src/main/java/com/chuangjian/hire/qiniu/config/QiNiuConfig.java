package com.chuangjian.hire.qiniu.config;

import com.qiniu.common.Zone;
import com.qiniu.storage.BucketManager;
import com.qiniu.util.Auth;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "qiniu")
public class QiNiuConfig {

    private String access_key;

    private String secret_key;

    private String bucket_name;

    private String domain;

    private String notifyUrl;


    com.qiniu.storage.Configuration cfg = new com.qiniu.storage.Configuration(Zone.huanan());

    public BucketManager getBucketManager() {
        Auth auth = Auth.create(access_key, secret_key);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        return bucketManager;
    }
}
