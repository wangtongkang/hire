package com.chuangjian.hire.qiniu.controller;

import com.chuangjian.hire.qiniu.dto.TokenDTO;
import com.chuangjian.hire.qiniu.service.QiNiuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Api(tags = "七牛token获取")
public class QiNiuController {

    @Resource
    QiNiuService qiNiuService;

    @ApiOperation("获取七牛token  1,设计师图片前缀 2,作品图片前缀")
    @GetMapping("getUploadToken")
    public TokenDTO getUploadToken(Integer prefixType) {
        return qiNiuService.getUploadToken(prefixType);
    }

}
