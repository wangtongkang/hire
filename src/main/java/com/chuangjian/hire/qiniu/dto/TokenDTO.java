package com.chuangjian.hire.qiniu.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("token返回类型")
public class TokenDTO {

    @ApiModelProperty("前缀域名")
    private String domain = "http://image.meigong999.com";

    @ApiModelProperty("token")
    private String token;

    public TokenDTO() {
    }

    public TokenDTO(String token) {
        this.token = token;
    }
}
