package com.chuangjian.hire.qiniu.enums;

/**
 * 获取前缀类型
 */
public enum QiNiuTokenTypeEnums {

    USER_IMG_PREFIX(1, "user/", "用户图片前缀"),
    PRODUCTION_IMG_PREFIX(2, "production/", "作品图片前缀"),
    ;

    private Integer type;

    private String pathPrefix;

    private String remark;

    QiNiuTokenTypeEnums(Integer type, String pathPrefix, String remark) {
        this.type = type;
        this.pathPrefix = pathPrefix;
        this.remark = remark;
    }

    public static QiNiuTokenTypeEnums getByType(Integer status) {
        for (QiNiuTokenTypeEnums value : values()) {
            if (value.type.equals(status)) {
                return value;
            }
        }
        return null;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPathPrefix() {
        return pathPrefix;
    }

    public void setPathPrefix(String pathPrefix) {
        this.pathPrefix = pathPrefix;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
