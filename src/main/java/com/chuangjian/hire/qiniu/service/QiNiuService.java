package com.chuangjian.hire.qiniu.service;


import com.chuangjian.hire.qiniu.dto.TokenDTO;

/**
 * 获取七牛Token
 */
public interface QiNiuService {
    /**
     * 获取询价配件上传token
     */
    TokenDTO getUploadToken(Integer prefixType);


    /**
     * 七牛上传图片
     *
     * @param prefixType 请使用 QiniuTokenTypeEnums 里面的枚举值
     * @param fileName   默认不指定fileName的情况下，以文件内容的hash值作为文件名
     * @param fileByte   文件字节
     * @return 返回key 图片地址等于= domain+${key}
     */
    String uploadFile(Integer prefixType, String fileName, byte[] fileByte);


    /**
     * 删除文件
     *
     * @param key 文件的key
     * @return 是否删除成功
     */
    Boolean deleteFile(String key);

    /**
     * desc 获取覆盖上传token
     *
     * author zhujunjie
     * date 2019-03-21 15:59
     * @param
     * @return
     */
    String getOverrideToken(String originKey);
}
