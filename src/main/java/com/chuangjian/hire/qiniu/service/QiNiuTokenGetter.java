package com.chuangjian.hire.qiniu.service;

import com.qiniu.processing.OperationManager;
import com.qiniu.util.Auth;

/**
 * token获取
 */
public interface QiNiuTokenGetter {
    String getVideoSimpleToken(String bucketName, String priFix);

    String getSimpleToken(String bucketName, String priFix);

    String getReplaceToken(String bucketName, String originKey);

    OperationManager getOperationManager();

    Auth getAuth();
}
