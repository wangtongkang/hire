package com.chuangjian.hire.qiniu.service.impl;

import com.alibaba.fastjson.JSON;
import com.chuangjian.hire.common.exceptionEnum.ApiExceptionEnum;
import com.chuangjian.hire.common.exption.ApiException;
import com.chuangjian.hire.qiniu.config.QiNiuConfig;
import com.chuangjian.hire.qiniu.dto.TokenDTO;
import com.chuangjian.hire.qiniu.enums.QiNiuTokenTypeEnums;
import com.chuangjian.hire.qiniu.service.QiNiuService;
import com.chuangjian.hire.qiniu.service.QiNiuTokenGetter;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 七牛token获取
 */
@Slf4j
@Component
public class QiNiuServiceImpl implements QiNiuService {

    @Resource
    QiNiuConfig qiNiuConfig;

    @Autowired
    private QiNiuTokenGetter qiniuTokenGetter;

    @Override
    public TokenDTO getUploadToken(Integer prefixType) {
        QiNiuTokenTypeEnums prefixTypeE = QiNiuTokenTypeEnums.getByType(prefixType);
        if (prefixTypeE == null) {
            throw new ApiException(ApiExceptionEnum.ERROR_PARAM);
        }
        return new TokenDTO(qiniuTokenGetter.getSimpleToken(qiNiuConfig.getBucket_name(), prefixTypeE.getPathPrefix()));
    }

    @Override
    public String uploadFile(Integer prefixType, String fileName, byte[] fileByte) {
        UploadManager uploadManager = new UploadManager(qiNiuConfig.getCfg());
        String upToken = getUploadToken(prefixType).getToken();
        try {
            if (StringUtils.isNotBlank(fileName)) {
                fileName = QiNiuTokenTypeEnums.getByType(prefixType).getPathPrefix().concat(fileName);
            }
            Response response = uploadManager.put(fileByte, fileName, upToken);
            //解析上传成功的结果
            DefaultPutRet putRet = JSON.parseObject(response.bodyString(), DefaultPutRet.class);
            return putRet.key;
        } catch (QiniuException ex) {
            Response r = ex.response;
            try {
                log.error(r.toString());
                log.error(r.bodyString());
            } catch (QiniuException e) {
                log.error("七牛云上传失败", e);
            }
        }
        return null;
    }

    @Override
    public Boolean deleteFile(String key) {
        try {
            Response delete = qiNiuConfig.getBucketManager().delete(qiNiuConfig.getBucket_name(), key);
            System.out.println(delete.bodyString());
            return true;
        } catch (QiniuException ex) {
            Response r = ex.response;
            try {
                log.error(r.toString());
                log.error(r.bodyString());
            } catch (QiniuException e) {
                log.error("七牛云删除文件失败", e);
            }
        }

        return false;

    }

    @Override
    public String getOverrideToken(String originKey) {
        return qiniuTokenGetter.getReplaceToken(qiNiuConfig.getBucket_name(), originKey);
    }
}
