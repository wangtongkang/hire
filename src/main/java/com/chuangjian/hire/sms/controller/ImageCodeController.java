package com.chuangjian.hire.sms.controller;

import com.chuangjian.hire.sms.service.ImageCodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * 机构员工层
 *
 * @Author pengq
 * @Date 2018/5/21 17:51
 * @Return
 */
@Slf4j
@RestController
@RequestMapping("/sms")
@Api(tags = "图片验证码接口")
public class ImageCodeController {

    @Resource
    ImageCodeService imageCodeService;


    @ApiOperation(value = "获取图片验证码")
    @GetMapping(value = "getImageCode")
    public void getImageCode(HttpServletResponse response, String key) {

        try (OutputStream os = response.getOutputStream()) {
            byte[] image = imageCodeService.getImage(key);
            response.setContentType("image/png");
            os.write(image);
        } catch (IOException e) {
            log.error("获取验证码失败", e);
        }
    }

    @ApiOperation(value = "验证图片验证码")
    @GetMapping(value = "validateImageCode")
    public Boolean validateImageCode(String key, String imageCode) {
        return imageCodeService.validateImageCode(key, imageCode);
    }

}
