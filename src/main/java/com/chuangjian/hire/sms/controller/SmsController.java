package com.chuangjian.hire.sms.controller;


import com.baomidou.kisso.annotation.Login;
import com.chuangjian.hire.common.controller.BaseController;
import com.chuangjian.hire.common.dto.R;
import com.chuangjian.hire.sms.service.MailService;
import com.chuangjian.hire.sms.service.SmsCodeService;
import com.chuangjian.hire.users.dto.EmailDTO;
import com.chuangjian.hire.users.dto.UserDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.*;

@Validated
@RestController
@RequestMapping("/sms")
@Api(tags = "短信或者邮件相关")
public class SmsController extends BaseController {
    @Resource
    private SmsCodeService smsCodeService;

    @Resource
    MailService mailService;

    @GetMapping("sendSmsCode")
    @ApiOperation(value = "发送短信验证码 1001, \"注册验证码\"  1002, \"忘记密码\" 1003, \"绑定支付宝\" 1004, \"发布需求\"")
    public R sendSmsCode(@RequestParam @NotNull(message = "手机号不能为空") String phone,
                         @RequestParam @NotNull(message = "类型不能为空") Integer type) {
        return R.success(smsCodeService.sendSmsCode(phone, type, new TreeMap()));
    }

    @PostMapping("sendEmailCode")
    @ApiOperation(value = "发送验证邮件")
    public R sendEmailCode(@RequestBody EmailDTO email) {
        return R.success(mailService.sendEmailCode(email));
    }

    @Login
    @PostMapping("verifyEmail")
    @ApiOperation(value = "绑定邮箱")
    public R verifyEmail(@RequestBody EmailDTO email) {
        UserDTO userInfo = getUserInfo();
        email.setUserId(userInfo.getId());
        return R.success(mailService.verifyEmail(email));
    }
}

