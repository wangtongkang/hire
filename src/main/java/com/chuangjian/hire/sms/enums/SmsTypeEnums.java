package com.chuangjian.hire.sms.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 请在添加短信类型的时候需要添加到数据库 sms_template 表中去
 *
 * @author wangtk
 */
@Getter
@AllArgsConstructor
public enum SmsTypeEnums {
    /**
     * 短信模板编号 袋鼠配 1000开始
     */
    REGISTER(1001, "注册验证码", 674546, "您正在申请创舰网注册，验证码为：{1}，3分钟内有效！"),
    FORGET_CODE(1002, "忘记密码", 674471, "您正在创舰网找回密码，您的动态验证码为：{1}，3分钟有效!"),
    BIND_ALI_PAY(1003, "绑定支付宝账号", 674475, "您正在创舰网绑定支付宝账号，您的动态验证码为：{1}，3分钟有效!"),
    CHANGE_PHONE(1005, "修改手机号", 674477, "您正在创舰网修改手机号码，验证码为：{1}，3分钟有效!"),
    SMS_CODE(1006, "创舰网通用短信验证码", 685385, "创建网，验证码为：{1}，3分钟内有效！"),
    ;

    private Integer value;
    private String name;
    private Integer templateId;
    private String content;


    public static SmsTypeEnums getByType(int value) {
        for (SmsTypeEnums userType : values()) {
            if (userType.getValue().equals(value)) {
                return userType;
            }
        }
        return null;
    }
}
