package com.chuangjian.hire.sms.service;


/**
 * 图片验证码接口
 */
public interface ImageCodeService {

    /**
     * 根据key值获取验证码
     *
     * @param imageKey
     * @return
     */
    byte[] getImage(String imageKey);


    /**
     * 验证图片中的验证码
     *
     * @param imageKey  图片key值
     * @param imageCode 图片中的验证码
     * @return
     */
    Boolean validateImageCode(String imageKey, String imageCode);


    /**
     * 只返回是否验证成功
     *
     * @param imageKey
     * @param imageCode
     * @return
     */
    Boolean validateImageCodeNoException(String imageKey, String imageCode);

}
