package com.chuangjian.hire.sms.service;

import com.chuangjian.hire.users.dto.EmailDTO;

public interface MailService {

    String sendEmailCode(EmailDTO emailDTO);

    Boolean verifyEmail(EmailDTO emailDTO);
}
