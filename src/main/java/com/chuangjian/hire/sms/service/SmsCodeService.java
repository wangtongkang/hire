package com.chuangjian.hire.sms.service;

import java.util.Map;

/**
 * 发送验证码短信
 */
public interface SmsCodeService {
    /**
     * 获取短信验证码
     */
    String sendSmsCode(String mobile, Integer smsType, Map map);

    /**
     * 验证短信验证码
     *
     * @param mobile
     * @param msgCode
     * @param smsType
     * @return
     */
    boolean verifySmsCode(String mobile, String msgCode, Integer smsType);

    /**
     * 删除短信验证码
     *
     * @param mobile
     * @param msgType
     * @return
     */
    boolean deleteSmsCode(String mobile, Integer msgType);
}
