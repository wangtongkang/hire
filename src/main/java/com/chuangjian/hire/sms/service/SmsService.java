package com.chuangjian.hire.sms.service;

import java.util.Map;

/**
 * FileName : SmsLogService
 * Description :
 *
 * @author : CodeGenerator
 * @date : 2018/11/16 00:15
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface SmsService {

    /**
     * 发送短信
     *
     * @param param
     * @return
     */
    Boolean sendSMS(String phone, Integer msgType, Map<String,String> param);

}
