package com.chuangjian.hire.sms.service;

public interface SmsStore {

    boolean storeVerifyCode(String mobile, int smsType, String verifyCode, long expireTime);

    String getVerifyCode(String mobile, int smsType);

    Long getVerifyCodeTime(String mobile, int smsType);

    boolean deleteVerifyCode(String mobile, int smsType);
}
