package com.chuangjian.hire.sms.service.impl;

import com.chuangjian.hire.common.exceptionEnum.ApiExceptionEnum;
import com.chuangjian.hire.common.exption.ApiException;
import com.chuangjian.hire.sms.enums.SmsTypeEnums;
import com.chuangjian.hire.sms.service.SmsService;
import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Map;

/**
 * FileName : SmsLogService
 * Description :
 *
 * @author : CodeGenerator
 * @date : 2018/11/16 00:15
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Slf4j
@Transactional
@Service("smsService")
public class SmsServiceImpl implements SmsService {

    @Value("${sms.sdk.appid}")
    private Integer appid;
    @Value("${sms.sdk.key}")
    private String appkey;
    @Value("${sms.sdk.smsSign}")
    private String smsSign;


    @Override
    @SneakyThrows
    public Boolean sendSMS(String phone, Integer msgType, Map<String, String> param) {
        log.info("发送短信开始：{} 类型：{} param: {}", phone, msgType);
        SmsTypeEnums smsTypeEnums = SmsTypeEnums.getByType(msgType);
        if (smsTypeEnums == null) {
            throw new ApiException(ApiExceptionEnum.MSG_TYPE_ERROR);
        }
        //创建发送短信的对象,传入申请的id和key
        SmsSingleSender sender = new SmsSingleSender(appid, appkey);
        SmsSingleSenderResult result = sender.sendWithParam("86", phone, smsTypeEnums.getTemplateId(), new ArrayList<String>(param.values()), smsSign, "", "");  // 签名不能为空串
        //调用第三方发送短信
        log.info("发送短信完成：{}  结果：{}", phone, result);
        return true;
    }


}
