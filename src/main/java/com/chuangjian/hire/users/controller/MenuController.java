package com.chuangjian.hire.users.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chuangjian.hire.common.controller.BaseController;
import com.chuangjian.hire.common.dto.IdsDTO;
import com.chuangjian.hire.common.dto.R;
import com.chuangjian.hire.common.exceptionEnum.ApiExceptionEnum;
import com.chuangjian.hire.common.exption.ApiException;
import com.chuangjian.hire.users.dao.MenuMapper;
import com.chuangjian.hire.users.dao.RoleMenuMapper;
import com.chuangjian.hire.users.domain.MenuDO;
import com.chuangjian.hire.users.domain.RoleMenuDO;
import com.chuangjian.hire.users.service.impl.UsersServiceImpl;
import com.chuangjian.hire.util.StringUtil;
import io.swagger.annotations.Api;
import lombok.extern.java.Log;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log
@Api(tags = "菜单")
@RestController
@RequestMapping("/menu")
public class MenuController extends BaseController {

    @Resource
    MenuMapper menuMapper;
    @Resource
    RoleMenuMapper roleMenuMapper;

    @PostMapping("list")
    public R list() {
        Wrapper q1 = Wrappers.<MenuDO>lambdaQuery();
        List<MenuDO> menuList = menuMapper.selectList(q1);

        return R.success(UsersServiceImpl.treeify(menuList));
    }

    @PostMapping("add")
    public R add(@Validated @RequestBody MenuDO roleDO) {

        Wrapper query = Wrappers.<MenuDO>lambdaQuery()
                .eq(StringUtil.isNotBlank(roleDO.getPath()), MenuDO::getPath, roleDO.getPath())
                .or()
                .eq(StringUtil.isNotBlank(roleDO.getName()), MenuDO::getName, roleDO.getName());
        if (menuMapper.selectCount(query) > 1) {
            throw ApiException.builder().baseExceptionCode(ApiExceptionEnum.EXIST_CODE_ERROR).build();
        }

        return R.success(menuMapper.insert(roleDO));
    }

    @PostMapping("delete")
    public R delete(@Validated @RequestBody MenuDO roleDO) {

        Wrapper query = Wrappers.<RoleMenuDO>lambdaQuery()
                .eq(RoleMenuDO::getMenuId, roleDO.getId());

        if (roleMenuMapper.selectCount(query) > 1) {
            throw ApiException.builder().baseExceptionCode(ApiExceptionEnum.MENU_EXIST_CODE_ERROR).build();
        }

        return R.success(menuMapper.deleteById(roleDO.getId()));
    }


    @PostMapping("listByRoleIds")
    public R listByRoleIds(@Validated @RequestBody IdsDTO idsDTO) {


        Wrapper query = Wrappers.<RoleMenuDO>lambdaQuery()
                .in(RoleMenuDO::getRoleId, idsDTO.getIdList());


        List<RoleMenuDO> list = roleMenuMapper.selectList(query);
        if (CollectionUtil.isEmpty(list)) {
            return R.success(new HashMap<>());
        }


        List<Long> menuIdList = list.stream().map(RoleMenuDO::getMenuId).collect(Collectors.toList());


        Wrapper q1 = Wrappers.<MenuDO>lambdaQuery()
                .in(MenuDO::getId, menuIdList);

        List<MenuDO> menuList = menuMapper.selectList(q1);
        Map<Long, MenuDO> menuMap = menuList.stream().collect(Collectors.toMap(it -> it.getId(), it -> it));


        HashMap<Long, List<MenuDO>> roleIdAndMenuList = list.stream()
                .filter(it -> menuMap.get(it.getMenuId()) != null)
                .collect(
                        Collectors.groupingBy(RoleMenuDO::getRoleId,
                                HashMap::new,
                                Collectors.mapping(it -> menuMap.get(it.getMenuId()), Collectors.toList())
                        ));

        return R.success(roleIdAndMenuList);
    }


}
