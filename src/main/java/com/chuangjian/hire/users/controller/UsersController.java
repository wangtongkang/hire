package com.chuangjian.hire.users.controller;

import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.annotation.Login;
import com.baomidou.kisso.security.token.SSOToken;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chuangjian.hire.common.controller.BaseController;
import com.chuangjian.hire.common.dto.LabelValueDTO;
import com.chuangjian.hire.common.dto.R;
import com.chuangjian.hire.users.dto.*;
import com.chuangjian.hire.users.enums.*;
import com.chuangjian.hire.users.service.UsersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.java.Log;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.stream.Collectors;

@Log
@Api(tags = "用户相关")
@RestController
@RequestMapping("/users")
public class UsersController extends BaseController {

    @Resource
    UsersService usersService;

    @PostMapping("register")
    public R register(@Validated @RequestBody RegisterDTO register) {
        return R.success(usersService.register(register));
    }

    @PostMapping("forget")
    public R forget(@Validated @RequestBody ForgetDTO forget) {
        return R.success(usersService.forget(forget));
    }

    @PostMapping("changePhone")
    @ApiOperation(value = "修改手机号")
    public R changePhone(@Validated @RequestBody ForgetDTO phone) {
        return R.success(usersService.changePhone(phone, getUserInfo()));
    }

    @PostMapping("login")
    public R login(HttpServletRequest request, HttpServletResponse response, @RequestBody @Validated LoginDTO login) {

        // 生成 jwt 票据，访问请求头设置‘ accessToken=票据内容 ’
        UserDTO userDTO = usersService.login(login);
        // Cookie 模式设置
        SSOHelper.setCookie(request, response, new SSOToken().setId(userDTO.getPhone()).setData(userDTO));
        return R.success(userDTO);
    }

    @PostMapping("logout")
    public R logout(HttpServletRequest request, HttpServletResponse response) {
        SSOHelper.clearLogin(request, response);
        return R.success(true);
    }

    @Login
    @GetMapping("info")
    public R getInfo() {
        UserDTO userInfo = getUserInfo();
        return R.success(usersService.getUserById(userInfo.getId()));
    }

    @PostMapping("getUserById")
    @ApiOperation(value = "根据Id获取用户信息")
    public R getUser(@RequestBody UserPayDTO user) {
        return R.success(usersService.getUserPayInfoById(user.getId()));
    }

    @Login
    @GetMapping("uploadIcon")
    @ApiOperation(value = "上传头像")
    public R uploadIcon(String icon) {
        UserDTO userInfo = getUserInfo();
        Boolean result = usersService.uploadIcon(icon, userInfo);
        return R.success(result);
    }


    @Login
    @GetMapping("getWorkStatus")
    @ApiOperation(value = "获取工作状态 (0,不找工作 1,找工作)")
    public R getWorkStatus() {
        UserDTO userInfo = getUserInfo();
        return R.success(usersService.getWorkStatus(userInfo.getId()));
    }

    @Login
    @GetMapping("changeWorkStatus")
    @ApiOperation(value = "修改工作状态 (0,不找工作 1,找工作)")
    public R changeWorkStatus(Integer workStatus) {
        UserDTO userInfo = getUserInfo();
        Boolean result = usersService.changeWorkStatus(workStatus, userInfo);
        return R.success(result);
    }

    @Login
    @PostMapping("changeLoginPassword")
    @ApiOperation(value = "修改登录密码")
    public R changeLoginPassword(@RequestBody @Validated PasswordDTO password) {
        UserDTO userInfo = getUserInfo();
        Boolean result = usersService.changeLoginPassword(password, userInfo);
        return R.success(result);
    }

    @Login
    @PostMapping("changePayPassword")
    @ApiOperation(value = "修改支付密码")
    public R changePayPassword(@RequestBody @Validated PasswordDTO password) {
        UserDTO userInfo = getUserInfo();
        Boolean result = usersService.changePayPassword(password, userInfo);
        return R.success(result);
    }

    @Login
    @PostMapping("bindPayPassword")
    @ApiOperation(value = "绑定支付密码")
    public R bindPayPassword(@RequestBody @Validated BindPasswordDTO password) {
        UserDTO userInfo = getUserInfo();
        Boolean result = usersService.bindPayPassword(password, userInfo);
        return R.success(result);
    }

    @Login
    @PostMapping("bindAliPay")
    @ApiOperation(value = "绑定支付宝")
    public R bindAliPay(@RequestBody @Validated BindAliPayDTO aliPay) {
        UserDTO userInfo = getUserInfo();
        Boolean result = usersService.bindAliPay(aliPay, userInfo);
        return R.success(result);
    }


    @GetMapping("getWorkAgeList")
    @ApiOperation(value = "工作年限列表")
    public R getWorkAgeList() {
        return R.success(
                Arrays.stream(WorkAgeEnum.values()).map(it -> {
                    LabelValueDTO<String, Integer> label = new LabelValueDTO<>();
                    label.setValue(it.getValue());
                    label.setLabel(it.getDesc());
                    return label;
                }).collect(Collectors.toList())
        );
    }

    @GetMapping("getLevelList")
    @ApiOperation(value = "获取设计师级别")
    public R getLevelList() {
        return R.success(
                Arrays.stream(LevelEnum.values()).map(it -> {
                    LabelValueDTO<String, Integer> label = new LabelValueDTO<>();
                    label.setValue(it.getValue());
                    label.setLabel(it.getDesc());
                    return label;
                }).collect(Collectors.toList())
        );
    }

    @GetMapping("getEducationList")
    @ApiOperation(value = "学历列表")
    public R getEducationList() {

        return R.success(
                Arrays.stream(EducationEnum.values()).map(it -> {
                    LabelValueDTO<String, Integer> label = new LabelValueDTO<>();
                    label.setValue(it.getValue());
                    label.setLabel(it.getDesc());
                    return label;
                }).collect(Collectors.toList())
        );
    }

    @GetMapping("getPlatformList")
    @ApiOperation(value = "平台列表")
    public R getPlatformList() {

        return R.success(
                Arrays.stream(PlatformEnum.values()).map(it -> {
                    LabelValueDTO<String, Integer> label = new LabelValueDTO<>();
                    label.setValue(it.getValue());
                    label.setLabel(it.getDesc());
                    return label;
                }).collect(Collectors.toList())
        );
    }

    @GetMapping("getOrgTypeList")
    @ApiOperation(value = "机构类型")
    public R getOrgTypeList() {
        return R.success(
                Arrays.stream(OrgTypeEnum.values()).map(it -> {
                    LabelValueDTO<String, Integer> label = new LabelValueDTO<>();
                    label.setValue(it.getValue());
                    label.setLabel(it.getDesc());
                    return label;
                }).collect(Collectors.toList())
        );
    }

    @GetMapping("getSalaryList")
    @ApiOperation(value = "薪资列表")
    public R getSalaryList() {

        return R.success(
                Arrays.stream(SalaryEnum.values()).map(it -> {
                    LabelValueDTO<String, Integer> label = new LabelValueDTO<>();
                    label.setValue(it.getValue());
                    label.setLabel(it.getDesc());
                    return label;
                }).collect(Collectors.toList())
        );
    }

    @Login
    @PostMapping("getSalary")
    @ApiOperation(value = "获取 月薪或者定制")
    public R getSalary() {
        UserDTO userInfo = getUserInfo();
        return R.success(usersService.getSalary(userInfo.getId()));
    }

    @Login
    @PostMapping("editSalary")
    @ApiOperation(value = "编辑月薪或者定制")
    public R editSalary(@Validated @RequestBody SalaryDTO salary) {
        UserDTO userInfo = getUserInfo();
        Boolean result = usersService.editSalary(userInfo.getId(), salary);
        return R.success(result);
    }

    @Login
    @PostMapping("getEditInfo")
    @ApiOperation(value = "获取个人信息")
    public R getEditInfo() {
        UserDTO userInfo = getUserInfo();
        return R.success(usersService.getEditInfo(userInfo.getId()));
    }


    @Login
    @PostMapping("editInfo")
    @ApiOperation(value = "编辑个人信息")
    public R editInfo(@Validated @RequestBody EditInfoDTO forget) {
        UserDTO userInfo = getUserInfo();
        Boolean result = usersService.editInfo(forget, userInfo);
        return R.success(result);
    }

    @Login
    @PostMapping("getOrgInfo")
    @ApiOperation(value = "获取企业信息")
    public R getOrgInfo() {
        UserDTO userInfo = getUserInfo();
        return R.success(usersService.getOrgInfo(userInfo.getId()));
    }

    @Login
    @PostMapping("editOrgInfo")
    @ApiOperation(value = "编辑企业信息")
    public R editOrgInfo(@Validated @RequestBody OrgDTO info) {
        UserDTO userInfo = getUserInfo();
        Boolean result = usersService.editOrgInfo(info, userInfo);
        return R.success(result);
    }

    @Login
    @PostMapping("getContact")
    @ApiOperation(value = "获取联系信息")
    public R getContact() {
        UserDTO userInfo = getUserInfo();
        return R.success(usersService.getContact(userInfo.getId()));
    }

    @Login
    @PostMapping("getContactByUserId")
    @ApiOperation(value = "获取根据用户Id联系信息")
    public R getContactByUserId(@RequestParam Long userId) {
        return R.success(usersService.getContact(userId));
    }

    @Login
    @PostMapping("editContact")
    @ApiOperation(value = "编辑联系人")
    public R editContact(@Validated @RequestBody ContactDTO contact) {
        UserDTO userInfo = getUserInfo();
        Boolean result = usersService.editContact(contact, userInfo);
        return R.success(result);
    }

    @Login
    @PostMapping("getIntro")
    @ApiOperation(value = "获取个人介绍和自定义简历")
    public R getIntro() {
        UserDTO userInfo = getUserInfo();
        return R.success(usersService.getIntro(userInfo.getId()));
    }

    @Login
    @PostMapping("editIntro")
    @ApiOperation(value = "编辑个人介绍或者自定义简历")
    public R editIntro(@Validated @RequestBody IntroDTO intro) {
        UserDTO userInfo = getUserInfo();
        Boolean result = usersService.editIntro(userInfo.getId(), intro);
        return R.success(result);
    }


    @PostMapping("searchUsers")
    @ApiOperation(value = "搜索用户")
    public R searchUsers(Page page, @RequestBody SearchUsersDTO search) {
        return R.success(usersService.searchUsers(page, search));
    }

    @PostMapping("getUserDetailById")
    @ApiOperation(value = "获取用户详情")
    public R searchUsers(@RequestBody UsersDTO user) {
        return R.success(usersService.getUserDetailById(user.getId()));
    }


    @Login
    @PostMapping("pageList")
    @ApiOperation(value = "用户列表", tags = {
            "后台"
    })
    public R pageList(Page page, @RequestBody UsersPOParam params) {
        return R.success(usersService.pageList(page, params));
    }


    @PostMapping("getUserInfoByPlatform")
    @ApiOperation(value = "获取用户详情", tags = {
            "后台"
    })
    public R getUserInfoByPlatform(@RequestBody UsersPO user) {
        return R.success(usersService.getUserInfoByPlatform(user.getId()));
    }

    @Login
    @PostMapping("addOrUpdateUserPO")
    @ApiOperation(value = "添加或者修改用户", tags = {
            "后台"
    })
    public R addOrUpdateUserPO(@RequestBody UsersPO users) {
        return R.success(usersService.addOrUpdateUserPO(users));
    }


    @Login
    @PostMapping("disableUser")
    @ApiOperation(value = "禁用用户", tags = {
            "后台"
    })
    public R disableUser(@RequestBody UsersPO users) {
        return R.success(usersService.disableUser(users));
    }

    @Login
    @PostMapping("changePassword")
    @ApiOperation(value = "修改密码", tags = {
            "后台"
    })
    public R changePassword(@RequestBody PasswordParam users) {
        return R.success(usersService.changePassword(users));
    }
}
