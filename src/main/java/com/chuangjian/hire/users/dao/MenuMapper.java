package com.chuangjian.hire.users.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.users.domain.MenuDO;


/**
* FileName: UsersShopBusiness
* Description:
* @author: CodeGenerator
* @date: 2020/07/16 10:22
* Copyright (C) 杭州同基汽车科技有限公司
*/
public interface MenuMapper extends BaseMapper<MenuDO> {

}
