package com.chuangjian.hire.users.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.users.domain.RoleDO;


/**
* FileName: UsersBalanceLogBusiness
* Description:
* @author: CodeGenerator
* @date: 2020/07/13 22:58
* Copyright (C) 杭州同基汽车科技有限公司
*/
public interface RoleMapper extends BaseMapper<RoleDO> {

}
