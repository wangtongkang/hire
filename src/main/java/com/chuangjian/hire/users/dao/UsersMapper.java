package com.chuangjian.hire.users.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chuangjian.hire.users.domain.MenuDO;
import com.chuangjian.hire.users.domain.UsersDO;
import com.github.flyingglass.mybatis.cache.MybatisRedisCache;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Property;

import java.util.List;


/**
 * FileName: UsersBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/09 19:08
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@CacheNamespace(
        implementation = MybatisRedisCache.class,
        properties = {@Property(
                name = "flushInterval",
                value = "50000"
        )}
)
public interface UsersMapper extends BaseMapper<UsersDO> {

    List<MenuDO> selectMenList(@Param("userId") Long userId);

    List<String> getRoleNameList(@Param("userId") Long userId);
}
