package com.chuangjian.hire.users.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.chuangjian.hire.common.base.BaseDO;
import com.gitee.sunchenbin.mybatis.actable.annotation.*;
import lombok.Data;

import java.io.Serializable;

/**
 * FileName: UsersBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/16 13:53
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
@TableName("u_users")
@Table(name = "u_users")
public class UsersDO extends BaseDO<UsersDO> {

    /**
     * 类目Id
     */
    @IsKey                         //actable主键注解
    @IsAutoIncrement             //自增
    @Column
    @TableId(type = IdType.AUTO)
    private java.lang.Long id;

    /**
     * 账号
     */
    @Column
    private java.lang.String account;
    /**
     * name
     */
    @Column
    private java.lang.String name;
    /**
     * 手机号
     */
    @Column
    private java.lang.String phone;
    /**
     * email
     */
    @Column
    private java.lang.String email;
    /**
     * 登录密码
     */
    @Column
    private java.lang.String loginPwd;

    /**
     * 员工状态   0,禁用 1,启用
     */
    @Column(defaultValue = "1")
    private java.lang.Integer status;

    /**
     * 是否有效，0.删除    1.有效
     */
    @TableLogic
    @Column(defaultValue = "1")
    private java.lang.Boolean valid;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
