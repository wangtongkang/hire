package com.chuangjian.hire.users.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.chuangjian.hire.common.base.BaseDO;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsAutoIncrement;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsKey;
import com.gitee.sunchenbin.mybatis.actable.annotation.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * FileName: UsersBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/16 13:53
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
@TableName("u_users_role")
@Table(name = "u_users_role")
public class UsersRoleDO extends BaseDO<UsersRoleDO> {

    /**
     * 类目Id
     */
    @IsKey                         //actable主键注解
    @IsAutoIncrement             //自增
    @Column
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 真实姓名
     */
    @Column
    private Long roleId;

    /**
     * 描述
     */
    @Column
    private Long usersId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
