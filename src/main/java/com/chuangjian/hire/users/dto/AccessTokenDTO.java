package com.chuangjian.hire.users.dto;


import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class AccessTokenDTO {

    /**
     * access_token
     */
    @JSONField(name = "access_token")
    private String accessToken;

    /**
     * 秒
     */
    @JSONField(name = "expires_in")
    private Integer expiresIn;

    @JSONField(name = "refresh_token")
    private String refreshToken;

    private String openid;

    private String scope;

}
