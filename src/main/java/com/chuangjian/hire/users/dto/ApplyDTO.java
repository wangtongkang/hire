package com.chuangjian.hire.users.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("申请提现")
public class ApplyDTO {

    @ApiModelProperty("申请提现ID")
    private java.lang.Long id;

    @ApiModelProperty("申请金额")
    @NotNull(message = "申请金额不能为空")
    private java.math.BigDecimal amount;

    @ApiModelProperty("支付密码")
    @NotNull(message = "支付密码 不能为空")
    private java.lang.String payPwd;

    @ApiModelProperty("支付宝账号")
    private java.lang.String aliPayAccount;

    @ApiModelProperty("支付宝真实姓名")
    private java.lang.String aliPayName;

    @ApiModelProperty("状态 1,申请中 2,申请成功 3,申请失败")
    private java.lang.Integer status;
    @ApiModelProperty("申请时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date createDate;
}
