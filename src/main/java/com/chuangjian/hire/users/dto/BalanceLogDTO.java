package com.chuangjian.hire.users.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("交易记录")
public class BalanceLogDTO {

    @ApiModelProperty("账户进出类型 1,入账,2,出账")
    private java.lang.Integer type;
    @ApiModelProperty("账户进出类型 1,充值 2,雇佣余额付款,3,提现 4,发工资")
    private java.lang.Integer subType;
    @ApiModelProperty("金额")
    private java.math.BigDecimal amount;
    @ApiModelProperty("时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date createDate;
}
