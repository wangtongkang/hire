package com.chuangjian.hire.users.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
public class BindAliPayDTO {
    @NotEmpty(message = "支付宝账号不能为空")
    private java.lang.String aliPayAccount;
    @NotEmpty(message = "支付宝真实姓名不能为空")
    private java.lang.String aliPayName;
    @NotEmpty(message = "短信不能为空")
    private String msgCode;
}
