package com.chuangjian.hire.users.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
public class BindPasswordDTO {
    @NotEmpty(message = "短信不能为空")
    private String msgCode;
    @NotEmpty(message = "新密码")
    private String password;
}
