package com.chuangjian.hire.users.dto;

import lombok.Data;

@Data
public class ContactDTO {
    /**
     * QQ
     */
    private java.lang.String qq;
    /**
     * 微信二维码URL
     */
    private java.lang.String wechat;

    private String phone;
}
