package com.chuangjian.hire.users.dto;

import lombok.Data;

@Data
public class EditInfoDTO {
    /**
     * email
     */
    private java.lang.String email;
    /**
     * 支付宝账号
     */
    private java.lang.String aliPayAccount;
    /**
     * 头像
     */
    private java.lang.String icon;
    /**
     * 姓名
     */
    private java.lang.String nickName;
    /**
     * 姓名
     */
    private java.lang.String realName;
    /**
     * 民族
     */
    private java.lang.String nation;
    /**
     * 年龄
     */
    private java.lang.Integer age;
    /**
     * 性别 1,男 2,女
     */
    private java.lang.Integer sex;
    /**
     * 学历
     */
    private java.lang.Integer education;
    /**
     * 毕业学校
     */
    private java.lang.String graduate;
    /**
     * 工作年限
     */
    private java.lang.Integer workAge;
    /**
     * 省份id
     */
    private java.lang.Integer provinceId;
    /**
     * 省名
     */
    private java.lang.String province;
    /**
     * 市id
     */
    private java.lang.Integer cityId;
    /**
     * 市名
     */
    private java.lang.String city;
    /**
     * 区县id
     */
    private java.lang.Integer districtId;
    /**
     * 区县名
     */
    private java.lang.String district;
    /**
     * 详细地址
     */
    private java.lang.String detailAddress;
}
