package com.chuangjian.hire.users.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("email")
public class EmailDTO {

    @ApiModelProperty("userId")
    @NotNull(message = "用户Id 不能为空")
    private java.lang.Long userId;

    @ApiModelProperty("email")
    @NotNull(message = "email 不能为空")
    private java.lang.String email;

    @ApiModelProperty("邮箱验证码")
    @NotNull(message = "邮箱验证码 不能为空")
    private java.lang.String code;
}
