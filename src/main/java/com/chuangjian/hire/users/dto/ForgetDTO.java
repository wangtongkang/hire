package com.chuangjian.hire.users.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
public class ForgetDTO {
    @NotEmpty(message = "手机号不能为空")
    private String phone;

    @NotEmpty(message = "短信不能为空")
    private String msgCode;

    @NotEmpty(message = "密码不能为空")
    private String password;
}
