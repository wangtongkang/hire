package com.chuangjian.hire.users.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("介绍")
public class IntroDTO {

    @ApiModelProperty("个人介绍")
    private String intro;

    @ApiModelProperty("自定义简历")
    private String resumeUrl;
}
