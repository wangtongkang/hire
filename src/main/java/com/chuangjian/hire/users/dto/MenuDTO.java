package com.chuangjian.hire.users.dto;

import com.chuangjian.hire.users.domain.MenuDO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("菜单")
public class MenuDTO extends MenuDO {

    /**
     * 子菜单
     */
    private List<MenuDTO> children;

}
