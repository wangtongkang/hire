package com.chuangjian.hire.users.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("企业信息")
public class OrgDTO {

    @ApiModelProperty("用户id")
    private Long id;
    @ApiModelProperty("企业名称")
    private java.lang.String orgName;
    @ApiModelProperty("企业行业")
    private java.lang.String orgType;
    @ApiModelProperty("企业行业Id")
    private java.lang.Long orgTypeId;
    @ApiModelProperty("企业label 逗号隔开")
    private java.lang.String orgLabel;
    @ApiModelProperty("企业介绍")
    private java.lang.String orgIntro;
    @ApiModelProperty("省份id")
    private java.lang.Integer orgProvinceId;
    @ApiModelProperty("省名")
    private java.lang.String orgProvince;
    @ApiModelProperty("市id")
    private java.lang.Integer orgCityId;
    @ApiModelProperty("市名")
    private java.lang.String orgCity;
    @ApiModelProperty("区县id")
    private java.lang.Integer orgDistrictId;
    @ApiModelProperty("区县名")
    private java.lang.String orgDistrict;
    @ApiModelProperty("详细地址")
    private java.lang.String orgDetailAddress;
}
