package com.chuangjian.hire.users.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
public class PasswordDTO {
    @NotEmpty(message = "旧密码")
    private String oldPassword;
    @NotEmpty(message = "新密码")
    private String password;
}
