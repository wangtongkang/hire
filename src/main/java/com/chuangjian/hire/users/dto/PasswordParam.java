package com.chuangjian.hire.users.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
public class PasswordParam {
    @NotEmpty(message = "用户Id")
    private Long userId;
    @NotEmpty(message = "新密码")
    private String password;
}
