package com.chuangjian.hire.users.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class RegisterDTO {
    @NotEmpty(message = "手机号不能为空")
    private String phone;
    @NotEmpty(message = "密码不能为空")
    private String password;
    @NotEmpty(message = "短信不能为空")
    private String msgCode;
    @NotNull(message = "用户类型不能为空")
    private Integer type;

}
