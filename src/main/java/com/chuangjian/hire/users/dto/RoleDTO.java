package com.chuangjian.hire.users.dto;

import com.chuangjian.hire.common.dto.LabelValueDTO;
import com.chuangjian.hire.users.domain.RoleDO;
import lombok.Data;

import java.util.List;

/**
 * FileName: UsersBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/16 13:53
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
public class RoleDTO extends RoleDO {


    /**
     * 角色列表
     */

    private List<LabelValueDTO<String, Long>> menuList;

}
