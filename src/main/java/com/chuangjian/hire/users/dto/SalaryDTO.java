package com.chuangjian.hire.users.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("薪资")
public class SalaryDTO {
    @ApiModelProperty("月薪(每月)")
    private java.math.BigDecimal payMonth;
    @ApiModelProperty("定制要求(每天)")
    private java.math.BigDecimal payCustom;
}
