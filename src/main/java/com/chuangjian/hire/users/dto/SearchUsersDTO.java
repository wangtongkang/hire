package com.chuangjian.hire.users.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("搜索内容")
public class SearchUsersDTO {

    @ApiModelProperty("搜索内容")
    private String content;

    @ApiModelProperty("擅长类目")
    private Long catalogId;

    @ApiModelProperty("设计师级别")
    private Integer level;

    @ApiModelProperty("性别")
    private java.lang.Integer sex;
}
