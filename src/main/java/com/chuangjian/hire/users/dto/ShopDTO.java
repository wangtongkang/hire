package com.chuangjian.hire.users.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("店铺")
public class ShopDTO {

    @ApiModelProperty("店铺Id")

    private java.lang.Long id;

    @ApiModelProperty("用户Id")
    private java.lang.Long userId;

    @ApiModelProperty("店铺名称")
    @NotNull(message = "店铺名称 不能为空")
    private java.lang.String name;

    @ApiModelProperty("店铺类目")
    @NotNull(message = "店铺类目 不能为空")
    private java.lang.Long catalogId;

    @ApiModelProperty("店铺类目")
    @NotNull(message = "店铺类目 不能为空")
    private java.lang.String catalog;

    @ApiModelProperty("所属平台")
    @NotNull(message = "所属平台 不能为空")
    private java.lang.Long platformId;

    @ApiModelProperty("所属平台")
    @NotNull(message = "所属平台 不能为空")
    private java.lang.String platform;

    @ApiModelProperty("店铺url")
    @NotNull(message = "店铺url 不能为空")
    private java.lang.String url;
}
