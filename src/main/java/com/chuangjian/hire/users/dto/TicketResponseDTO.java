package com.chuangjian.hire.users.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class TicketResponseDTO {

    @JSONField(name = "errcode")
    private Integer errCode;

    @JSONField(name = "errmsg")
    private String errMsg;

    /**
     * access_token
     */
    private String ticket;

    /**
     * 秒
     */
    @JSONField(name = "expires_in")
    private Integer expiresIn;
}
