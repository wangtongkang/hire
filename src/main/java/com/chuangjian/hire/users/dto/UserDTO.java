package com.chuangjian.hire.users.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("用户")
public class UserDTO {
    @ApiModelProperty("用户id")
    private Long id;
    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("名称")
    private String name;
    @ApiModelProperty("昵称")
    private List<MenuDTO> menus;
}
