package com.chuangjian.hire.users.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("用户")
public class UserPayDTO {
    @ApiModelProperty("用户id")
    private Long id;
    @ApiModelProperty("icon")
    private String icon;
    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("月薪(每月)")
    private java.math.BigDecimal payMonth;
    @ApiModelProperty("定制要求(每天)")
    private java.math.BigDecimal payCustom;

}
