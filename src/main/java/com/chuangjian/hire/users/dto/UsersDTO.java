package com.chuangjian.hire.users.dto;

import com.chuangjian.hire.catalog.dto.UsersCatalogDTO;
import com.chuangjian.hire.product.dto.ProductDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel("用户")
public class UsersDTO {
    @ApiModelProperty("用户id")
    private Long id;
    @ApiModelProperty("icon")
    private java.lang.String icon;
    @ApiModelProperty("昵称")
    private String nickName;
    @ApiModelProperty("个人介绍")
    private String intro;
    @ApiModelProperty("月薪(每月)")
    private BigDecimal payMonth;

    @ApiModelProperty("年龄")
    private java.lang.Integer age;
    @ApiModelProperty("性别 1,男 2,女")
    private java.lang.Integer sex;
    @ApiModelProperty("工作年限")
    private java.lang.Integer workAge;
    @ApiModelProperty("毕业学校")
    private java.lang.String graduate;
    @ApiModelProperty("学历")
    private java.lang.Integer education;
    @ApiModelProperty("学历")
    private java.lang.String educations;

    @ApiModelProperty("省名")
    private java.lang.String province;
    @ApiModelProperty("城市")
    private java.lang.String city;

    @ApiModelProperty("自定义简历URL")
    private java.lang.String resumeUrl;

    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("QQ")
    private java.lang.String qq;
    @ApiModelProperty("微信二维码URL")
    private java.lang.String wechat;

    @ApiModelProperty("用户作品列表")
    List<ProductDTO> productList;

    @ApiModelProperty("用户标签")
    List<String> usersLabelList;

    @ApiModelProperty("用户擅长类目")
    List<UsersCatalogDTO> usersCatalogList;
}
