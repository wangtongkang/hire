package com.chuangjian.hire.users.dto;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.chuangjian.hire.common.dto.LabelValueDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * FileName: UsersBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/16 13:53
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
public class UsersPO {

    private java.lang.Long id;

    /**
     * 账号
     */
    private java.lang.String account;
    /**
     * 登录密码
     */
    private java.lang.String loginPwd;
    /**
     * name
     */
    private java.lang.String name;
    /**
     * 手机号
     */
    private java.lang.String phone;
    /**
     * email
     */
    private java.lang.String email;
    /**
     * 角色列表
     */

    private List<LabelValueDTO<String, Long>> roles;

    /**
     * 员工状态   0,禁用 1,启用
     */
    private java.lang.Integer status;

    /**
     * 创建人
     */
    @TableField(value = "creator", fill = FieldFill.INSERT)
    private java.lang.String creator;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private java.util.Date createDate;

}
