package com.chuangjian.hire.users.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * FileName: UsersBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/16 13:53
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Data
public class UsersPOParam {

    @ApiModelProperty("昵称")
    private String name;

    @ApiModelProperty("手机号")
    private String phone;
}
