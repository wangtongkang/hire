package com.chuangjian.hire.users.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class initJSSDKDataDTO {

    private String appId;

    private String timestamp;

    private String signature;

    private String nonceStr;
}
