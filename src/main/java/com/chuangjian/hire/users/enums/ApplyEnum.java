package com.chuangjian.hire.users.enums;

/**
 * 状态 1,申请中 2,申请成功 3,申请失败
 */
public enum ApplyEnum {


    APPLYING(1, "申请中"),
    SUCCESS(2, "申请成功"),
    FAIL(3, "申请失败"),
    ;

    private Integer value;
    private String desc;

    private ApplyEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
