package com.chuangjian.hire.users.enums;

/**
 * 1,入账,2,出账
 */
public enum BalanceEnum {


    IN_COME(1, "入账"),
    OUT_COM(2, "出账"),
    ;

    private Integer value;
    private String desc;

    private BalanceEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
