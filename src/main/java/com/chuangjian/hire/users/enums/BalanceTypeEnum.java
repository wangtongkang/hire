package com.chuangjian.hire.users.enums;

/**
 * 1,充值 2,雇佣余额付款 3,提现 4,提现失败退款 5,发工资
 */
public enum BalanceTypeEnum {


    CHARGE(1, "充值"),
    HIRE_PAY(2, "雇佣余额 付款"),
    APPLY(3, "提现"),
    APPLY_FAIL(4, "提现失败退款"),
    PAY(5, "发工资"),
    ;

    private Integer value;
    private String desc;

    private BalanceTypeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
