package com.chuangjian.hire.users.enums;

public enum EducationEnum {

    LEVEL_1(1, "小学及以下"),
    LEVEL_2(2, "初中"),
    LEVEL_3(3, "高中"),
    LEVEL_4(4, "大专"),
    LEVEL_5(5, "本科"),
    LEVEL_6(6, "研究生"),
    LEVEL_7(7, "博士及以上"),
    ;

    private Integer value;
    private String desc;

    private EducationEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static String getLabels(Integer value) {
        if (value == null) {
            return null;
        }
        for (EducationEnum typeEnum : values()) {
            if (typeEnum.value.equals(value)) {
                return typeEnum.getDesc();
            }
        }
        return null;
    }


    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
