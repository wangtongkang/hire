package com.chuangjian.hire.users.enums;

public enum LevelEnum {
    //助理
    //初级
    //中级
    //高级
    //专家
    ASSIST(10, "助理级别"),
    JUNIOR(20, "初级级别"),
    MIDDLE(30, "中级级别"),
    SENIOR(40, "高级级别"),
    EXPERT(50, "专家级别");

    private Integer value;
    private String desc;

    private LevelEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static LevelEnum getType(Integer value) {
        if (value == null) {
            return null;
        }
        for (LevelEnum typeEnum : values()) {
            if (typeEnum.value.equals(value)) {
                return typeEnum;
            }
        }
        return null;
    }


    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
