package com.chuangjian.hire.users.enums;

public enum OrgTypeEnum {
    LEVEL_1(10, "农业领域"),
    LEVEL_2(20, "汽车领域"),
    LEVEL_3(30, "商业服务"),
    LEVEL_4(40, "服装纺织"),
    LEVEL_5(50, "其他行业"),
    ;

    private Integer value;
    private String desc;

    private OrgTypeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
