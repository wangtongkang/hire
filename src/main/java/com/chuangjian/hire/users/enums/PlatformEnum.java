package com.chuangjian.hire.users.enums;

public enum PlatformEnum {

    LEVEL_1(10, "淘宝"),
    LEVEL_2(20, "天猫"),
    LEVEL_3(30, "京东"),
    LEVEL_4(40, "唯品会"),
    LEVEL_5(50, "苏宁"),
    LEVEL_6(60, "一号店"),
    LEVEL_7(90, "其他"),
    ;

    private Integer value;
    private String desc;

    private PlatformEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
