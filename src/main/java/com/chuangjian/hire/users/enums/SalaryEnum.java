package com.chuangjian.hire.users.enums;

public enum SalaryEnum {

    LEVEL_1(10, "1500~3000"),
    LEVEL_2(20, "3000~5000"),
    LEVEL_3(30, "5000~7000"),
    LEVEL_4(40, "7000~10000"),
    LEVEL_5(50, "10000~15000"),
    LEVEL_6(60, "15000以上"),
    LEVEL_7(70, "其他"),
    ;

    private Integer value;
    private String desc;

    private SalaryEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
