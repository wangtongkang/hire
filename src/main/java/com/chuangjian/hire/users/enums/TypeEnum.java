package com.chuangjian.hire.users.enums;

public enum TypeEnum {
    //(1,雇主 2,设计师,3,后台用户)
    HIRE(1, "雇主"),
    DESIGNER(2, "设计师"),
    PLATFORM(3, "后台用户");

    private Integer value;
    private String desc;

    private TypeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static TypeEnum getType(Integer value) {
        if (value == null) {
            return null;
        }
        for (TypeEnum typeEnum : values()) {
            if (typeEnum.value.equals(value)) {
                return typeEnum;
            }
        }
        return null;
    }


    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
