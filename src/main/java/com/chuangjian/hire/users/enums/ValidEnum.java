package com.chuangjian.hire.users.enums;

public enum ValidEnum {
    valid(1, "有"),
    invalid(0, "无");

    private Integer value;
    private String desc;

    private ValidEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }


    public static ValidEnum getType(Integer value) {
        if (value == null) {
            return null;
        }
        for (ValidEnum typeEnum : values()) {
            if (typeEnum.value.equals(value)) {
                return typeEnum;
            }
        }
        return null;
    }

    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
