package com.chuangjian.hire.users.enums;

public enum WorkAgeEnum {
    //助理
    //初级
    //中级
    //高级
    //专家
    AGE_1(10, "0-0.5年"),
    AGE_2(20, "0.5-1.5年"),
    AGE_3(30, "1.5-2.5年"),
    AGE_4(40, "2.5-5年"),
    AGE_5(50, "5以上年"),
    ;

    private Integer value;
    private String desc;

    private WorkAgeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
