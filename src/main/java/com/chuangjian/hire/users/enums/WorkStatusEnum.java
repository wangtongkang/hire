package com.chuangjian.hire.users.enums;

public enum WorkStatusEnum {
    /**
     * (0,不找工作 1,找工作)
     */
    valid(0, "不找工作"),
    invalid(1, "找工作");

    private Integer value;
    private String desc;

    private WorkStatusEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
