package com.chuangjian.hire.users.service;


import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName: CacheService
 * @Description:
 * @Author zhujunjie
 * @Date 2018/11/03 14:47
 * @Copyright (C) 杭州典击科技有限公司
 */
public interface CacheService {

    /**
     * @param key
     * @param value
     * @return
     * @desc 添加緩存
     * @author zhujunjie
     * @date 2018/11/3 15:07
     */
    void setCache(String key, String value);

    /**
     * @param key
     * @param object
     * @return
     * @desc 添加緩存
     * @author zhujunjie
     * @date 2018/11/3 15:07
     */
    void setCache(String key, Object object);

    /**
     * @param key
     * @return
     * @desc 获取缓存
     * @author zhujunjie
     * @date 2018/11/3 15:07
     */
    String getCache(String key);

    /**
     * @param key
     * @param value
     * @param time
     * @param unit
     * @return
     * @desc 添加有时效的缓存
     * @author zhujunjie
     * @date 2018/11/3 15:10
     */
    void setCacheForTimeOut(String key, String value, Long time, TimeUnit unit);

    /**
     * @param key
     * @param object
     * @param time
     * @param unit
     * @return
     * @desc 添加有时效的缓存
     * @author zhujunjie
     * @date 2018/11/3 15:10
     */
    void setCacheForTimeOut(String key, Object object, Long time, TimeUnit unit);

    /**
     * @param key
     * @return
     * @desc 删除缓存
     * @author zhujunjie
     * @date 2018/11/3 15:13
     */
    Boolean deleteCache(String key);

    /**
     * desc 模糊查询缓存
     *
     * author zhujunjie
     * date 2018-12-22 14:16
     * @param key
     * @return
     */
    Set<String> getCacheListByKeys(String key);

    /**
     * desc 获取缓存，返回数字
     *
     * author zhujunjie
     * date 2018-12-22 14:54
     * @param key
     * @return
     */
    Integer getCacheForInt(String key);
}
