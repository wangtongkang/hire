package com.chuangjian.hire.users.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chuangjian.hire.common.dto.LabelValueDTO;
import com.chuangjian.hire.product.dto.ProductUserDTO;
import com.chuangjian.hire.users.domain.UsersDO;
import com.chuangjian.hire.users.dto.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * FileName: UsersBusiness
 * Description:
 *
 * @author: CodeGenerator
 * @date: 2020/07/09 19:08
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public interface UsersService {


    UsersDO findById(Long userID);

    Boolean register(RegisterDTO register);


    UserDTO login(LoginDTO login);

    UserDTO getUserById(Long userId);

    UserPayDTO getUserPayInfoById(Long userId);


    Boolean forget(ForgetDTO forget);

    Boolean changePhone(ForgetDTO forget, UserDTO userInfo);

    Boolean changeLoginPassword(PasswordDTO password, UserDTO userInfo);


    Boolean changePayPassword(PasswordDTO password, UserDTO userInfo);


    Boolean bindPayPassword(BindPasswordDTO password, UserDTO userInfo);

    Boolean bindAliPay(BindAliPayDTO aliPay, UserDTO userInfo);


    Boolean bindEmail(EmailDTO emailDTO);

    Boolean uploadIcon(String icon, UserDTO userInfo);

    Integer getWorkStatus(Long userId);

    Boolean changeWorkStatus(Integer workStatus, UserDTO userInfo);

    SalaryDTO getSalary(Long userId);

    Boolean editSalary(Long userId, SalaryDTO salary);

    OrgDTO getOrgInfo(Long userId);

    Boolean editOrgInfo(OrgDTO editInfo, UserDTO userInfo);

    EditInfoDTO getEditInfo(Long userId);

    Boolean editInfo(EditInfoDTO editInfo, UserDTO userInfo);

    ContactDTO getContact(Long userId);

    Boolean editContact(ContactDTO contact, UserDTO userInfo);

    IntroDTO getIntro(Long userId);

    Boolean editIntro(Long userId, IntroDTO intro);


    Boolean reduceBalance(BigDecimal amount, Long userId);

    Boolean reduceFreezeBalance(BigDecimal amount, Long userId);

    Boolean addBalance(BigDecimal amount, Long userId);

    Boolean addFreezeBalance(BigDecimal amount, Long userId);


    Boolean confirmPayPwd(Long userId, String payPwd);


    List<ProductUserDTO> getUserByIds(List<Long> userIdList);

    UsersDTO getUserDetailById(Long userId);

    ProductUserDTO getProductUserById(Long userId);

    Page<UsersDTO> searchUsers(Page page, SearchUsersDTO search);

    Page<UsersDTO> getRecommendUsers(Page page);

    Page<UsersDTO> getLatestUsers(Page page);


    Page<UsersPO> pageList(Page page, UsersPOParam search);

    UsersPO getUserInfoByPlatform(Long userId);


    Boolean addOrUpdateUserPO(UsersPO usersPO);

    Boolean changePassword(PasswordParam usersPO);

    List<LabelValueDTO> getEmployerList();

    List<LabelValueDTO> getEmployeeList();


    Boolean disableUser(UsersPO users);
}
