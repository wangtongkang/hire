package com.chuangjian.hire.util;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * ClassName: OrderNumUtils
 * Description:
 *
 * @author hh
 * @date 2018/09/11 13:50
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public class OrderNumUtils {

    /**
     * 订单号
     *
     * @return
     */
    public static String getOrderNum() {
        String title = "DD";
        Long time = System.currentTimeMillis();
        String random = RandomStringUtils.random(2, false, true);
        return title + time + random;
    }

    public static String getPayNum() {
        String title = "ZD";
        Long time = System.currentTimeMillis();
        String random = RandomStringUtils.random(2, false, true);
        return title + time + random;
    }

    /**
     * 提现单号
     *
     * @return
     */
    public static String getWithdrawNo() {
        String title = "TX";
        Long time = System.currentTimeMillis();
        String random = RandomStringUtils.random(2, false, true);
        return title + time + random;
    }

}
