package com.chuangjian.hire.util;


import cn.hutool.core.collection.CollectionUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;


public class StringUtil extends StringUtils {


    public static List<String> getList(String labels) {
        if (StringUtil.isNotBlank(labels)) {
            return new ArrayList(Arrays.asList(labels.split(",")));
        }
        return new ArrayList<>();
    }

    public static String getDBString(List<String> labelList) {
        if (CollectionUtil.isEmpty(labelList)) {
            return null;
        }
        return String.join(",", labelList);
    }

    /**
     * 生成随机验证码【数字】
     */
    public static String getRandomCodeA(Integer count) {
        String code = "";
        char[] codes = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        Random r = new Random();
        for (int i = 0; i < count; i++) {
            code += codes[r.nextInt(codes.length)];
        }
        return code;
    }

    /**
     * 功能:生成随机码【英文+数字】
     *
     * @param count 随机码的位数
     */
    public static String getRandomCodeB(Integer count) {
        String code = "";
        char[] codes = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T',
                'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        Random r = new Random();
        for (int i = 0; i < count; i++) {
            code += codes[r.nextInt(codes.length)];
        }
        return code;
    }


    /**
     * 生成订单编号
     *
     * @return 订单编号
     */
    public static String genOrderNo() {
        Date curDate = new Date();
        String dateStr4yyyyMMddHHmmss = new SimpleDateFormat("yyyyMMddHHmmss").format(curDate);
        String dateStr4SSS = new SimpleDateFormat("SSS").format(curDate);
        String random = RandomStringUtils.random(4, false, true);
        return dateStr4yyyyMMddHHmmss + random + dateStr4SSS;
    }

    /**
     * 功能:根据时间，生成不重复的字符串
     */
    public static String createUniqueStrByDate(Integer count) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        char[] codes = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T',
                'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        Random r = new Random();
        String code = "";
        for (int i = 0; i < count; i++) {
            code += codes[r.nextInt(codes.length)];
        }
        return sdf.format(date) + code;
    }

    /**
     * 功能:根据时间，生成不重复long类型的字符串
     */
    public static long createUniqueLongByDate() {
        String code = "";
        char[] codes = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        Random r = new Random();
        for (int i = 0; i < 6; i++) {
            code += codes[r.nextInt(codes.length)];
        }
        return Long.valueOf(code + System.currentTimeMillis());
    }

    /**
     * 把重量转换成string
     */
    public static String createKG(Integer kg) {
        if (kg != null && kg > 0) {
            return kg + "KG";
        }
        return "";
    }


    /**
     * 比较字符串
     */
    public static final Comparator<String> StringComparator = (s1, s2) -> {
        int n1 = s1 == null ? 0 : s1.length();
        int n2 = s2 == null ? 0 : s2.length();
        int mn = n1 < n2 ? n1 : n2;
        for (int i = 0; i < mn; i++) {
            int k = s1.charAt(i) - s2.charAt(i);
            if (k != 0) {
                return k;
            }
        }
        return n1 - n2;
    };

    /**
     * oemCodeResource去空格,小写转大写
     */
    public static String createOemCode(String oemCodeResource) {
        //去掉空格并把小写转为大写
        String oemCode = oemCodeResource.replace(" ", "").toUpperCase();
        return oemCode;
    }
}
