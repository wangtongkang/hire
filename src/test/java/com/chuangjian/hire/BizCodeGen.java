package com.chuangjian.hire;



import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.*;

/**
 * Created by Kictto on 2017/6/2.
 */
public class BizCodeGen {

    public static void main(String[] args) {
//        System.out.println("确认生成代码？(y/n) :");
//        Scanner scanner = new Scanner(System.in);
//        String inputString = scanner.nextLine();
//        if (!inputString.equals("y") && !inputString.equals("Y")) {
//            return;
//        }
//        ClassPathResource classPathResource = new ClassPathResource("dbconfig.properties");
//        Properties properties = new Properties();
//        try {
//            properties.load(classPathResource.getInputStream());
//        } catch (IOException e) {
//            e.printStackTrace();
//            throw new RuntimeException("资源文件读取错误，配置文件的位置不正确");
//        }
//
//        CodeGenerator codeGenerator = new CodeGenerator(
//                properties.getProperty("kangaroo.jdbc.url")
//                , properties.getProperty("kangaroo.jdbc.user")
//                , properties.getProperty("kangaroo.jdbc.password")
//        );
//
//        //各项文件的叶子包路径
//        //比如:com.yangche51.app.module.entity
//        //比如:com.yangche51.app.module.service.impl
//        //叶子包路径分别为：.entity和.service.impl
//        //以下为默认值
//        codeGenerator.setPutMapperIntoResourcesDir(true);
//        codeGenerator.setMapperPrefixPath("mapper");
//
//        codeGenerator.setEntityLeafPackage(".domain");
//        codeGenerator.setMapperLeafPackage(".mapper");
//        codeGenerator.setRepoLeafPackage(".dao");
//        codeGenerator.setServiceLeafPackage(".service");
//        codeGenerator.setServiceImplLeafPackage(".service.impl");
//        //各项文件的文件名后缀
//        //以下为默认值
//        codeGenerator.setEntityFileSuffix("DO");
//        codeGenerator.setMapperFileSuffix("Mapper");
//        codeGenerator.setRepoFileSuffix("Mapper");
//        codeGenerator.setServiceFileSuffix("Service");
//        codeGenerator.setServiceImplFileSuffix("ServiceImpl");
//
//
//        String basePackage = "com.chuangjian.hire.product";
//        //设置模块和表名
//        //List<Map<模块名,List<表名>>>
//        List<Map<String, List<String>>> moduleTables = new ArrayList<>();
//        Map<String, List<String>> module = new HashMap<>();
//        List<String> tables = new ArrayList<>();
//
//        tables.add("users_product");
////        tables.add("order_main");
//
//
//        module.put("", tables);
//        moduleTables.add(module);
//
//        //设置待生成的内容
//        ToGenerateContent toduList = new ToGenerateContent();
//        toduList.setEntity(true);
//        toduList.setMapper(true);
////        toduList.setRepo(true);
////        toduList.setService(true);
////        toduList.setServiceImpl(true);
//        codeGenerator.generate(BizCodeGen.class, basePackage, moduleTables, toduList);
    }
}
