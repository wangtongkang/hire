package com.chuangjian.hire;

import com.chuangjian.hire.sms.enums.SmsTypeEnums;
import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class SmsUtils extends BaseTest {

    @Value("${sms.sdk.appid}")
    private Integer appid;
    @Value("${sms.sdk.key}")
    private String appkey;
    @Value("${sms.sdk.smsSign}")
    private String smsSign;

    @SneakyThrows
    public Boolean sendSimpleMsg(Integer templateId, String tel, ArrayList<String> params, String content) {
        //创建发送短信的对象,传入申请的id和key
        SmsSingleSender ssender = new SmsSingleSender(appid, appkey);

        SmsSingleSenderResult result = ssender.sendWithParam("86", tel, templateId, params, smsSign, "", "");  // 签名不能为空串
        System.out.println(result);
        return true;
    }


    @SneakyThrows
    @Test
    public void sendTest() {
        //创建发送短信的对象,传入申请的id和key
        SmsSingleSender ssender = new SmsSingleSender(appid, appkey);

        ArrayList<String> param = new ArrayList<>();
        param.add("66538");
        param.add("3");


        SmsSingleSenderResult result = ssender.sendWithParam("86", "17682441915", SmsTypeEnums.REGISTER.getTemplateId(), param, smsSign, "", "");  // 签名不能为空串

    }
}