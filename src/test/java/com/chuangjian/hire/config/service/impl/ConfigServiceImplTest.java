package com.chuangjian.hire.config.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.chuangjian.hire.BaseTest;
import com.chuangjian.hire.config.service.ConfigService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class ConfigServiceImplTest extends BaseTest {

    @Resource
    ConfigService configService;

    @Test
    void getIndexBanner() {
    }

    @Test
    void saveIndexBanner() {


//      [{"url":"/sdfasdf/1234124","href":"/1234123/12341234"},{"url":"/sdfasdf/1234124","href":"/1234123/12341234"},{"url":"/sdfasdf/1234124","href":"/1234123/12341234"}]
        configService.saveIndexBanner("123,123123,123123123");
        String indexBanner = configService.getIndexBanner();
        log.info("配置信息：{}", indexBanner);
    }
}