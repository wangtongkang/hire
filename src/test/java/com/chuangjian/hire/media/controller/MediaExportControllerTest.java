package com.chuangjian.hire.media.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chuangjian.hire.BaseTest;
import com.chuangjian.hire.common.dto.R;
import com.chuangjian.hire.media.dto.TypesDTO;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

class MediaExportControllerTest extends BaseTest {
    @Resource
    MediaExportController mediaExportController;

    @Test
    void typePageList() {
        Page page = new Page();

        page.setSize(100);
        R r = mediaExportController.typePageList(page, new TypesDTO());
        System.out.println(r);
    }
}