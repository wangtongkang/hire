package com.chuangjian.hire.media.controller;

import com.chuangjian.hire.BaseTest;
import com.chuangjian.hire.common.dto.IdsDTO;
import com.chuangjian.hire.common.dto.R;
import com.chuangjian.hire.media.dto.MediaFilterDTO;
import com.chuangjian.hire.users.dao.UsersMapper;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

class MediaItemControllerTest extends BaseTest {

    @Resource
    MediaItemController mediaItemController;

    @Resource
    MediaExportController mediaExportController;

    @Resource
    UsersMapper usersMapper;

    @Test
    void push() {

        IdsDTO idsDTO = new IdsDTO();
        idsDTO.setIdList(Arrays.asList(1, 2, 3, 4, 5, 6, 7).stream().map(it -> Long.valueOf(it)).collect(Collectors.toList()));

        R push = mediaItemController.push(idsDTO);

        System.out.println(push);
    }


    @Test
    void export() throws IOException {


        String roleNames = usersMapper.getRoleNameList(27L).stream().collect(Collectors.joining(","));
        MediaFilterDTO mediaFilterDTO = new MediaFilterDTO();


        R export = mediaExportController.export(mediaFilterDTO);


        System.out.println(export);
    }
}