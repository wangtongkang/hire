package com.chuangjian.hire.order.service.impl;

import com.chuangjian.hire.BaseTest;
import com.chuangjian.hire.order.service.WechatService;
import com.chuangjian.hire.users.dto.initJSSDKDataDTO;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

class WechatServiceImplTest extends BaseTest {
    @Resource
    WechatService wechatService;

    @Test
    void getInitJSSDKData() {

        initJSSDKDataDTO initJSSDKData = wechatService.getInitJSSDKData("http://meigong999.com");

        System.out.println(initJSSDKData);
    }
}