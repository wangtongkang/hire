package com.chuangjian.hire.qiniu.service.impl;

import com.chuangjian.hire.BaseTest;
import com.chuangjian.hire.qiniu.dto.TokenDTO;
import com.chuangjian.hire.qiniu.enums.QiNiuTokenTypeEnums;
import com.chuangjian.hire.qiniu.service.QiNiuService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.io.File;
import java.nio.file.Files;

class QiNiuServiceImplTest extends BaseTest {
    @Resource
    QiNiuService qiNiuService;

    @Test
    @SneakyThrows
    void uploadFile() {

        File file = new File("/Users/wangtk/Desktop/hire/WechatIMG161.jpeg");
        byte[] fileContent = Files.readAllBytes(file.toPath());
        String filePath = qiNiuService.uploadFile(QiNiuTokenTypeEnums.USER_IMG_PREFIX.getType(), "WechatIMG161.jpeg", fileContent);
        TokenDTO tokenDTO = new TokenDTO();
        System.out.println(tokenDTO.getDomain().concat("/").concat(filePath));
        System.out.println(filePath);
    }
}