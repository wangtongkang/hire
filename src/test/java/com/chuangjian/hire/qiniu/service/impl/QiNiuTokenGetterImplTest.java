package com.chuangjian.hire.qiniu.service.impl;

import com.chuangjian.hire.BaseTest;
import com.chuangjian.hire.qiniu.config.QiNiuConfig;
import com.chuangjian.hire.qiniu.service.QiNiuTokenGetter;
import com.qiniu.common.QiniuException;
import com.qiniu.processing.OperationManager;
import com.qiniu.processing.OperationStatus;
import com.qiniu.util.StringUtils;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

class QiNiuTokenGetterImplTest extends BaseTest {

    @Resource
    QiNiuTokenGetter qiNiuTokenGetter;
    @Resource
    QiNiuConfig qiNiuConfig;

    @Test
    void getThumbnailImg() throws QiniuException {
        //将多个数据处理指令拼接起来
        String crop = "/imageMogr2/crop/x820";
        String thumbnail = "/imageMogr2/thumbnail/!292";
        String persistentOpfs = StringUtils.join(new String[]{
                crop, thumbnail
        }, ";");
        String persistentPipeline = "default.sys";
        String persistentNotifyUrl = qiNiuConfig.getDomain() + qiNiuConfig.getNotifyUrl();
        OperationManager operationManager = qiNiuTokenGetter.getOperationManager();
        String persistentId = operationManager.pfop(qiNiuConfig.getBucket_name(),
                "user/FiYXhV8V-065P4T50Sac0-nujYCn.jpg",
                persistentOpfs,
                persistentPipeline,
                persistentNotifyUrl,
                true
        );
        OperationStatus prefop = operationManager.prefop(persistentId);
        System.out.println(prefop);
    }
}