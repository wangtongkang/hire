package com.chuangjian.hire.sms.service.impl;

import com.chuangjian.hire.BaseTest;
import com.chuangjian.hire.sms.service.MailService;
import com.chuangjian.hire.users.dto.EmailDTO;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

class MailServiceImplTest extends BaseTest {

    @Resource
    MailService mailService;


    @Test
    void sendEmailCode() {


        EmailDTO email = new EmailDTO();
        email.setEmail("513190048@qq.com");
        email.setCode("123123");
        mailService.sendEmailCode(email);
    }
}